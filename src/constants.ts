type Colors = { [key: string]: string }
export const THEME_COLORS: Colors = {
  // Black & white
  white: '#FFFFFF',
  lightGrey: '#F8F9FA',
  neutralGrey: '#ECECEC',
  black: '#000000',
}

type VizColors = { [key: string]: string[] }
export const VIZ_COLORS: VizColors = {
  // Viz palette
  main: [
    '#FD7F6F',
    '#80AED3',
    '#B3D76B',
    '#BC80BD',
    '#F7B667',
    '#FCEB72',
    '#BEB9DB',
    '#FAD0E6',
    '#90D4C8',
    '#D4B590',
    '#C6DAEB',
    '#FFBCB3',
    '#E0F0C2',
    '#E1DEED',
    '#FCDAB6',
    '#FFF5B3',
    '#E3CCE6',
    '#FCE8F4',
    '#DAF1ED',
    '#EADAC8',
    '#964C42',
    '#526F87',
    '#6A803F',
    '#895D8A',
    '#AB7E47',
    '#B0A450',
    '#7C798F',
    '#C7A5B7',
    '#6DA198',
    '#806D57',
  ],

  // Groups palette
  group: [
    '#E1184D',
    '#4AB7FC',
    '#5BE693',
    '#6C2EF0',
    '#F5A22F',
    '#E4D536',
    '#9D23D7',
    '#D7238F',
    '#48BCA0',
    '#BC6B48',
  ],
}
