import { ChangeEvent } from 'react'
import type { RangePickerProps } from 'antd/lib/date-picker'
import { DatePicker } from 'antd'
import { ScaleOrdinal } from 'd3'
import {
  Grid,
  Box,
  FormGroup,
  FormControlLabel,
  FormControl,
  SelectChangeEvent,
  Typography,
  Link,
} from '@mui/material'
import moment from 'moment'
import {
  getMinMaxDates,
  setAllCategoriesVisibility,
  sortArrayGroupsAlphabetically,
} from 'lib'
import {
  AggregationSelect,
  DatasetSelect,
  AlphabeticOrderSelect,
  CustomSwitch,
} from 'components'
import { GroupRow } from './GroupRow'
import { Group as GroupType } from 'models/group'
import { getCategoriesHierarchy, isGroup } from '../lib/groups'

import {
  AGGREGATION_TYPE_MONTHLY,
  AGGREGATION_TYPE_YEARLY,
  Datum,
  IAggregationType,
  IDataType,
  ITimeframe,
  IVisualizationType,
  TIMEFRAME_CURRENT,
  VISUALIZATION_TYPE_BARCHART,
  IDatasetType,
} from 'models'

interface FiltersProps {
  startDate: any
  setStartDate: any
  endDate: any
  setEndDate: any
  fullDataset: Datum[]
  aggregation: IAggregationType
  setAggregation: any
  normalization: boolean
  setNormalization: any
  subSelection: IDataType
  setSubSelection: any
  timeframe: ITimeframe
  visualization: IVisualizationType
  groups: GroupType[]
  setGroups: any
  colorScale: ScaleOrdinal<string, string>
  filters: Record<string, boolean>
  setFilters: any
  datasetType: IDatasetType
  alphabeticOrder: string
  setAlphabeticOrder: any
}

export const Filters = ({
  startDate,
  endDate,
  setNormalization,
  aggregation,
  setAggregation,
  setStartDate,
  setEndDate,
  fullDataset,
  normalization,
  subSelection,
  setSubSelection,
  timeframe,
  visualization,
  groups,
  setGroups,
  colorScale,
  filters,
  setFilters,
  datasetType,
  alphabeticOrder,
  setAlphabeticOrder,
}: FiltersProps) => {
  const { RangePicker } = DatePicker

  const filtersKeys = Object.keys(filters)

  const categoriesHierarchy = getCategoriesHierarchy(groups, filtersKeys)
  const categoriesHierarchyAlphabetic = sortArrayGroupsAlphabetically(
    alphabeticOrder,
    categoriesHierarchy,
    'label'
  )

  const [minDate, maxDate] = getMinMaxDates(fullDataset)

  const onChangeRange: RangePickerProps['onChange'] = (
    date: any,
    dateString: string[]
  ) => {
    switch (aggregation) {
      case AGGREGATION_TYPE_YEARLY:
        if (dateString[0] === '') {
          // If the user reset the calendar show the entire date range
          setStartDate(minDate)
          setEndDate(maxDate)
        } else {
          // Add months at the end when calendar is set to years
          setStartDate(dateString[0] + '-01')
          setEndDate(dateString[1] + '-12')
        }
        break
      case AGGREGATION_TYPE_MONTHLY:
        if (dateString[0] === '') {
          // If the user reset the calendar show the entire date range
          setStartDate(minDate)
          setEndDate(maxDate)
        } else {
          // Calendar set to months
          setStartDate(dateString[0])
          setEndDate(dateString[1])
        }
        break
    }
  }

  const handleAggregation = (event: SelectChangeEvent) => {
    setAggregation(event.target.value as string)
  }

  const handleAlphabeticOrder = (event: SelectChangeEvent) => {
    setAlphabeticOrder(event.target.value as string)
  }

  const handleSubSelection = (event: SelectChangeEvent) => {
    setSubSelection(event.target.value as string)
  }

  const handleFiltersChange = (key: string) => {
    const isTriggeredByAGroup = isGroup(groups, key)
    if (isTriggeredByAGroup) {
      const keys = groups.find((g) => g.label === key)?.categories as string[]
      const filtersUpdated = keys.reduce((acc, value) => {
        acc[value] = !filters[value]
        return acc
      }, {} as Record<string, boolean>)
      setFilters({
        ...filters,
        ...filtersUpdated,
        [key]: !filters[key],
      })
    } else {
      setFilters({
        ...filters,
        [key]: !filters[key],
      })
    }
  }

  const hideAllCategories = () => {
    setFilters(setAllCategoriesVisibility(filters, false))
  }

  const showAllCategories = () => {
    setFilters(setAllCategoriesVisibility(filters, true))
  }

  const handleChangeNormalization = (event: ChangeEvent<HTMLInputElement>) => {
    setNormalization(event.target.checked)
  }

  return (
    <Grid item xs={12} pt={'7px'}>
      {/* Data types */}
      <Box style={{ ...labelAndSelect }}>
        <Typography variant="body1" m={0}>
          Data types
        </Typography>
        <DatasetSelect
          subSelection={subSelection}
          handleSubSelection={handleSubSelection}
        />
      </Box>
      {/* Aggregation period */}
      {timeframe !== TIMEFRAME_CURRENT && (
        <Box style={labelAndSelect}>
          <Typography variant="body1" m={0}>
            Period of aggregation
          </Typography>
          <AggregationSelect
            aggregation={aggregation}
            handleAggregation={handleAggregation}
          />
        </Box>
      )}
      {/* Date range */}
      {timeframe !== TIMEFRAME_CURRENT && (
        <Box style={labelAndSelect} pb={'8px'}>
          <Typography variant="body1" m={0}>
            Data from
          </Typography>
          <RangePicker
            picker={aggregation === AGGREGATION_TYPE_YEARLY ? 'year' : 'month'}
            suffixIcon=""
            separator="→"
            onChange={onChangeRange}
            style={{ width: '50%', border: 'none' }}
            disabledDate={(d) =>
              !d || d.isAfter(maxDate) || d.isSameOrBefore(minDate)
            }
            value={
              aggregation === AGGREGATION_TYPE_YEARLY
                ? [
                    moment(startDate ? startDate : minDate, 'YYYY'),
                    moment(endDate ? endDate : maxDate, 'YYYY'),
                  ]
                : [
                    moment(startDate ? startDate : minDate, 'YYYY-MM'),
                    moment(endDate ? endDate : maxDate, 'YYYY-MM'),
                  ]
            }
          />
        </Box>
      )}
      {/* normalization */}
      {visualization === VISUALIZATION_TYPE_BARCHART &&
        timeframe !== TIMEFRAME_CURRENT && (
          <Box mt={1} pb={1}>
            <FormGroup>
              <FormControlLabel
                sx={{ marginLeft: '0' }}
                label={'Normalization'}
                control={
                  <CustomSwitch
                    defaultChecked
                    checked={normalization}
                    onChange={handleChangeNormalization}
                  />
                }
              />
            </FormGroup>
          </Box>
        )}
      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          margin: '18px 0 0 0',
        }}
      >
        <div style={{ display: 'flex', overflow: 'hidden' }}>
          <p style={{ lineHeight: 1.55, opacity: 0.65, marginRight: '6px' }}>
            Sort:{' '}
          </p>
          <AlphabeticOrderSelect
            alphabeticOrder={alphabeticOrder}
            handleAlphabeticOrder={handleAlphabeticOrder}
          />
        </div>

        <div style={{ display: 'flex' }}>
          <Link
            sx={linkStyle}
            style={{ marginRight: '10px' }}
            onClick={() => showAllCategories()}
          >
            <Typography variant="body1">Show all </Typography>
          </Link>
          <Link onClick={() => hideAllCategories()} sx={linkStyle}>
            <Typography variant="body1">Hide all</Typography>
          </Link>
        </div>
      </Box>
      {/* checkboxes  */}
      <FormControl
        sx={{ mt: 1 }}
        style={{
          width: '100%',
          height: '40vh',
          overflowY: 'auto',
          overflowX: 'hidden',
          marginBottom: '40px',
          paddingRight: '5px',
        }}
        component="fieldset"
        variant="standard"
      >
        <FormGroup>
          {categoriesHierarchyAlphabetic.map((group: GroupType) => {
            return (
              <GroupRow
                key={group.label}
                element={group}
                filters={filters}
                toggleVisibility={handleFiltersChange}
                visualization={visualization}
                groups={groups}
                setGroups={setGroups}
                colorScale={colorScale}
                datasetType={datasetType}
              />
            )
          })}
        </FormGroup>
      </FormControl>
    </Grid>
  )
}

const labelAndSelect: object = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  marginBottom: '8px',
}

const linkStyle: object = {
  textDecoration: 'none',
  color: '#72777D',
  '&:hover': {
    color: 'black',
  },
}
