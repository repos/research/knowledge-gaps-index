import { BarStack } from '@visx/shape'
import { Group } from '@visx/group'
import { Grid } from '@visx/grid'
import { AxisBottom } from '@visx/axis'
import { scaleBand, scaleLinear, scaleOrdinal } from '@visx/scale'
import { timeParse, timeFormat } from 'd3-time-format'
import { prepareDatasetForStackedBarchart } from 'lib'
import { DATA_COLUMN_DATE } from 'models'

const defaultMargin = { top: 40, right: 0, bottom: 0, left: 0 }

export const VisxSimpleStackedBarchart = ({
  data: unstackedDataset,
  width,
  height,
  events = false,
  margin = defaultMargin,
  subSelection,
}: BarStackProps) => {
  const data = prepareDatasetForStackedBarchart(unstackedDataset, subSelection)

  // define colors and margin for graph
  const purple1 = '#6c5efb'
  const purple2 = '#c998ff'
  const purple3 = '#a44afe'
  const background = '#eaedff'

  // get keys of object
  const keys = Object.keys(data[0]).filter(
    (d) => d !== DATA_COLUMN_DATE
  ) as string[]

  const valueTotals = data.reduce((allTotals: any, currentDate: any) => {
    const totalValue = keys.reduce((dailyTotal: any, k) => {
      dailyTotal += Number(currentDate[k as keyof object])
      return dailyTotal
    }, 0)
    allTotals.push(totalValue)
    return allTotals
  }, [] as number[])

  const parseDate = timeParse('%Y-%m-%d')
  const format = timeFormat('%b %d')
  const formatDate = (date: string) => format(parseDate(date) as Date)

  // accessors
  const getDate = (d: any) => d[DATA_COLUMN_DATE]

  // scales
  const dateScale = scaleBand<string>({
    domain: data.map(getDate),
    padding: 0.2,
  })
  const valueScale = scaleLinear<number>({
    domain: [0, Math.max(...valueTotals)],
    nice: true,
  })
  const colorScale = scaleOrdinal<string, string>({
    domain: keys,
    range: [purple1, purple2, purple3],
  })

  if (width < 10) return null
  // bounds
  const xMax = width
  const yMax = height - margin.top - 100

  dateScale.rangeRound([0, xMax])
  valueScale.range([yMax, 0])

  return width < 10 ? null : (
    <div style={{ position: 'relative' }}>
      <svg width={width} height={height}>
        <rect
          x={0}
          y={0}
          width={width}
          height={height}
          fill={background}
          rx={14}
        />
        <Grid
          top={margin.top}
          left={margin.left}
          xScale={dateScale}
          yScale={valueScale}
          width={xMax}
          height={yMax}
          stroke="black"
          strokeOpacity={0.1}
          xOffset={dateScale.bandwidth() / 2}
        />
        <Group top={margin.top}>
          <BarStack<any, string>
            data={data}
            keys={keys}
            x={getDate}
            xScale={dateScale}
            yScale={valueScale}
            color={colorScale}
          >
            {(barStacks) =>
              barStacks.map((barStack) =>
                barStack.bars.map((bar) => (
                  <rect
                    key={`bar-stack-${barStack.index}-${bar.index}`}
                    x={bar.x}
                    y={bar.y}
                    height={bar.height}
                    width={bar.width}
                    fill={bar.color}
                    onClick={() => {
                      if (events) alert(`clicked: ${JSON.stringify(bar)}`)
                    }}
                  />
                ))
              )
            }
          </BarStack>
        </Group>
        <AxisBottom
          top={yMax + margin.top}
          scale={dateScale}
          tickFormat={formatDate}
          stroke={purple3}
          tickStroke={purple3}
          tickLabelProps={() => ({
            fill: purple3,
            fontSize: 11,
            textAnchor: 'middle',
          })}
        />
      </svg>
    </div>
  )
}

export type BarStackProps = {
  data: any
  width: number
  height: number
  margin?: { top: number; right: number; bottom: number; left: number }
  events?: boolean
  subSelection: string
}
