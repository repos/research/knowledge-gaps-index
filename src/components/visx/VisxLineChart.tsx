import { ScaleOrdinal } from 'd3'
import {
  AnimatedAxis,
  AnimatedGrid,
  XYChart,
  Tooltip,
  buildChartTheme,
  LineSeries,
} from '@visx/xychart'
import { round } from 'lodash-es'
import { curveLinear } from '@visx/curve'
import { aggregateAndFlatDataset, getValuesUniqBy , formatNumbersWithCommas} from 'lib'
import {
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  Datum,
  IAggregationType,
  IDataType,
} from 'models'

interface VisxLineChartProps {
  width: number
  height: number
  filteredDataset: Datum[]
  aggregation: IAggregationType
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
}

export const VisxLineChart = ({
  height,
  filteredDataset: unstackedDataset,
  aggregation,
  subSelection,
  colorScale,
}: VisxLineChartProps) => {
  const dataset = aggregateAndFlatDataset(
    unstackedDataset,
    aggregation,
    subSelection
  ) as any[]
  const categories = getValuesUniqBy(unstackedDataset, DATA_COLUMN_CATEGORY)
  const keys = Object.keys(dataset[0]).filter((e) => e !== DATA_COLUMN_DATE)

  const filteredColors = keys.map((key) => colorScale(key))

  const wikiTheme = buildChartTheme({
    backgroundColor: '#ffffff' as string,
    colors: filteredColors,
    gridColor: '#30475e' as string,
    gridColorDark: '#222831' as string,
    svgLabelSmall: { fill: '#30475e' } as object,
    svgLabelBig: { fill: '#30475e' } as object,
    tickLength: 4 as number,
  })

  const getDatasetbyCategoryandDate = (category: string) => {
    return categories
      .filter((e) => e === category)
      .flatMap((category) => {
        return dataset.map((d) => {
          return {
            [DATA_COLUMN_DATE]:
              d[DATA_COLUMN_DATE].length === 4
                ? `${d[DATA_COLUMN_DATE]}-01-01`
                : `${d[DATA_COLUMN_DATE]}-01`,
            [category]: d[category as keyof object],
          }
        })
      })
  }

  return (
    <>
      <XYChart
        theme={wikiTheme}
        xScale={{ type: 'band', paddingInner: 0.3 }}
        yScale={{ type: 'linear' }}
        height={Math.min(400, height)}
      >
        <AnimatedGrid
          key={`grid-center`} // force animate on update
          rows={true}
          columns={false}
          animationTrajectory={'center'}
          numTicks={4}
        />
        {keys.map((k: string) => (
          <LineSeries
            key={k}
            dataKey={k}
            data={getDatasetbyCategoryandDate(k)}
            enableEvents
            xAccessor={(d: any) => d[DATA_COLUMN_DATE]}
            yAccessor={(d: any) => d[k as keyof object]}
            curve={curveLinear}
          />
        ))}
        <AnimatedAxis
          key={`time-axis-center-false`}
          orientation={'bottom'}
          numTicks={4}
          animationTrajectory={'center'}
        />
        <AnimatedAxis
          key={`temp-axis-center-false`}
          orientation={'right'}
          numTicks={4}
          animationTrajectory={'center'}
        />
        <Tooltip
          showVerticalCrosshair
          snapTooltipToDatumX
          snapTooltipToDatumY
          renderTooltip={({ tooltipData }) => {
            const td = tooltipData as unknown as any
            const tooltipDatum = td.nearestDatum
            const currentDate: string = tooltipDatum.datum[DATA_COLUMN_DATE]

            return (
              <>
                <p style={{ marginBottom: 15 }}>{currentDate}</p>

                {keys.map((key: string) => {
                  if (tooltipDatum.datum[key] > 0) {
                    return (
                      <div
                        key={key}
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          marginBottom: 5,
                        }}
                      >
                        <div
                          style={{
                            display: 'inline-block',
                            width: 16,
                            height: 16,
                            borderRadius: '50%',
                            backgroundColor: colorScale(key),
                            marginRight: 5,
                          }}
                        />
                        <span style={{ marginRight: 10 }}>{key}</span>
                        <span style={{ marginLeft: 'auto' }}>
                          {formatNumbersWithCommas(round(tooltipDatum.datum[key] , 1))}
                        </span>
                      </div>
                    )
                  }
                  return null
                })}
              </>
            )
          }}
        />
      </XYChart>
    </>
  )
}
