import {
  AnimatedAxis,
  BarSeries,
  BarStack,
  AnimatedGrid,
  XYChart,
  Tooltip,
  buildChartTheme,
} from '@visx/xychart'
import { ScaleOrdinal } from 'd3'
import { round } from 'lodash-es'
import { aggregateAndFlatDataset, getValuesUniqBy , formatNumbersWithCommas} from 'lib'
import {
  DATASET_TYPE_GEOGRAPHY,
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  DATA_COLUMN_LANGUAGE,
  Datum,
  IAggregationType,
  IDatasetType,
  IDataType,
} from 'models'

interface VisxStackedBarchartProps {
  width: number
  height: number
  filteredDataset: Datum[]
  normalization: boolean
  aggregation: IAggregationType
  datasetType: IDatasetType
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
}

const getStackOffset = (normalize: boolean) => {
  if (normalize) {
    return 'expand'
  } else {
    return undefined
  }
}

export const VisxStackedBarchart = ({
  height,
  filteredDataset: unstackedDataset,
  normalization,
  aggregation,
  datasetType,
  subSelection,
  colorScale,
}: VisxStackedBarchartProps) => {
  const dataset = aggregateAndFlatDataset(
    unstackedDataset,
    aggregation,
    subSelection
  ) as any[]
  const keys = getValuesUniqBy(unstackedDataset, DATA_COLUMN_CATEGORY)

  const filteredColors = keys.map((key) => colorScale(key))

  const wikiTheme = buildChartTheme({
    backgroundColor: '#ffffff' as string,
    colors: filteredColors,
    gridColor: '#30475e' as string,
    gridColorDark: '#222831' as string,
    tickLength: 6 as number,
  })

  return (
    <>
      <XYChart
        theme={wikiTheme}
        xScale={{ type: 'band', paddingInner: 0.3 }}
        yScale={{ type: 'linear' }}
        height={Math.min(400, height)}
      >
        <AnimatedGrid
          key={`grid-center`} // force animate on update
          rows={true}
          columns={false}
          animationTrajectory={'center'}
          numTicks={4}
        />
        <BarStack offset={getStackOffset(normalization)}>
          {keys.map((k) => (
            <BarSeries
              key={k}
              dataKey={k}
              data={dataset}
              enableEvents
              xAccessor={(d) => d[DATA_COLUMN_DATE]}
              yAccessor={(d) => d[k as keyof object]}
            />
          ))}
        </BarStack>
        <AnimatedAxis
          key={`time-axis-center-false`}
          orientation={'bottom'}
          numTicks={4}
          animationTrajectory={'center'}
        />
        <AnimatedAxis
          key={`temp-axis-center-false`}
          orientation={'right'}
          numTicks={4}
          animationTrajectory={'center'}
        />

        {datasetType !== DATASET_TYPE_GEOGRAPHY && (
          <Tooltip
            showVerticalCrosshair
            snapTooltipToDatumX
            snapTooltipToDatumY
            renderTooltip={({ tooltipData }) => {
              const td = tooltipData as unknown as any
              const tooltipDatum = td.nearestDatum
              const currentDate: string = tooltipDatum.datum[DATA_COLUMN_DATE]

              return (
                <>
                  <p style={{ marginBottom: 15 }}>{currentDate}</p>

                  {keys.map((key) => {
                    if (!tooltipDatum.datum[key]) {
                      return null
                    }

                    return (
                      <div
                        key={key}
                        style={{
                          display: 'flex',
                          alignItems: 'center',
                          marginBottom: 5,
                        }}
                      >
                        <div
                          style={{
                            display: 'inline-block',
                            width: 16,
                            height: 16,
                            borderRadius: '50%',
                            backgroundColor: colorScale(key),
                            marginRight: 5,
                          }}
                        />
                        <span style={{ marginRight: 10 }}>{key}</span>
                        <span style={{ marginLeft: 'auto' }}>
                          {formatNumbersWithCommas(round(tooltipDatum.datum[key],2))}
                        </span>
                      </div>
                    )
                  })}
                </>
              )
            }}
          />
        )}
      </XYChart>
    </>
  )
}
