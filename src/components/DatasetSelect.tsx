import { FormControl, Select, MenuItem, SelectChangeEvent } from '@mui/material'
import { texts } from 'lib'
import {
  DATA_TYPE_ARTICLE_COUNTS,
  DATA_TYPE_PAGE_VIEWS,
  DATA_TYPE_QUALITY_SCORE,
  DATA_TYPE_REVISION_COUNT,
  IDataType,
} from '../models/filters'

export const SUB_SELECTIONS = [
  DATA_TYPE_ARTICLE_COUNTS,
  DATA_TYPE_PAGE_VIEWS,
  DATA_TYPE_QUALITY_SCORE,
  DATA_TYPE_REVISION_COUNT,
]

interface DatasetSelectProps {
  subSelection: IDataType
  handleSubSelection: (event: SelectChangeEvent) => void
}

export const DatasetSelect = ({
  subSelection,
  handleSubSelection,
}: DatasetSelectProps) => {
  return (
    <FormControl style={{ width: '50%', marginRight: '-10px' }}>
      <Select
        labelId="select-label"
        defaultValue={SUB_SELECTIONS[0]}
        placeholder={SUB_SELECTIONS[0]}
        id="select-dataset-label"
        value={subSelection}
        onChange={handleSubSelection}
        sx={{
          height: '35px',
          '&. MuiOutlinedInput-notchedOutline': {
            border: 'none',
          },
        }}
        style={{fontWeight: 'bold'}}
      >
        {SUB_SELECTIONS.map((dataType) => (
          <MenuItem key={dataType} value={dataType}>
            {texts[dataType]}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
