import { ScaleOrdinal } from 'd3'
import { CurrentCharts, OverTimeCharts } from 'components'
import {
  Datum,
  Group,
  IAggregationType,
  IDatasetType,
  IDataType,
  ITimeframe,
  IVisualizationType,
  TIMEFRAME_CURRENT,
} from 'models'

interface ChartsProps {
  visualization: IVisualizationType
  filteredDataset: Datum[]
  fullDataset: Datum[]
  subSelection: IDataType
  aggregation: IAggregationType
  datasetType: IDatasetType
  timeframe: ITimeframe
  normalization: boolean
  colorScale: ScaleOrdinal<string, string>
  groups: Group[]
  filters: Record<string, boolean>
  alphabeticOrder: string
}

export const Charts = ({
  visualization,
  filteredDataset,
  fullDataset,
  subSelection,
  aggregation,
  datasetType,
  timeframe,
  normalization,
  colorScale,
  groups,
  filters,
  alphabeticOrder,
}: ChartsProps) => {
  if (timeframe === TIMEFRAME_CURRENT) {
    return (
      <CurrentCharts
        visualization={visualization}
        fullDataset={fullDataset}
        filteredDataset={filteredDataset}
        subSelection={subSelection}
        aggregation={aggregation}
        colorScale={colorScale}
        datasetType={datasetType}
      />
    )
  } else {
    return (
      <OverTimeCharts
        visualization={visualization}
        filteredDataset={filteredDataset}
        aggregation={aggregation}
        datasetType={datasetType}
        normalization={normalization}
        subSelection={subSelection}
        colorScale={colorScale}
        fullDataset={fullDataset}
        groups={groups}
        filters={filters}
        alphabeticOrder={alphabeticOrder}
      />
    )
  }
}
