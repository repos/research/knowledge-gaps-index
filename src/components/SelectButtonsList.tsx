import { useState } from 'react'
import { Box } from '@mui/material'

interface SelectButtonsListProps {
  options: any
  onChange: any
  value: any
}

export const SelectButtonsList = ({
  options,
  onChange,
  value,
}: SelectButtonsListProps) => {
  const [select, setSelect] = useState<string>(options[0])
  const handleSelect = (key: string) => setSelect(key)

  return (
    <Box
      style={{
        width: '100%',
        display: 'inline-flex',
        justifyContent: 'space-between',
      }}
    >
      <h3>Category: </h3>
      <Box
        style={{
          alignSelf: 'flex-start',
          backgroundColor: '#eaecf0',
          borderRadius: '999px',
        }}
      >
        {options.map((category: any, i: number) => {
          return (
            <button
              key={options[i]}
              onClick={() => handleSelect(options[i])}
              style={select === options[i] ? selected : notSelected}
            >
              {category}
            </button>
          )
        })}
      </Box>
    </Box>
  )
}

const selected: object = {
  padding: '3px 8px 4px',
  color: '#3a25ff',
  border: '2px solid #3a25ff',
  borderRadius: '999px',
  transition: 'color .4s, border-color .9s',
  cursor: 'pointer',
}

const notSelected: object = {
  padding: '3px 8px 4px',
  border: 'none',
  borderColor: '#eaecf0',
  borderRadius: '999px',
  margin: '0 2px',
  transition: 'color .4s, border-color .9s',
  cursor: 'pointer',
}
