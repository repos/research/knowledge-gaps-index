import { Box, Grid } from '@mui/material'
import ParentSize from '@visx/responsive/lib/components/ParentSize'
import {
  RichTable,
  RichTableMultilanguage,
  RichTableLegendMultilanguage,
  CurrentBarchart,
} from 'components'
import { Heatmap } from '../components/svg/Heatmap'
import {
  Datum,
  IAggregationType,
  IDataType,
  IVisualizationType,
  VISUALIZATION_TYPE_BARCHART,
  VISUALIZATION_TYPE_HEATMAP,
  VISUALIZATION_TYPE_RICH_TABLE,
  IDatasetType,
} from 'models'
import { ScaleOrdinal } from 'd3'

interface CurrentChartsProps {
  visualization: IVisualizationType
  filteredDataset: Datum[]
  fullDataset: Datum[]
  subSelection: IDataType
  aggregation: IAggregationType
  colorScale: ScaleOrdinal<string, string>
  datasetType: IDatasetType
}

export const CurrentCharts = ({
  visualization,
  filteredDataset,
  fullDataset,
  subSelection,
  aggregation,
  colorScale,
  datasetType,
}: CurrentChartsProps) => {
  if (!filteredDataset || filteredDataset.length < 0) {
    return null
  }

  if (filteredDataset.length === 0) {
    return (
      <Grid item xs={6} style={{ display: 'flex' }}>
        <h3 style={{ margin: 'auto', color: 'grey' }}>
          Currently there is no visible data. Select a category to discover
          more.
        </h3>
      </Grid>
    )
  }

  if (visualization === VISUALIZATION_TYPE_BARCHART) {
    return (
      <Grid item xs={6}>
        <Box sx={{ width: '100%', height: 330 }}>
          <ParentSize>
            {({ width, height }) => {
              return (
                <div
                  style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'center',
                  }}
                >
                  <CurrentBarchart
                    width={width}
                    height={height}
                    filteredDataset={filteredDataset}
                    fullDataset={fullDataset}
                    subSelection={subSelection}
                    colorScale={colorScale}
                  />
                </div>
              )
            }}
          </ParentSize>
        </Box>
      </Grid>
    )
  }

  // Multilanguage richTable
  if (visualization === VISUALIZATION_TYPE_RICH_TABLE) {
    const datasetGroupedByLan = filteredDataset.reduce(function (r, a) {
      r[a.wiki_db] = r[a.wiki_db] || []
      r[a.wiki_db].push(a)
      return r
    }, Object.create(null))

    const languages = Object.keys(datasetGroupedByLan)

    if (languages.length === 1) {
      return (
        <Grid item xs={6}>
          <Box sx={{ height: 400 }}>
            <ParentSize>
              {({ width, height }) => {
                return (
                  <RichTable
                    width={width}
                    height={height}
                    filteredDataset={filteredDataset}
                    aggregation={aggregation}
                    subSelection={subSelection}
                    colorScale={colorScale}
                    visualization={visualization}
                    datasetType={datasetType}
                  />
                )
              }}
            </ParentSize>
          </Box>
        </Grid>
      )
    } else {
      return (
        <Grid item xs={6}>
          <ParentSize>
            {({ width, height }) => {
              return (
                <>
                  <RichTableLegendMultilanguage />
                  {languages.map((language, i) => {
                    return (
                      <RichTableMultilanguage
                        key={i}
                        width={width}
                        height={height}
                        filteredDataset={datasetGroupedByLan[language]}
                        language={language}
                        aggregation={aggregation}
                        subSelection={subSelection}
                        colorScale={colorScale}
                      />
                    )
                  })}
                </>
              )
            }}
          </ParentSize>
        </Grid>
      )
    }
  }

  if (visualization === VISUALIZATION_TYPE_HEATMAP) {
    return (
      <Grid item xs={6}>
        <Box sx={{ width: '90%', margin: 'auto' }}>
          <Heatmap dataset={filteredDataset} subSelection={subSelection} />
        </Box>
      </Grid>
    )
  }

  return null
}
