export const LoadingPage = () => {
  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 30,
        overflow: 'hidden',
      }}
    >
      Loading dataset...
    </div>
  )
}
