import { Select, MenuItem, SelectChangeEvent } from '@mui/material'

interface AlphabeticOrderSelectProps {
  alphabeticOrder: string
  handleAlphabeticOrder: (event: SelectChangeEvent) => void
}

export const AlphabeticOrderSelect = ({
  alphabeticOrder,
  handleAlphabeticOrder,
}: AlphabeticOrderSelectProps) => {
  return (
    <Select
      labelId="Alphabetic"
      defaultValue={'Alphabetical'}
      id="Alphabetic"
      value={alphabeticOrder}
      onChange={handleAlphabeticOrder}
      sx={{
        height: '24px',
        '&. MuiOutlinedInput-notchedOutline': {
          border: 'none',
        },
      }}
      style={{ opacity: 0.65, width: '115px', marginTop: '-1px' }}
    >
      <MenuItem value={'Alphabetical'}>Alphabetical</MenuItem>
      <MenuItem value={'Reverse Alphabetical'}>Reverse Alphabetical</MenuItem>
    </Select>
  )
}
