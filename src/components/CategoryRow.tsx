import { useState } from 'react'
import { ScaleOrdinal } from 'd3'
import { ReactComponent as AddToGroupIcon } from '../assets/add_to_group.svg'
import { Group as GroupType } from 'models/group'
import { getParentGroup, moveOut } from 'lib/groups'
import { IVisualizationType } from 'models'
import { VisibilityIcon } from './VisibilityIcon'
import { IconButton, Tooltip } from '@mui/material'

interface CategoryRowProps {
  element: GroupType
  filters: FilterValue
  visualization: IVisualizationType
  toggleVisibility: (key: string) => void
  groups: GroupType[]
  setGroups: any
  colorScale: ScaleOrdinal<string, string>
}

type FilterValue = {
  [key: string]: boolean
}

export const CategoryRow = ({
  element,
  filters,
  toggleVisibility,
  visualization,
  groups,
  setGroups,
  colorScale,
}: CategoryRowProps) => {
  const [showGroupMenu, setShowGroupMenu] = useState<boolean>(false)
  const parentGroup = getParentGroup(groups, element.label)
  const text = element.label

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 2,
        paddingBottom: 2,
        userSelect: 'none',
        paddingLeft: 35,
      }}
    >
      <div
        style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          width: 130,
          fontWeight: 'normal',
        }}
      >
        {text}
      </div>

      <div style={{ display: 'flex', alignItems: 'center' }}>
        {/* <div style={{}}>
          {visualization !== VISUALIZATION_TYPE_HEATMAP && (
            <div
              style={{
                width: 12,
                height: 12,
                backgroundColor: colorScale(element.label),
                borderRadius: '50%',
              }}
            />
          )}
        </div> */}

        <div
          style={{
            cursor: 'pointer',
          }}
          onClick={() => toggleVisibility(element.label)}
        >
          <div
            style={{
              height: 18,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Tooltip title="Hide category" arrow>
              <IconButton style={{ height: 32, marginRight: 3 }}>
                <VisibilityIcon
                  // isDisabled
                  size={18}
                  isVisible={!!filters[element.label]}
                />
              </IconButton>
            </Tooltip>
          </div>
        </div>

        <div style={{ cursor: 'pointer', position: 'relative' }}>
          <div onClick={() => setShowGroupMenu(!showGroupMenu)}>
            <Tooltip title="Move out category" arrow>
              <IconButton style={{ height: 32 }}>
                <AddToGroupIcon />
              </IconButton>
            </Tooltip>
          </div>

          {showGroupMenu && (
            <div
              style={{
                position: 'absolute',
                right: 0,
                top: 25,
                border: '1px solid black',
                backgroundColor: 'white',
                padding: 3,
                zIndex: 2,
                overflow: 'visible',
                width: 150,
                fontSize: 14,
              }}
            >
              <div
                style={{ cursor: 'pointer' }}
                onClick={() =>
                  moveOut(
                    groups,
                    element,
                    parentGroup,
                    setGroups,
                    setShowGroupMenu
                  )
                }
              >
                Move out
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
