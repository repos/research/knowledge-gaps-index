import { scaleLinear, ScaleOrdinal } from 'd3'
import { round } from 'lodash-es'
import { Box, Grid, Typography } from '@mui/material'
import ParentSize from '@visx/responsive/lib/components/ParentSize'
import {
  aggregateAndFlatDataset,
  getValuesUniqBy,
  sumValuesWithSameKeys,
  formatNumbersWithCommas,
  getCountry,
} from 'lib'
import { RichTableTrendChart, RichTableLegend } from 'components'
import {
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  Datum,
  IAggregationType,
  IDataType,
  IVisualizationType,
  IDatasetType,
  DATASET_TYPE_GEOGRAPHY,
} from 'models'
import { omit } from 'lodash-es'

const ROW_HEIGHT = 35

interface RichTableProps {
  width: number
  height: number
  filteredDataset: Datum[]
  aggregation: IAggregationType
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
  visualization: IVisualizationType
  datasetType: IDatasetType
}

interface Bar {
  category: string
  value: number
}

export const RichTable = ({
  width: w,
  height: h,
  filteredDataset: unstackedDataset,
  aggregation,
  subSelection,
  colorScale,
  visualization,
  datasetType,
}: RichTableProps) => {
  const dataset = aggregateAndFlatDataset(
    unstackedDataset,
    aggregation,
    subSelection
  ) as any[]
  const categories = getValuesUniqBy(unstackedDataset, DATA_COLUMN_CATEGORY)

  const datasetOnlyCategories = dataset.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const values = sumValuesWithSameKeys(datasetOnlyCategories, categories)

  const allSum = Object.values(values).reduce(
    (partialSum: any, a: any) => (partialSum + a) as number,
    0
  )

  const categoriesValues: Bar[] = Object.entries(values).map(([k, v]) => ({
    category: k,
    value: v as number,
  }))

  const calculatePercentage = (value: number) => {
    return `${((value / (allSum as number)) * 100).toFixed(2).toString()} %`
  }

  // trendlines
  const getDatasetbyCategoryandDate = (category: string) => {
    return categories
      .filter((e: any) => e === category)
      .flatMap((category: any) => {
        return dataset.map((d: any) => {
          return {
            [DATA_COLUMN_DATE]:
              d[DATA_COLUMN_DATE].length === 4
                ? `${d[DATA_COLUMN_DATE]}-01-01`
                : `${d[DATA_COLUMN_DATE]}-01`,
            [category]: d[category as keyof object],
          }
        })
      })
  }

  const getLabel = (category: string) => {
    if (datasetType === DATASET_TYPE_GEOGRAPHY) {
      return `${getCountry(category)} (${category})`
    } else {
      return category
    }
  }

  return (
    <Box pl={3} pt={0.7} pb={10}>
      <RichTableLegend />
      {categoriesValues.map((e: Bar) => {
        return (
          <Box key={e[DATA_COLUMN_CATEGORY]}>
            {/* charts */}
            <Grid container>
              {/* category */}
              <Grid item xs={3}>
                <Typography variant={'body1'}>
                  {getLabel(e[DATA_COLUMN_CATEGORY])}
                </Typography>
              </Grid>
              {/* number */}
              <Grid item xs={1}>
                <Typography variant={'body1'}>
                  {formatNumbersWithCommas(round(e.value, 2)).toString()}
                </Typography>
              </Grid>
              {/* bar */}
              <Grid item xs={3} px={1}>
                <Box mb={0.5} height={ROW_HEIGHT}>
                  <ParentSize>
                    {({ width }) => {
                      const x = scaleLinear<number, number>()
                        .domain([0, allSum] as [number, number])
                        .range([0, width])
                      return (
                        <svg width={width} height={ROW_HEIGHT}>
                          <g>
                            <rect
                              x={0}
                              y={0}
                              height={ROW_HEIGHT}
                              width={w}
                              fill={'#EAECF0'}
                            />
                            <rect
                              x={0}
                              y={0}
                              height={ROW_HEIGHT}
                              width={x(e.value)}
                              fill={colorScale(e[DATA_COLUMN_CATEGORY])}
                            />
                          </g>
                        </svg>
                      )
                    }}
                  </ParentSize>
                </Box>
              </Grid>
              {/* percentage */}
              <Grid item xs={1}>
                {calculatePercentage(e.value)}
              </Grid>
              <Grid item xs={4}>
                <Box height={ROW_HEIGHT}>
                  <ParentSize>
                    {({ width, height }) => {
                      return (
                        <RichTableTrendChart
                          dataset={getDatasetbyCategoryandDate(
                            e[DATA_COLUMN_CATEGORY]
                          )}
                          colorScale={colorScale}
                          width={width}
                          height={height}
                          category={e[DATA_COLUMN_CATEGORY]}
                        />
                      )
                    }}
                  </ParentSize>
                </Box>
              </Grid>
            </Grid>
          </Box>
        )
      })}
    </Box>
  )
}
