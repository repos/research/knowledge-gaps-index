import { Grid } from '@mui/material'

export const RichTableLegendMultilanguage = () => {
  return (
    <Grid container pl={3} pt={0.8}>
      <Grid item xs={2} style={legendTextStyle}>
        Languages
      </Grid>
      <Grid item xs={6} />
      <Grid item xs={2} style={legendTextStyle}>
        Tot. Articles
      </Grid>
      <Grid item xs={2} style={legendTextStyle}>
        Highest Gap Abs.
      </Grid>
    </Grid>
  )
}

const legendTextStyle = {
  fontWeight: 'bold',
  marginBottom: '20px',
}
