import * as d3 from 'd3'
import {
  DATA_COLUMN_DATE,
  Datum,
  IAggregationType,
  IDatasetType,
  IDataType,
} from 'models'

interface HighlightLinechartProps {
  fullDataset: Datum[]
  filteredDataset: Datum[]
  width: number
  height: number
  aggregation: IAggregationType
  datasetType: IDatasetType
  subSelection: IDataType
}

export const HighlightLinechart = ({
  fullDataset,
  filteredDataset,
  width,
  height,
  aggregation,
  datasetType,
  subSelection,
}: HighlightLinechartProps) => {
  /* const parseDataset = (dataset: any[]) => {
    return dataset.map((d) => {
      return {
        date: d3.timeParse('%Y-%m-%d')(`${d[DATA_COLUMN_DATE]}-01`),
        value: d.valueCml,
      }
    })
  }
  const keys = Object.keys(filteredDataset[0]).filter(
    (e) => e !== DATA_COLUMN_DATE
  )
  const parsedDataset = parseDataset(filteredDataset)

  const dates = parsedDataset.map((d) => {
    return d[DATA_COLUMN_DATE]
  })

  const extent = d3.extent(dates as Date[]) as [Date, Date]

  const x = d3.scaleTime().domain(extent).range([0, width])

  TODO finish chart */

  return null
}
