import { Grid } from '@mui/material'

export const RichTableLegend = () => {
  return (
    <Grid container>
      <Grid item xs={3} style={legendTextStyle}>
        Categories
      </Grid>
      <Grid item xs={1} style={legendTextStyle}>
        Value
      </Grid>
      <Grid item xs={4} />
      <Grid item xs={1} style={legendTextStyle}>
        %
      </Grid>
      <Grid item xs={3} style={legendTextStyle}>
        Trend
      </Grid>
    </Grid>
  )
}

const legendTextStyle = {
  fontWeight: 'bold',
  marginBottom: '20px',
}
