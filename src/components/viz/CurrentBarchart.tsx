import * as d3 from 'd3'
import { ScaleOrdinal } from 'd3'
import { omit, sum, sumBy } from 'lodash-es'
import { styled, Tooltip, tooltipClasses, TooltipProps } from '@mui/material'
import {
  prepareDatasetForStackedBarchart,
  sumValuesWithSameKeys,
  textEllipsis,
  formatNumbersWithCommas,
} from 'lib'
import { DATA_COLUMN_DATE, Datum } from 'models'

interface CurrentBarchartProps {
  width: number
  height: number
  filteredDataset: Datum[]
  fullDataset: Datum[]
  subSelection: string
  colorScale: ScaleOrdinal<string, string>
}

export const CurrentBarchart = ({
  width,
  height,
  filteredDataset: unstackedDataset,
  fullDataset,
  subSelection,
  colorScale,
}: CurrentBarchartProps) => {
  const dataset = prepareDatasetForStackedBarchart(
    unstackedDataset,
    subSelection
  )

  const margin = { top: 92, right: 0, bottom: 20, left: 40 }

  const categories = Object.keys(dataset?.[0]).filter(
    (e) => e !== DATA_COLUMN_DATE
  )

  const datasetOnlyCategories = dataset.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const values = sumValuesWithSameKeys(datasetOnlyCategories, categories)

  const fullSum = sumBy(fullDataset, subSelection)
  const allSum: number = sum(Object.values(values))

  const xScale = d3
    .scaleLinear()
    .domain([0, allSum])
    .range([0, width - margin.left - margin.right])

  // Sort all current data
  const orderedData = []
  for (const category in values) {
    orderedData.push([category, values[category]])
  }
  orderedData.sort((a, b) => b[1] - a[1])

  let currentPosX = margin.left
  let currentWidth = 0
  let showLabel = true

  const BarchartTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className, tooltip: className }} />
  ))({
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: 'none',
      minWidth: 'fit-content',
      backgroundColor: 'black',
    },
  })

  return (
    <>
      <svg width={width} height={height}>
        {orderedData.map((orderedDatum, i) => {
          const currentKey = orderedDatum[0]
          const currentValue =
            subSelection === 'quality_score_value'
              ? orderedDatum[1].toFixed(2)
              : orderedDatum[1]
          const currentPerc = (currentValue * 100) / fullSum
          if (currentValue <= 0) return null

          currentPosX = currentWidth + currentPosX
          currentWidth = xScale(currentValue)
          if (currentWidth <= 0) return null

          showLabel = true
          if (currentKey.length * 10 > currentWidth) showLabel = false

          return (
            <g key={currentKey} data-val={currentValue} data-key={currentKey}>
              {showLabel && (
                <g fontFamily={'Inter, sans-serif'}>
                  <text
                    y={22}
                    x={currentPosX}
                    fontWeight="bold"
                    style={{
                      textOverflow: 'ellipsis',
                      overflow: 'hidden',
                      whiteSpace: 'nowrap',
                    }}
                  >
                    {textEllipsis(currentKey, currentWidth)}
                  </text>
                  <text
                    y={48}
                    x={currentPosX}
                    fontSize={13.5}
                    letterSpacing={-0.2}
                  >
                    {currentPerc.toFixed(2) + '%'}
                  </text>
                  <text
                    y={72}
                    x={currentPosX}
                    fontSize={13.5}
                    letterSpacing={-0.3}
                  >
                    {textEllipsis(
                      formatNumbersWithCommas(currentValue),
                      currentWidth
                    )}
                  </text>
                </g>
              )}
              <BarchartTooltip
                followCursor
                disableHoverListener={
                  currentKey.length * 10 > currentWidth ? false : true
                }
                title={
                  <div>
                    <div>{currentKey}</div>
                    <div>{currentPerc.toFixed(2) + '%'}</div>
                    <div>{formatNumbersWithCommas(currentValue)}</div>
                  </div>
                }
              >
                <rect
                  y={margin.top}
                  x={currentPosX}
                  width={currentWidth}
                  height={height - margin.top}
                  fill={colorScale(currentKey)}
                />
              </BarchartTooltip>
            </g>
          )
        })}
      </svg>
      {!showLabel && (
        <p
          style={{
            display: 'inline-block',
            margin: '20px 0 0 40px',
            padding: '5px 10px',
            borderRadius: '5px',
            color: 'black',
            backgroundColor: '#E4E7ED',
            fontSize: '13.5px',
            textAlign: 'center',
          }}
        >
          Some figures are not fully visible. Hide some categories to discover
          more.
        </p>
      )}
    </>
  )
}
