import { scaleLinear, ScaleOrdinal, stack } from 'd3'
import { omit, pick, minBy, maxBy } from 'lodash-es'
import {
  prepareDatasetForStackedBarchart,
  sumValuesWithSameKeys,
  formatNumberCompact,
  sortArrayGroupsAlphabetically,
} from 'lib'
import { DATA_COLUMN_DATE, Datum, IDataType } from 'models'
import { Box } from '@mui/material'

export const DetailBarchart = ({
  width,
  visibleHeight,
  dataset: unstackedDataset,
  subSelection,
  colorScale,
  alphabeticOrder,
}: {
  width: number
  visibleHeight: number
  dataset: Datum[]
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
  alphabeticOrder: string
}) => {
  const dataset = prepareDatasetForStackedBarchart(
    unstackedDataset,
    subSelection
  )

  const margin = { top: 10, right: 0, bottom: 6, left: 0 }
  const innerBarchartHeight = visibleHeight * 0.6

  const categories = Object.keys(dataset?.[0]).filter(
    (e) => e !== DATA_COLUMN_DATE
  )

  const datasetOnlyCategories = dataset.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const datasetOnlyTimeBuckets = dataset.map((datum) =>
    pick(datum, DATA_COLUMN_DATE)
  )

  const minTimeBucket: Partial<{ time_bucket: string }> | undefined = minBy(
    datasetOnlyTimeBuckets,
    DATA_COLUMN_DATE
  )

  const maxTimeBucket: Partial<{ time_bucket: string }> | undefined = maxBy(
    datasetOnlyTimeBuckets,
    DATA_COLUMN_DATE
  )

  const minDate = minTimeBucket?.[DATA_COLUMN_DATE]
  const maxDate = maxTimeBucket?.[DATA_COLUMN_DATE]

  const values = sumValuesWithSameKeys(datasetOnlyCategories, categories)
  const allSum = Object.values(values).reduce(
    (partialSum: any, a: any) => partialSum + a,
    0
  )

  const stackedData = stack().keys(categories.slice())([values])

  const yScale = scaleLinear()
    .domain([0, allSum as unknown as number])
    .range([0, innerBarchartHeight])

  return (
    <Box width={width}>
      <svg width={'100%'} height={innerBarchartHeight}>
        {stackedData.map((d, i) => {
          const barHeight = yScale(d[0][1]) - yScale(d[0][0])
          const min = (dataset as any).find(
            (e: any) => e[DATA_COLUMN_DATE] === minDate
          )
          const max = (dataset as any).find(
            (e: any) => e[DATA_COLUMN_DATE] === maxDate
          )
          const minValue: number = min[d.key]
          const maxValue: number = max[d.key]
          const difference = maxValue - minValue

          return (
            <g key={d.key} className={d.key}>
              {barHeight > 0 && ( //Threshold to hide invisible rectangles
                <>
                  <rect
                    y={yScale(d[0][0])}
                    x={margin.left}
                    height={barHeight}
                    width={width - margin.left - margin.right}
                    fill={colorScale(d.key)}
                  />
                  <pattern
                    id="pattern"
                    width="8"
                    height="10"
                    patternUnits="userSpaceOnUse"
                    patternTransform="rotate(45 50 50)"
                  >
                    <line stroke={'#ffffff99'} stroke-width="7px" y2="10" />
                  </pattern>
                  <rect
                    y={yScale(d[0][0])}
                    x={margin.left}
                    height={yScale(difference < 0 ? -difference : difference)}
                    width={width - margin.left - margin.right}
                    fill="url(#pattern)"
                  />
                </>
              )}
            </g>
          )
        })}
      </svg>
      <Box
        width={width}
        height={200}
        sx={{ overflowY: 'scroll', marginTop: '10px' }}
      >
        {sortArrayGroupsAlphabetically(alphabeticOrder, stackedData, 'key').map(
          (d: any) => {
            const category: string = d.key
            const min = (dataset as any).find(
              (e: any) => e[DATA_COLUMN_DATE] === minDate
            )
            const max = (dataset as any).find(
              (e: any) => e[DATA_COLUMN_DATE] === maxDate
            )
            const minValue: number = min[category]
            const maxValue: number = max[category]
            const difference = maxValue - minValue
            const currentValue = d[0].data[d.key]
            const differenceCompact = formatNumberCompact(difference)
            const currentValueCompact = formatNumberCompact(currentValue)

            return (
              <Box key={d.key} sx={{ display: 'flex', alignItems: 'center' }}>
                <p
                  style={{
                    margin: 0,
                    width: '45%',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                  }}
                >
                  {d.key}
                </p>

                <Box
                  sx={{
                    display: 'flex',
                    width: '65px',
                    marginLeft: 'auto',
                    alignItems: 'center',
                  }}
                >
                  <Box
                    sx={{
                      height: '14px',
                      width: '14px',
                      borderRadius: '50%',
                      backgroundColor: colorScale(d.key),
                    }}
                  ></Box>
                  <p style={{ margin: '0 2px' }}>{currentValueCompact}</p>
                </Box>

                <Box
                  sx={{
                    display: 'flex',
                    width: '65px',
                    alignItems: 'center',
                  }}
                >
                  <Box
                    sx={{
                      height: '14px',
                      width: '14px',
                      borderRadius: '50%',
                      backgroundColor: colorScale(d.key),
                      backgroundImage: `repeating-linear-gradient(
                      -45deg, 
                      #ffffff99 0px, 
                      #ffffff99 2px, 
                      transparent 2px, 
                      transparent 4px
                    )`,
                    }}
                  ></Box>
                  <p style={{ margin: '0 2px' }}>
                    {difference >= 0
                      ? `+${differenceCompact}`
                      : differenceCompact}
                  </p>
                </Box>
              </Box>
            )
          }
        )}
      </Box>
    </Box>
  )
}
