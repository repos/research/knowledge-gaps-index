import { ScaleOrdinal } from 'd3'
import { XYChart, LineSeries, buildChartTheme } from '@visx/xychart'
import { curveLinear } from '@visx/curve'
import { Box } from '@mui/material'
import { DATA_COLUMN_DATE } from 'models'

interface RichTableTrendChartProps {
  dataset: any[]
  height: number
  width: number
  category: string
  colorScale: ScaleOrdinal<string, string>
}

export const RichTableTrendChart = ({
  dataset,
  width,
  height,
  category,
  colorScale,
}: RichTableTrendChartProps) => {
  const wikiTheme = buildChartTheme({
    backgroundColor: '#ffffff' as string,
    colors: [colorScale(category)],
    gridColor: '#30475e' as string,
    gridColorDark: '#222831' as string,
    svgLabelSmall: { fill: '#30475e' } as object,
    svgLabelBig: { fill: '#30475e' } as object,
    tickLength: 4 as number,
  })

  const getTrendPercentage = () => {
    const first = dataset[0]
    const firstEl = Object.values(first)[1] as number
    const last = dataset[dataset.length - 1]
    const lastEl = Object.values(last)[1] as number
    const percentage = (firstEl + lastEl) / 100
    return percentage
  }

  const percentage = getTrendPercentage()

  return (
    <Box
      display={'flex'}
      sx={{
        height: height,
        transform: 'scaleY(1)',
        backgroundColor: 'white',
      }}
    >
      <Box>
        <XYChart
          theme={wikiTheme}
          xScale={{ type: 'band' }}
          yScale={{ type: 'linear' }}
          margin={{ top: 1, right: -10, bottom: 1, left: -10 }}
          height={height}
          width={width * 0.55}
        >
          <LineSeries
            dataKey={category}
            data={dataset}
            xAccessor={(d: any) => d[DATA_COLUMN_DATE]}
            yAccessor={(d: any) => d[category]}
            curve={curveLinear}
            strokeWidth={1.5}
          />
        </XYChart>
      </Box>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'start',
        }}
      >
        <p style={{ margin: '0px 0px 0px 8px' }}>{`${
          percentage >= 0
            ? `+ ${percentage.toFixed(2)}`
            : `- ${percentage.toFixed(2)}`
        } %`}</p>
      </Box>
    </Box>
  )
}
