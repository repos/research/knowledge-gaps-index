import { scaleLinear, ScaleOrdinal } from 'd3'
import { Box, Grid, Typography } from '@mui/material'
import ParentSize from '@visx/responsive/lib/components/ParentSize'
import {
  aggregateAndFlatDataset,
  getValuesUniqBy,
  sumValuesWithSameKeys,
  formatNumbersWithCommas,
} from 'lib'
import {
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  Datum,
  IAggregationType,
  IDataType,
} from 'models'
import { omit, round } from 'lodash-es'

const ROW_HEIGHT = 35

interface RichTableMultilanguageProps {
  width: number
  height: number
  filteredDataset: Datum[]
  language: string
  aggregation: IAggregationType
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
}

export const RichTableMultilanguage = ({
  width,
  height: h,
  filteredDataset: unstackedDataset,
  language,
  aggregation,
  subSelection,
  colorScale,
}: RichTableMultilanguageProps) => {
  const dataset = aggregateAndFlatDataset(
    unstackedDataset,
    aggregation,
    subSelection
  ) as any[]
  const categories = getValuesUniqBy(unstackedDataset, DATA_COLUMN_CATEGORY)

  const datasetOnlyCategories = dataset.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const values = sumValuesWithSameKeys(datasetOnlyCategories, categories)

  const allSum = Object.values(values).reduce(
    (partialSum: any, a: any) => (partialSum + a) as number,
    0
  )
  function getRoundedValue(value: number) {
    return subSelection === 'quality_score_value' ? round(value, 2) : value
  }

  // Sort all current data
  const orderedData: any = []
  for (const category in values) {
    orderedData.push([category, values[category]])
  }
  orderedData.sort((a: any, b: any) => b[1] - a[1])

  const highestGap = orderedData[0][1] - orderedData[orderedData.length - 1][1]

  return (
    <Box pl={3} pt={0}>
      <Box>
        {/* charts */}
        <Grid container>
          {/* category */}
          <Grid item xs={3}>
            <Typography variant={'body1'}>{language}</Typography>
          </Grid>
          {/* bar */}
          <Grid item xs={5} px={1}>
            <Box mb={0.5} mx={2} height={ROW_HEIGHT}>
              <ParentSize>
                {({ width }) => {
                  let currentPosX = 0
                  let currentWidth = 0

                  const xScale = scaleLinear<number, number>()
                    .domain([0, allSum] as [number, number])
                    .range([0, width])

                  return (
                    <svg width={width} height={ROW_HEIGHT}>
                      {orderedData.map((orderedDatum: any) => {
                        const currentKey = orderedDatum[0]
                        const currentValue = orderedDatum[1].toFixed(2)
                        if (currentValue <= 0) return null

                        currentPosX = currentWidth + currentPosX
                        currentWidth = xScale(currentValue)
                        if (currentWidth <= 0) return null

                        return (
                          <rect
                            key={currentKey}
                            data-val={currentValue}
                            data-key={currentKey}
                            y={0}
                            x={currentPosX}
                            width={currentWidth}
                            height={ROW_HEIGHT}
                            fill={colorScale(currentKey)}
                          />
                        )
                      })}
                    </svg>
                  )
                }}
              </ParentSize>
            </Box>
          </Grid>
          {/* percentage */}
          <Grid item xs={2}>
            {formatNumbersWithCommas(getRoundedValue(allSum as number))}
          </Grid>
          {/* percentage */}
          <Grid item xs={2}>
            <div
              style={{
                display: 'inline-block',
                width: '12px',
                height: '12px',
                backgroundColor: colorScale(orderedData[0][0]),
                borderRadius: '10px',
                marginLeft: '1px',
              }}
            ></div>
            <div
              style={{
                display: 'inline-block',
                width: '12px',
                height: '12px',
                backgroundColor: colorScale(
                  orderedData[orderedData.length - 1][0]
                ),
                borderRadius: '10px',
                marginLeft: '-4px',
              }}
            ></div>
             {formatNumbersWithCommas(getRoundedValue(highestGap))}
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}
