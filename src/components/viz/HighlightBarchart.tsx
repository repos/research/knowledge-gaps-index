import { scaleBand, scaleLinear, ScaleOrdinal, stack } from 'd3'
import { cloneDeep, omit, uniq } from 'lodash-es'
import {
  aggregateAndFlatDataset,
  filterItems,
  getParentGroup,
  getValuesUniqBy,
} from 'lib'
import {
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  Datum,
  Group,
  IAggregationType,
  IDataType,
} from 'models'

const margin = { top: 10, right: 45, bottom: 10, left: 45 }
const brushVertPadding = 10

export const HighlightBarchart = ({
  filteredDataset: unstackedFilteredDataset,
  fullDataset: unstackedFullDataset,
  width,
  height: rawHeight,
  aggregation,
  subSelection,
  colorScale,
  groups,
  filters,
}: {
  filteredDataset: Datum[]
  fullDataset: Datum[]
  width: number
  height: number
  aggregation: IAggregationType
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
  groups: Group[]
  filters: Record<string, boolean>
}) => {
  // filter full dataset by categories and replace category names with the one in the groups if necessary
  // use this dataset as the filtered one, because the filtered one (unstackedDataset) is also filtered by date
  // and the brush should be show all the data in time
  const filteredDatasetCustom = unstackedFullDataset.map((datum) => {
    const parentGroup = getParentGroup(groups, datum[DATA_COLUMN_CATEGORY])
    const isInAGroup = parentGroup !== undefined
    return {
      ...datum,
      [DATA_COLUMN_CATEGORY]: isInAGroup
        ? parentGroup.label
        : datum[DATA_COLUMN_CATEGORY],
    }
  })

  const visibleCategories = filterItems(filters) // array of categories with value true in filters
  const filteredByCategoriesAndLanguage = filteredDatasetCustom.filter((d) =>
    visibleCategories.includes(d[DATA_COLUMN_CATEGORY])
  )
  const fullDatasetUnsorted = filteredByCategoriesAndLanguage

  // sort fullDataset by categories on the same order of unstackedFullDataset
  const unstackedFullDatasetCategories = uniq(
    unstackedFilteredDataset.map((d) => d[DATA_COLUMN_CATEGORY])
  ).map((c) => {
    const parentGroup = getParentGroup(groups, c)
    const isInAGroup = parentGroup !== undefined
    return isInAGroup ? parentGroup.label : c
  })
  const fullDataset = cloneDeep(fullDatasetUnsorted).sort(
    (a, b) =>
      unstackedFullDatasetCategories.indexOf(a[DATA_COLUMN_CATEGORY]) -
      unstackedFullDatasetCategories.indexOf(b[DATA_COLUMN_CATEGORY])
  )

  const fullDatasetStacked = aggregateAndFlatDataset(
    fullDataset,
    aggregation,
    subSelection
  ) as any[]
  const categories = getValuesUniqBy(fullDataset, DATA_COLUMN_CATEGORY)

  // Filtered dataset stacked
  const filteredDatasetStacked = aggregateAndFlatDataset(
    unstackedFilteredDataset,
    aggregation,
    subSelection
  ) as any[]

  const height = Math.min(100, rawHeight)
  const innerGraphWidth = width - margin.right - margin.left
  const innerGraphHeight = height - margin.bottom - margin.top
  const groupsComputed = fullDatasetStacked.map(
    (e) => e[DATA_COLUMN_DATE]
  ) as string[]
  const datasetOnlyCategories = fullDatasetStacked.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const allValues = datasetOnlyCategories.map((e) =>
    Object.values(e)
  ) as number[][]

  const sum = allValues.map((e) =>
    e.reduce((previousValue, currentValue) => previousValue + currentValue, 0)
  )
  const max = Math.max(...sum)

  const xScale = scaleBand()
    .domain(groupsComputed)
    .range([0, innerGraphWidth])
    .paddingInner(0.2)
    .paddingOuter(0.1)

  const yScale = scaleLinear().domain([0, max]).range([innerGraphHeight, 0])

  const stackedData = stack().keys(categories)(fullDatasetStacked)

  // Brush
  // Calc brush size and position
  const brushStart = fullDatasetStacked.findIndex(
    (d) => d[DATA_COLUMN_DATE] === filteredDatasetStacked[0][DATA_COLUMN_DATE]
  )

  const brushEnd = fullDatasetStacked.findIndex(
    (d) =>
      d[DATA_COLUMN_DATE] ===
      filteredDatasetStacked[filteredDatasetStacked.length - 1][
        DATA_COLUMN_DATE
      ]
  )

  // Brush settings and measures
  const colNumber = groupsComputed.length
  const padding = (innerGraphWidth * xScale.padding()) / colNumber
  const colWidth = xScale.bandwidth() + padding
  const brushWidth = filteredDatasetStacked.length * colWidth

  return (
    <svg width={width} height={height}>
      {stackedData.map((stackedDatum) => {
        return (
          <g key={stackedDatum.key} fill={colorScale(stackedDatum.key)}>
            {stackedDatum.map((e) => {
              const start = e[0]
              const end = e[1]
              const stringDate = e.data[DATA_COLUMN_DATE].toString()
              const xValue = xScale(stringDate) as number

              return (
                <rect
                  key={stringDate}
                  x={xValue + margin.left}
                  y={yScale(end) + margin.top}
                  height={Math.abs(yScale(start) - yScale(end))}
                  width={xScale.bandwidth()}
                />
              )
            })}
          </g>
        )
      })}

      {/* Data opacity left side */}
      <rect
        x={margin.left}
        y={brushVertPadding / 2}
        width={brushStart * colWidth}
        height={innerGraphHeight + brushVertPadding}
        fill="#ffffff"
        fillOpacity={0.44}
      />
      {/* Brush */}
      <rect
        x={margin.left + brushStart * colWidth}
        y={brushVertPadding / 2}
        width={brushWidth}
        height={innerGraphHeight + brushVertPadding}
        fill="none"
        rx={5}
        ry={5}
        stroke="#3a25ff"
        strokeWidth={1}
      />
      {/* Data opacity right */}
      <rect
        x={margin.left + (brushEnd + 1) * colWidth}
        y={brushVertPadding / 2}
        width={(colNumber - 1 - brushEnd) * colWidth}
        height={innerGraphHeight + brushVertPadding}
        fill="#ffffff"
        fillOpacity={0.44}
      />
    </svg>
  )
}
