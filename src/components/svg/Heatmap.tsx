import { useState } from 'react'
import { prepareDatasetForStackedBarchart, sumValuesWithSameKeys } from 'lib'
import { DATA_COLUMN_DATE } from 'models'
import { omit } from 'lodash-es'
import {
  HoverTooltips,
  ContinentNames,
  Oceania,
  Americas,
  Africa,
  Antarctica,
  Europe,
  Asia,
} from '../heatmap'

interface CurrentBarchartProps {
  dataset: any
  subSelection: string
}

export const Heatmap = ({
  dataset: unstackedDataset,
  subSelection,
}: CurrentBarchartProps) => {
  const [hoverValue, setHoverValue] = useState(['none', 'not hovering'])
  const dataset = prepareDatasetForStackedBarchart(
    unstackedDataset,
    subSelection
  )
  const subgroups = Object.keys(dataset?.[0]).filter(
    (e) => e !== DATA_COLUMN_DATE
  )
  const datasetOnlyCategories = dataset.map((datum) =>
    omit(datum, DATA_COLUMN_DATE)
  )

  const values = sumValuesWithSameKeys(datasetOnlyCategories, subgroups)

  const numericValues = Object.values(values) as number[]
  const minValue = Math.min(...numericValues)
  const maxValue = Math.max(...numericValues)

  return (
    <svg
      viewBox="0 0 869 593"
      preserveAspectRatio="xMinYMin meet"
      style={{ marginBottom: '20px' }}
    >
      <HoverTooltips
        subSelection={subSelection}
        hoverValue={hoverValue}
        minValue={minValue}
        maxValue={maxValue}
      />
      <ContinentNames />
      <Oceania
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
      <Americas
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
      <Africa
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
      <Antarctica
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
      <Europe
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
      <Asia
        values={values}
        maxValue={maxValue}
        minValue={minValue}
        setHoverValue={setHoverValue}
      />
    </svg>
  )
}
