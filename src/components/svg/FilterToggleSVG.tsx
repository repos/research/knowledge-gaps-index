export const FilterToggleSVG = () => {
  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0 16C0 7.16344 7.16344 0 16 0C24.8366 0 32 7.16344 32 16C32 24.8366 24.8366 32 16 32C7.16344 32 0 24.8366 0 16Z"
        fill="#3A25FF"
      />
      <mask
        id="mask0_680_5186"
        style={{ maskType: 'alpha' }}
        maskUnits="userSpaceOnUse"
        x="6"
        y="6"
        width="20"
        height="20"
      >
        <rect x="6" y="6" width="20" height="20" fill="white" />
      </mask>
      <g mask="url(#mask0_680_5186)">
        <path
          d="M8.5 21.833V20.083H13.667V21.833H8.5ZM8.5 11.917V10.167H16.771V11.917H8.5ZM15.208 23.5V18.438H16.958V20.083H23.5V21.833H16.958V23.5H15.208ZM11.917 18.542V16.875H8.5V15.125H11.917V13.479H13.667V18.542H11.917ZM15.208 16.875V15.125H23.5V16.875H15.208ZM18.333 13.562V8.5H20.083V10.167H23.5V11.917H20.083V13.562H18.333Z"
          fill="white"
        />
      </g>
    </svg>
  )
}
