import { useForm, Controller, SubmitHandler } from 'react-hook-form'
import {
  Box,
  Grid,
  Button,
  Typography,
  Autocomplete,
  TextField,
} from '@mui/material'
import { Radio } from 'antd'
import { texts } from 'lib'
import CloseIcon from '@mui/icons-material/Close'
import {
  DATASET_TYPE_GENDER,
  DATASET_TYPE_GEOGRAPHY,
  DATASET_TYPE_SEXUAL_ORIENTATION,
  DATASET_TYPE_TEST,
  DATASET_TYPE_TIMESPAN,
  IMainFilters,
  IMainSelect,
  TIMEFRAME_CURRENT,
  TIMEFRAME_OVER_TIME,
} from 'models'
import { IS_DEVELOPMENT } from 'App'
import { Breadcrumb } from './Breadcrumb'

const { wikipedia, contents } = texts

interface MainSelectProps {
  mainFilters: IMainFilters
  setMainFilters: any
  languages: string[]
  selectedLanguages: string[]
  setSelectedLanguages: (l: string[]) => void
  setMainSelectOpen: any
  mainSelectOpen: boolean
}

export const MainSelect = ({
  mainFilters,
  setMainFilters,
  languages,
  selectedLanguages,
  setSelectedLanguages,
  setMainSelectOpen,
  mainSelectOpen,
}: MainSelectProps) => {
  const datasets = [
    {
      name: 'gap',
      values: [
        DATASET_TYPE_GENDER,
        DATASET_TYPE_SEXUAL_ORIENTATION,
        DATASET_TYPE_GEOGRAPHY,
        DATASET_TYPE_TIMESPAN,
        IS_DEVELOPMENT ? DATASET_TYPE_TEST : null,
      ].filter(Boolean),
    },
    { name: 'project', values: [wikipedia] },
    { name: 'dimension', values: [contents] },
    { name: 'timeframe', values: [TIMEFRAME_CURRENT, TIMEFRAME_OVER_TIME] },
  ]

  const defaultValues: IMainSelect = {
    gap: mainFilters.datasetType,
    project: wikipedia,
    dimension: contents,
    timeframe: mainFilters.timeframe,
  }

  const { control, handleSubmit, reset } = useForm<IMainSelect>({
    defaultValues,
    shouldUnregister: false,
  })

  const onSubmit: SubmitHandler<IMainSelect> = (data) => {
    setMainFilters({ datasetType: data.gap, timeframe: data.timeframe })
    setMainSelectOpen((p: boolean) => !p)
  }

  return (
    <>
      <Breadcrumb
        mainFilters={mainFilters}
        languages={selectedLanguages}
        onIconClick={() => setMainSelectOpen((p: boolean) => !p)}
      />

      {mainSelectOpen && (
        <Grid
          container
          sx={{
            backgroundColor: '#f8f9fa',
            borderRadius: '10px',
            padding: '20px 25px',
            marginBottom: '1vh',
            position: 'relative',
          }}
        >
          {/* TODO: USE THIS LAYOUT <SelectButtonsList options={gaps} /> */}
          <form onSubmit={handleSubmit(onSubmit)} style={{ width: '100%' }}>
            <Grid item xs={12} display={'flex'} flexDirection={'column'}>
              {datasets.map(({ name, values }) => {
                return (
                  <Controller
                    key={name as keyof IMainSelect}
                    name={name as keyof IMainSelect}
                    control={control}
                    render={({ field }) => (
                      <Box display="flex" key={name}>
                        <Box sx={{ width: '160px' }}>
                          <Typography
                            variant="body2"
                            sx={{ lineHeight: '30px' }}
                          >
                            {texts[name === 'gap' ? 'knowledgeGap' : name]}
                          </Typography>
                        </Box>
                        <Radio.Group
                          {...field}
                          style={{
                            alignSelf: 'flex-start',
                            backgroundColor: '#eaecf0',
                            borderRadius: '999px',
                            marginBottom: '8px',
                          }}
                        >
                          {values.map((value) => {
                            return (
                              <Radio.Button
                                key={value}
                                value={value}
                                style={{
                                  backgroundColor: '#eaecf0',
                                  borderRadius: '999px',
                                  marginLeft: '1px',
                                  border: '1px solid #eaecf0',
                                }}
                              >
                                {value}
                              </Radio.Button>
                            )
                          })}
                        </Radio.Group>
                      </Box>
                    )}
                  />
                )
              })}
            </Grid>

            {/* CTA */}
            <Grid
              item
              xs={12}
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
              }}
            >
              {/* CANCEL BUTTON */}
              <Button
                onClick={() => reset()}
                style={{
                  border: '1px solid lightgrey',
                  marginRight: '5px',
                  cursor: 'pointer',
                  textTransform: 'none',
                }}
              >
                Reset
              </Button>
              {/* SUBMIT BUTTON */}
              <Button
                type="submit"
                sx={{
                  border: 'none',
                  color: 'white',
                  backgroundColor: '#3a25ff',
                  textTransform: 'none',
                }}
              >
                Apply
              </Button>
            </Grid>
          </form>
        </Grid>
      )}

      <div
        style={{
          display: 'flex',
          backgroundColor: '#f8f9fa',
          padding: '15px 25px',
          borderRadius: '10px',
          marginBottom: '2vh',
          alignItems: 'center',
          position: 'relative',
        }}
      >
        {mainSelectOpen && (
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: '#ffffffd4',
            }}
          ></div>
        )}
        <div style={{ width: '160px', fontWeight: 'bold' }}>Languages</div>
        <Box sx={{ display: 'flex', width: '100%' }}>
          <Box sx={{ width: '100%' }}>
            <Box>
              <Autocomplete
                multiple
                id="languages"
                options={languages}
                getOptionLabel={(option) => option}
                freeSolo
                onChange={(event: any, newValue: string[]) => {
                  newValue.length === 0
                    ? setSelectedLanguages([
                        selectedLanguages[selectedLanguages.length - 1],
                      ])
                    : setSelectedLanguages(newValue)
                }}
                value={selectedLanguages}
                ChipProps={{
                  deleteIcon: (
                    <CloseIcon
                      sx={{
                        fill: '#3a25ff',
                        width: '14px',
                        height: '14px',
                      }}
                    />
                  ),
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label=""
                    placeholder="&#x1F50D;   Add a Language"
                    InputLabelProps={{ shrink: false }}
                    sx={{
                      '& .MuiInput-input': {
                        border: '1px solid transparent',
                        borderRadius: '999px',
                        margin: '4px',
                        padding: '6px !important',
                        backgroundColor: '#eaecf0',
                        maxWidth: '135px',
                      },
                    }}
                  />
                )}
              />
            </Box>
          </Box>
        </Box>
      </div>
    </>
  )
}
