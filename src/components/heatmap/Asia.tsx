import { scaleThreshold } from 'd3'
import { texts, formatNumbersWithCommas } from 'lib'

interface AsiaProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Asia = ({
  values,
  maxValue,
  minValue,
  setHoverValue,
}: AsiaProps) => {
  const delta = maxValue - minValue
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])

  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  return (
    <g id="Asia">
      <g
        id="AF"
        fill={getCountryFill('AF')}
        onMouseOver={(e) => showCountryDatum(e, 'AF')}
        onMouseOut={(e) => hideCountryDatum(e, 'AF')}
      >
        <path d="M648 216H624V240H648V216Z" />
        <text style={getCountryStyle('AF')}>
          <tspan x="630" y="231.39">
            AF
          </tspan>
        </text>
      </g>
      <g
        id="AM"
        fill={getCountryFill('AM')}
        onMouseOver={(e) => showCountryDatum(e, 'AM')}
        onMouseOut={(e) => hideCountryDatum(e, 'AM')}
      >
        <path d="M624 144H600V168H624V144Z" />
        <text style={getCountryStyle('AM')}>
          <tspan x="604" y="159.085">
            AM
          </tspan>
        </text>
      </g>
      <g
        id="AZ"
        fill={getCountryFill('AZ')}
        onMouseOver={(e) => showCountryDatum(e, 'AZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'AZ')}
      >
        <path d="M648 168H624V192H648V168Z" />
        <text style={getCountryStyle('AZ')}>
          <tspan x="630" y="183.39">
            AZ
          </tspan>
        </text>
      </g>
      <g
        id="BH"
        fill={getCountryFill('BH')}
        onMouseOver={(e) => showCountryDatum(e, 'BH')}
        onMouseOut={(e) => hideCountryDatum(e, 'BH')}
      >
        <path d="M624.139 240H600.139V264H624.139V240Z" />
        <text style={getCountryStyle('BH')}>
          <tspan x="605" y="255.219">
            BH
          </tspan>
        </text>
      </g>
      <g
        id="BD"
        fill={getCountryFill('BD')}
        onMouseOver={(e) => showCountryDatum(e, 'BD')}
        onMouseOut={(e) => hideCountryDatum(e, 'BD')}
      >
        <path d="M696 192H672V216H696V192Z" />
        <text style={getCountryStyle('BD')}>
          <tspan x="677" y="207.085">
            BD
          </tspan>
        </text>
      </g>
      <g
        id="BT"
        fill={getCountryFill('BT')}
        onMouseOver={(e) => showCountryDatum(e, 'BT')}
        onMouseOut={(e) => hideCountryDatum(e, 'BT')}
      >
        <path d="M720 168H696V192H720V168Z" />
        <text style={getCountryStyle('BT')}>
          <tspan x="702" y="183.085">
            BT
          </tspan>
        </text>
      </g>
      <g
        id="BN"
        fill={getCountryFill('BN')}
        onMouseOver={(e) => showCountryDatum(e, 'BN')}
        onMouseOut={(e) => hideCountryDatum(e, 'BN')}
      >
        <path d="M744.139 288.171H720.139V312.171H744.139V288.171Z" />
        <text style={getCountryStyle('BN')}>
          <tspan x="725" y="303.39">
            BN
          </tspan>
        </text>
      </g>
      <g
        id="KH"
        fill={getCountryFill('KH')}
        onMouseOver={(e) => showCountryDatum(e, 'KH')}
        onMouseOut={(e) => hideCountryDatum(e, 'KH')}
      >
        <path d="M744 240H720V264H744V240Z" />
        <text style={getCountryStyle('KH')}>
          <tspan x="725" y="255.39">
            KH
          </tspan>
        </text>
      </g>
      <g
        id="CN"
        fill={getCountryFill('CN')}
        onMouseOver={(e) => showCountryDatum(e, 'CN')}
        onMouseOut={(e) => hideCountryDatum(e, 'CN')}
      >
        <path d="M720 144H696V168H720V144Z" />
        <text style={getCountryStyle('CN')}>
          <tspan x="701" y="159.39">
            CN
          </tspan>
        </text>
      </g>
      <g
        id="CY"
        fill={getCountryFill('CY')}
        onMouseOver={(e) => showCountryDatum(e, 'CY')}
        onMouseOut={(e) => hideCountryDatum(e, 'CY')}
      >
        <path d="M552 264H528V288H552V264Z" />
        <text style={getCountryStyle('CY')}>
          <tspan x="533" y="278.56">
            CY
          </tspan>
        </text>
      </g>
      <g
        id="GE"
        fill={getCountryFill('GE')}
        onMouseOver={(e) => showCountryDatum(e, 'GE')}
        onMouseOut={(e) => hideCountryDatum(e, 'GE')}
      >
        <path d="M600 144H576V168H600V144Z" />
        <text style={getCountryStyle('GE')}>
          <tspan x="581" y="159.085">
            GE
          </tspan>
        </text>
      </g>
      <g
        id="IN"
        fill={getCountryFill('IN')}
        onMouseOver={(e) => showCountryDatum(e, 'IN')}
        onMouseOut={(e) => hideCountryDatum(e, 'IN')}
      >
        <path d="M672 216H648V240H672V216Z" />
        <text style={getCountryStyle('IN')}>
          <tspan x="655" y="231.085">
            IN
          </tspan>
        </text>
      </g>
      <g
        id="ID"
        fill={getCountryFill('ID')}
        onMouseOver={(e) => showCountryDatum(e, 'ID')}
        onMouseOut={(e) => hideCountryDatum(e, 'ID')}
      >
        <path d="M744 312H720V336H744V312Z" />
        <text style={getCountryStyle('ID')}>
          <tspan x="727" y="327.39">
            ID
          </tspan>
        </text>
      </g>
      <g
        id="TL"
        fill={getCountryFill('TL')}
        onMouseOver={(e) => showCountryDatum(e, 'TL')}
        onMouseOut={(e) => hideCountryDatum(e, 'TL')}
      >
        <path d="M744 336H720V360H744V336Z" />
        <text style={getCountryStyle('TL')}>
          <tspan x="726" y="351.39">
            TL
          </tspan>
        </text>
      </g>
      <g
        id="IR"
        fill={getCountryFill('IR')}
        onMouseOver={(e) => showCountryDatum(e, 'IR')}
        onMouseOut={(e) => hideCountryDatum(e, 'IR')}
      >
        <path d="M624 216H600V240H624V216Z" />
        <text style={getCountryStyle('IR')}>
          <tspan x="606.861" y="231.219">
            IR
          </tspan>
        </text>
      </g>
      <g
        id="IQ"
        fill={getCountryFill('IQ')}
        onMouseOver={(e) => showCountryDatum(e, 'IQ')}
        onMouseOut={(e) => hideCountryDatum(e, 'IQ')}
      >
        <path d="M648 192H624V216H648V192Z" />
        <text style={getCountryStyle('IQ')}>
          <tspan x="631" y="207.39">
            IQ
          </tspan>
        </text>
      </g>
      <g
        id="IL"
        fill={getCountryFill('IL')}
        onMouseOver={(e) => showCountryDatum(e, 'IL')}
        onMouseOut={(e) => hideCountryDatum(e, 'IL')}
      >
        <path d="M576 264H552V288H576V264Z" />
        <text style={getCountryStyle('IL')}>
          <tspan x="560" y="279.39">
            IL
          </tspan>
        </text>
      </g>
      <g
        id="JP"
        fill={getCountryFill('JP')}
        onMouseOver={(e) => showCountryDatum(e, 'JP')}
        onMouseOut={(e) => hideCountryDatum(e, 'JP')}
      >
        <path d="M840 144H816V168H840V144Z" />
        <text style={getCountryStyle('JP')}>
          <tspan x="822" y="159.39">
            JP
          </tspan>
        </text>
      </g>
      <g
        id="JO"
        fill={getCountryFill('JO')}
        onMouseOver={(e) => showCountryDatum(e, 'JO')}
        onMouseOut={(e) => hideCountryDatum(e, 'JO')}
      >
        <path d="M576.139 216.171H552.139V240.171H576.139V216.171Z" />
        <text style={getCountryStyle('JO')}>
          <tspan x="558" y="231.39">
            JO
          </tspan>
        </text>
      </g>
      <g
        id="KZ"
        fill={getCountryFill('KZ')}
        onMouseOver={(e) => showCountryDatum(e, 'KZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'KZ')}
      >
        <path d="M720.139 120.171H696.139V144.171H720.139V120.171Z" />
        <text style={getCountryStyle('KZ')}>
          <tspan x="702" y="135.269">
            KZ
          </tspan>
        </text>
      </g>
      <g
        id="KW"
        fill={getCountryFill('KW')}
        onMouseOver={(e) => showCountryDatum(e, 'KW')}
        onMouseOut={(e) => hideCountryDatum(e, 'KW')}
      >
        <path d="M600 216H576V240H600V216Z" />
        <text style={getCountryStyle('KW')}>
          <tspan x="576.861" y="230.511">
            KW
          </tspan>
        </text>
      </g>
      <g
        id="KG"
        fill={getCountryFill('KG')}
        onMouseOver={(e) => showCountryDatum(e, 'KG')}
        onMouseOut={(e) => hideCountryDatum(e, 'KG')}
      >
        <path d="M696 144H672V168H696V144Z" />
        <text style={getCountryStyle('KG')}>
          <tspan x="677" y="159.39">
            KG
          </tspan>
        </text>
      </g>
      <g
        id="LA"
        fill={getCountryFill('LA')}
        onMouseOver={(e) => showCountryDatum(e, 'LA')}
        onMouseOut={(e) => hideCountryDatum(e, 'LA')}
      >
        <path d="M744 216H720V240H744V216Z" />
        <text style={getCountryStyle('LA')}>
          <tspan x="726" y="231.39">
            LA
          </tspan>
        </text>
      </g>
      <g
        id="LB"
        fill={getCountryFill('LB')}
        onMouseOver={(e) => showCountryDatum(e, 'LB')}
        onMouseOut={(e) => hideCountryDatum(e, 'LB')}
      >
        <path d="M576.139 240.171H552.139V264.171H576.139V240.171Z" />
        <text style={getCountryStyle('LB')}>
          <tspan x="558" y="255.39">
            LB
          </tspan>
        </text>
      </g>
      <g
        id="MY"
        fill={getCountryFill('MY')}
        onMouseOver={(e) => showCountryDatum(e, 'MY')}
        onMouseOut={(e) => hideCountryDatum(e, 'MY')}
      >
        <path d="M720 264H696V288H720V264Z" />
        <text style={getCountryStyle('MY')}>
          <tspan x="700" y="279.39">
            MY
          </tspan>
        </text>
      </g>
      <g
        id="MV"
        fill={getCountryFill('MV')}
        onMouseOver={(e) => showCountryDatum(e, 'MV')}
        onMouseOut={(e) => hideCountryDatum(e, 'MV')}
      >
        <path d="M672 264H648V288H672V264Z" />
        <text style={getCountryStyle('MV')}>
          <tspan x="653" y="279.39">
            MV
          </tspan>
        </text>
      </g>
      <g
        id="MN"
        fill={getCountryFill('MN')}
        onMouseOver={(e) => showCountryDatum(e, 'MN')}
        onMouseOut={(e) => hideCountryDatum(e, 'MN')}
      >
        <path d="M744 120H720V144H744V120Z" />
        <text style={getCountryStyle('MN')}>
          <tspan x="724" y="135.085">
            MN
          </tspan>
        </text>
      </g>
      <g
        id="NP"
        fill={getCountryFill('NP')}
        onMouseOver={(e) => showCountryDatum(e, 'NP')}
        onMouseOut={(e) => hideCountryDatum(e, 'NP')}
      >
        <path d="M696 216H672V240H696V216Z" />
        <text style={getCountryStyle('NP')}>
          <tspan x="677" y="231.39">
            NP
          </tspan>
        </text>
      </g>
      <g
        id="OM"
        fill={getCountryFill('OM')}
        onMouseOver={(e) => showCountryDatum(e, 'OM')}
        onMouseOut={(e) => hideCountryDatum(e, 'OM')}
      >
        <path d="M600 287.829H576V311.829H600V287.829Z" />
        <text style={getCountryStyle('OM')}>
          <tspan x="580" y="302.295">
            OM
          </tspan>
        </text>
      </g>
      <g
        id="PK"
        fill={getCountryFill('PK')}
        onMouseOver={(e) => showCountryDatum(e, 'PK')}
        onMouseOut={(e) => hideCountryDatum(e, 'PK')}
      >
        <path d="M672 192H648V216H672V192Z" />
        <text style={getCountryStyle('PK')}>
          <tspan x="651" y="207.085">
            PK
          </tspan>
        </text>
      </g>
      <g
        id="PS"
        fill={getCountryFill('PS')}
        onMouseOver={(e) => showCountryDatum(e, 'PS')}
        onMouseOut={(e) => hideCountryDatum(e, 'PS')}
      >
        <path d="M624 192H600V216H624V192Z" />
        <text style={getCountryStyle('PS')}>
          <tspan x="606" y="207.39">
            PS
          </tspan>
        </text>
      </g>
      <g
        id="PH"
        fill={getCountryFill('PH')}
        onMouseOver={(e) => showCountryDatum(e, 'PH')}
        onMouseOut={(e) => hideCountryDatum(e, 'PH')}
      >
        <path d="M768 264H744V288H768V264Z" />
        <text style={getCountryStyle('PH')}>
          <tspan x="749" y="279.39">
            PH
          </tspan>
        </text>
      </g>
      <g
        id="QA"
        fill={getCountryFill('QA')}
        onMouseOver={(e) => showCountryDatum(e, 'QA')}
        onMouseOut={(e) => hideCountryDatum(e, 'QA')}
      >
        <path d="M600 263.829H576V287.829H600V263.829Z" />
        <text style={getCountryStyle('QA')}>
          <tspan x="580.861" y="279.048">
            QA
          </tspan>
        </text>
      </g>
      <g
        id="RU"
        fill={getCountryFill('RU')}
        onMouseOver={(e) => showCountryDatum(e, 'RU')}
        onMouseOut={(e) => hideCountryDatum(e, 'RU')}
      >
        <path d="M744 96H720V120H744V96Z" />
        <text style={getCountryStyle('RU')}>
          <tspan x="725" y="111.085">
            RU
          </tspan>
        </text>
      </g>
      <g
        id="SA"
        fill={getCountryFill('SA')}
        onMouseOver={(e) => showCountryDatum(e, 'SA')}
        onMouseOut={(e) => hideCountryDatum(e, 'SA')}
      >
        <path d="M600.139 240H576.139V264H600.139V240Z" />
        <text style={getCountryStyle('SA')}>
          <tspan x="582" y="255.219">
            SA
          </tspan>
        </text>
      </g>
      <g
        id="SG"
        fill={getCountryFill('SG')}
        onMouseOver={(e) => showCountryDatum(e, 'SG')}
        onMouseOut={(e) => hideCountryDatum(e, 'SG')}
      >
        <path d="M720 312H696V336H720V312Z" />
        <text style={getCountryStyle('SG')}>
          <tspan x="701" y="327.39">
            SG
          </tspan>
        </text>
      </g>
      <g
        id="CX"
        fill={getCountryFill('CX')}
        onMouseOver={(e) => showCountryDatum(e, 'CX')}
        onMouseOut={(e) => hideCountryDatum(e, 'CX')}
      >
        <path d="M720 336H696V360H720V336Z" />
        <text style={getCountryStyle('CX')}>
          <tspan x="701" y="351.39">
            CX
          </tspan>
        </text>
      </g>
      <g
        id="LK"
        fill={getCountryFill('LK')}
        onMouseOver={(e) => showCountryDatum(e, 'LK')}
        onMouseOut={(e) => hideCountryDatum(e, 'LK')}
      >
        <path d="M672 240H648V264H672V240Z" />
        <text style={getCountryStyle('LK')}>
          <tspan x="654" y="255.39">
            LK
          </tspan>
        </text>
      </g>
      <g
        id="SY"
        fill={getCountryFill('SY')}
        onMouseOver={(e) => showCountryDatum(e, 'SY')}
        onMouseOut={(e) => hideCountryDatum(e, 'SY')}
      >
        <path d="M600 192H576V216H600V192Z" />
        <text style={getCountryStyle('SY')}>
          <tspan x="582" y="207.39">
            SY
          </tspan>
        </text>
      </g>
      <g
        id="TW"
        fill={getCountryFill('TW')}
        onMouseOver={(e) => showCountryDatum(e, 'TW')}
        onMouseOut={(e) => hideCountryDatum(e, 'TW')}
      >
        <path d="M840 168H816V192H840V168Z" />
        <text style={getCountryStyle('TW')}>
          <tspan x="821" y="183.39">
            TW
          </tspan>
        </text>
      </g>
      <g
        id="TJ"
        fill={getCountryFill('TJ')}
        onMouseOver={(e) => showCountryDatum(e, 'TJ')}
        onMouseOut={(e) => hideCountryDatum(e, 'TJ')}
      >
        <path d="M696 168H672V192H696V168Z" />
        <text style={getCountryStyle('TJ')}>
          <tspan x="679" y="183.39">
            TJ
          </tspan>
        </text>
      </g>
      <g
        id="TH"
        fill={getCountryFill('TH')}
        onMouseOver={(e) => showCountryDatum(e, 'TH')}
        onMouseOut={(e) => hideCountryDatum(e, 'TH')}
      >
        <path d="M720 240H696V264H720V240Z" />
        <text style={getCountryStyle('TH')}>
          <tspan x="702" y="255.39">
            TH
          </tspan>
        </text>
      </g>
      <g
        id="TR"
        fill={getCountryFill('TR')}
        onMouseOver={(e) => showCountryDatum(e, 'TR')}
        onMouseOut={(e) => hideCountryDatum(e, 'TR')}
      >
        <path d="M576.139 192.171H552.139V216.171H576.139V192.171Z" />
        <text style={getCountryStyle('TR')}>
          <tspan x="558" y="207.39">
            TR
          </tspan>
        </text>
      </g>
      <g
        id="TM"
        fill={getCountryFill('TM')}
        onMouseOver={(e) => showCountryDatum(e, 'TM')}
        onMouseOut={(e) => hideCountryDatum(e, 'TM')}
      >
        <path d="M672 168H648V192H672V168Z" />
        <text style={getCountryStyle('TM')}>
          <tspan x="653" y="183.39">
            TM
          </tspan>
        </text>
      </g>
      <g
        id="AE"
        fill={getCountryFill('AE')}
        onMouseOver={(e) => showCountryDatum(e, 'AE')}
        onMouseOut={(e) => hideCountryDatum(e, 'AE')}
      >
        <path d="M624 263.829H600V287.829H624V263.829Z" />
        <text style={getCountryStyle('AE')}>
          <tspan x="605.861" y="278.372">
            AE
          </tspan>
        </text>
      </g>
      <g
        id="UZ"
        fill={getCountryFill('UZ')}
        onMouseOver={(e) => showCountryDatum(e, 'UZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'UZ')}
      >
        <path d="M672 144H648V168H672V144Z" />
        <text style={getCountryStyle('UZ')}>
          <tspan x="653" y="159.39">
            UZ
          </tspan>
        </text>
      </g>
      <g
        id="VN"
        fill={getCountryFill('VN')}
        onMouseOver={(e) => showCountryDatum(e, 'VN')}
        onMouseOut={(e) => hideCountryDatum(e, 'VN')}
      >
        <path d="M768 216H744V240H768V216Z" />
        <text style={getCountryStyle('VN')}>
          <tspan x="749" y="231.39">
            VN
          </tspan>
        </text>
      </g>
      <g
        id="YE"
        fill={getCountryFill('YE')}
        onMouseOver={(e) => showCountryDatum(e, 'YE')}
        onMouseOut={(e) => hideCountryDatum(e, 'YE')}
      >
        <path d="M576 288H552V312H576V288Z" />
        <text style={getCountryStyle('YE')}>
          <tspan x="558" y="303.085">
            YE
          </tspan>
        </text>
      </g>
      <g
        id="HK"
        fill={getCountryFill('HK')}
        onMouseOver={(e) => showCountryDatum(e, 'HK')}
        onMouseOut={(e) => hideCountryDatum(e, 'HK')}
      >
        <path d="M816 192H792V216H816V192Z" />
        <text style={getCountryStyle('HK')}>
          <tspan x="797" y="207.39">
            HK
          </tspan>
        </text>
      </g>
      <g
        id="MO"
        fill={getCountryFill('MO')}
        onMouseOver={(e) => showCountryDatum(e, 'MO')}
        onMouseOut={(e) => hideCountryDatum(e, 'MO')}
      >
        <path d="M792 192H768V216H792V192Z" />
        <text style={getCountryStyle('MO')}>
          <tspan x="772" y="207.39">
            MO
          </tspan>
        </text>
      </g>
      <g
        id="MM"
        fill={getCountryFill('MM')}
        onMouseOver={(e) => showCountryDatum(e, 'MM')}
        onMouseOut={(e) => hideCountryDatum(e, 'MM')}
      >
        <path d="M720 192H696V216H720V192Z" />
        <text style={getCountryStyle('MM')}>
          <tspan x="699" y="207.085">
            MM
          </tspan>
        </text>
      </g>
      <g
        id="KP"
        fill={getCountryFill('KP')}
        onMouseOver={(e) => showCountryDatum(e, 'KP')}
        onMouseOut={(e) => hideCountryDatum(e, 'KP')}
      >
        <path d="M744 144H720V168H744V144Z" />
        <text style={getCountryStyle('KP')}>
          <tspan x="725" y="159.085">
            KP
          </tspan>
        </text>
      </g>
      <g
        id="KR"
        fill={getCountryFill('KR')}
        onMouseOver={(e) => showCountryDatum(e, 'KR')}
        onMouseOut={(e) => hideCountryDatum(e, 'KR')}
      >
        <path d="M744 168H720V192H744V168Z" />
        <text style={getCountryStyle('KR')}>
          <tspan x="725" y="183.39">
            KR
          </tspan>
        </text>
      </g>
    </g>
  )
}
