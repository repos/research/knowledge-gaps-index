import { scaleThreshold } from 'd3'
import { texts, formatNumbersWithCommas } from 'lib'

interface AfricaProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Africa = ({
  maxValue,
  minValue,
  values,
  setHoverValue,
}: AfricaProps) => {
  const delta = maxValue - minValue
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])

  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  return (
    <g id="Africa">
      <g
        id="DZ"
        fill={getCountryFill('DZ')}
        onMouseOver={(e) => showCountryDatum(e, 'DZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'DZ')}
      >
        <path d="M480 288H456V312H480V288Z" />
        <text style={getCountryStyle('DZ')}>
          <tspan x="461" y="303.085">
            DZ
          </tspan>
        </text>
      </g>
      <g
        id="EG"
        fill={getCountryFill('EG')}
        onMouseOver={(e) => showCountryDatum(e, 'EG')}
        onMouseOut={(e) => hideCountryDatum(e, 'EG')}
      >
        <path d="M552 288H528V312H552V288Z" />
        <text style={getCountryStyle('EG')}>
          <tspan x="533" y="303.085">
            EG
          </tspan>
        </text>
      </g>
      <g
        id="LY"
        fill={getCountryFill('LY')}
        onMouseOver={(e) => showCountryDatum(e, 'LY')}
        onMouseOut={(e) => hideCountryDatum(e, 'LY')}
      >
        <path d="M528 288H504V312H528V288Z" />
        <text style={getCountryStyle('LY')}>
          <tspan x="507" y="303.085">
            LY
          </tspan>
        </text>
      </g>
      <g
        id="MA"
        fill={getCountryFill('MA')}
        onMouseOver={(e) => showCountryDatum(e, 'MA')}
        onMouseOut={(e) => hideCountryDatum(e, 'MA')}
      >
        <path d="M456 288H432V312H456V288Z" />
        <text style={getCountryStyle('MA')}>
          <tspan x="436" y="303.39">
            MA
          </tspan>
        </text>
      </g>
      <g
        id="TN"
        fill={getCountryFill('TN')}
        onMouseOver={(e) => showCountryDatum(e, 'TN')}
        onMouseOut={(e) => hideCountryDatum(e, 'TN')}
      >
        <path d="M504 288H480V312H504V288Z" />
        <text style={getCountryStyle('TN')}>
          <tspan x="486" y="303.085">
            TN
          </tspan>
        </text>
      </g>
      <g
        id="MR"
        fill={getCountryFill('MR')}
        onMouseOver={(e) => showCountryDatum(e, 'MR')}
        onMouseOut={(e) => hideCountryDatum(e, 'MR')}
      >
        <path d="M432 312H408V336H432V312Z" />
        <text style={getCountryStyle('MR')}>
          <tspan x="412" y="327.39">
            MR
          </tspan>
        </text>
      </g>
      <g
        id="SN"
        fill={getCountryFill('SN')}
        onMouseOver={(e) => showCountryDatum(e, 'SN')}
        onMouseOut={(e) => hideCountryDatum(e, 'SN')}
      >
        <path d="M480 312H456V336H480V312Z" />
        <text style={getCountryStyle('SN')}>
          <tspan x="461" y="327.39">
            SN
          </tspan>
        </text>
      </g>
      <g
        id="ML"
        fill={getCountryFill('ML')}
        onMouseOver={(e) => showCountryDatum(e, 'ML')}
        onMouseOut={(e) => hideCountryDatum(e, 'ML')}
      >
        <path d="M504 312H480V336H504V312Z" />
        <text style={getCountryStyle('ML')}>
          <tspan x="485" y="327.39">
            ML
          </tspan>
        </text>
      </g>
      <g
        id="NE"
        fill={getCountryFill('NE')}
        onMouseOver={(e) => showCountryDatum(e, 'NE')}
        onMouseOut={(e) => hideCountryDatum(e, 'NE')}
      >
        <path d="M528 312H504V336H528V312Z" />
        <text style={getCountryStyle('NE')}>
          <tspan x="509" y="327.39">
            NE
          </tspan>
        </text>
      </g>
      <g
        id="EH"
        fill={getCountryFill('EH')}
        onMouseOver={(e) => showCountryDatum(e, 'EH')}
        onMouseOut={(e) => hideCountryDatum(e, 'EH')}
      >
        <path d="M432 288H408V312H432V288Z" />
        <text style={getCountryStyle('EH')}>
          <tspan x="413" y="303.39">
            EH
          </tspan>
        </text>
      </g>
      <g
        id="GW"
        fill={getCountryFill('GW')}
        onMouseOver={(e) => showCountryDatum(e, 'GW')}
        onMouseOut={(e) => hideCountryDatum(e, 'GW')}
      >
        <path d="M432 336H408V360H432V336Z" />
        <text style={getCountryStyle('GW')}>
          <tspan x="412" y="351.39">
            GW
          </tspan>
        </text>
      </g>
      <g
        id="SL"
        fill={getCountryFill('SL')}
        onMouseOver={(e) => showCountryDatum(e, 'SL')}
        onMouseOut={(e) => hideCountryDatum(e, 'SL')}
      >
        <path d="M456 336H432V360H456V336Z" />
        <text style={getCountryStyle('SL')}>
          <tspan x="438" y="351.39">
            SL
          </tspan>
        </text>
      </g>
      <g
        id="BF"
        fill={getCountryFill('BF')}
        onMouseOver={(e) => showCountryDatum(e, 'BF')}
        onMouseOut={(e) => hideCountryDatum(e, 'BF')}
      >
        <path d="M480 336H456V360H480V336Z" />
        <text style={getCountryStyle('BF')}>
          <tspan x="462" y="351.39">
            BF
          </tspan>
        </text>
      </g>
      <g
        id="TD"
        fill={getCountryFill('TD')}
        onMouseOver={(e) => showCountryDatum(e, 'TD')}
        onMouseOut={(e) => hideCountryDatum(e, 'TD')}
      >
        <path d="M504 336H480V360H504V336Z" />
        <text style={getCountryStyle('TD')}>
          <tspan x="486" y="351.39">
            TD
          </tspan>
        </text>
      </g>
      <g
        id="GN"
        fill={getCountryFill('GN')}
        onMouseOver={(e) => showCountryDatum(e, 'GN')}
        onMouseOut={(e) => hideCountryDatum(e, 'GN')}
      >
        <path d="M432 360H408V384H432V360Z" />
        <text style={getCountryStyle('GN')}>
          <tspan x="411" y="375.085">
            GN
          </tspan>
        </text>
      </g>
      <g
        id="LR"
        fill={getCountryFill('LR')}
        onMouseOver={(e) => showCountryDatum(e, 'LR')}
        onMouseOut={(e) => hideCountryDatum(e, 'LR')}
      >
        <path d="M456 360H432V384H456V360Z" />
        <text style={getCountryStyle('LR')}>
          <tspan x="438" y="375.39">
            LR
          </tspan>
        </text>
      </g>
      <g
        id="GH"
        fill={getCountryFill('GH')}
        onMouseOver={(e) => showCountryDatum(e, 'GH')}
        onMouseOut={(e) => hideCountryDatum(e, 'GH')}
      >
        <path d="M480 360H456V384H480V360Z" />
        <text style={getCountryStyle('GH')}>
          <tspan x="461" y="375.39">
            GH
          </tspan>
        </text>
      </g>
      <g
        id="TG"
        fill={getCountryFill('TG')}
        onMouseOver={(e) => showCountryDatum(e, 'TG')}
        onMouseOut={(e) => hideCountryDatum(e, 'TG')}
      >
        <path d="M504 360H480V384H504V360Z" />
        <text style={getCountryStyle('TG')}>
          <tspan x="485" y="375.39">
            TG
          </tspan>
        </text>
      </g>
      <g
        id="BJ"
        fill={getCountryFill('BJ')}
        onMouseOver={(e) => showCountryDatum(e, 'BJ')}
        onMouseOut={(e) => hideCountryDatum(e, 'BJ')}
      >
        <path d="M528 360H504V384H528V360Z" />
        <text style={getCountryStyle('BJ')}>
          <tspan x="510" y="375.39">
            BJ
          </tspan>
        </text>
      </g>
      <g
        id="CV"
        fill={getCountryFill('CV')}
        onMouseOver={(e) => showCountryDatum(e, 'CV')}
        onMouseOut={(e) => hideCountryDatum(e, 'CV')}
      >
        <path d="M408 384H384V408H408V384Z" />
        <text style={getCountryStyle('CV')}>
          <tspan x="386" y="399.343">
            CV
          </tspan>
        </text>
      </g>
      <g
        id="SD"
        fill={getCountryFill('SD')}
        onMouseOver={(e) => showCountryDatum(e, 'SD')}
        onMouseOut={(e) => hideCountryDatum(e, 'SD')}
      >
        <path d="M552 312H528V336H552V312Z" />
        <text style={getCountryStyle('SD')}>
          <tspan x="533" y="327.39">
            SD
          </tspan>
        </text>
      </g>
      <g
        id="ER"
        fill={getCountryFill('ER')}
        onMouseOver={(e) => showCountryDatum(e, 'ER')}
        onMouseOut={(e) => hideCountryDatum(e, 'ER')}
      >
        <path d="M552 336H528V360H552V336Z" />
        <text style={getCountryStyle('ER')}>
          <tspan x="534" y="351.39">
            ER
          </tspan>
        </text>
      </g>
      <g
        id="DJ"
        fill={getCountryFill('DJ')}
        onMouseOver={(e) => showCountryDatum(e, 'DJ')}
        onMouseOut={(e) => hideCountryDatum(e, 'DJ')}
      >
        <path d="M576 336H552V360H576V336Z" />
        <text style={getCountryStyle('DJ')}>
          <tspan x="558" y="351.39">
            DJ
          </tspan>
        </text>
      </g>
      <g
        id="ET"
        fill={getCountryFill('ET')}
        onMouseOver={(e) => showCountryDatum(e, 'ET')}
        onMouseOut={(e) => hideCountryDatum(e, 'ET')}
      >
        <path d="M576 360H552V384H576V360Z" />
        <text style={getCountryStyle('ET')}>
          <tspan x="558" y="375.39">
            ET
          </tspan>
        </text>
      </g>
      <g
        id="SO"
        fill={getCountryFill('SO')}
        onMouseOver={(e) => showCountryDatum(e, 'SO')}
        onMouseOut={(e) => hideCountryDatum(e, 'SO')}
      >
        <path d="M600 360H576V384H600V360Z" />
        <text style={getCountryStyle('SO')}>
          <tspan x="581" y="375.39">
            SO
          </tspan>
        </text>
      </g>
      <g
        id="NG"
        fill={getCountryFill('NG')}
        onMouseOver={(e) => showCountryDatum(e, 'NG')}
        onMouseOut={(e) => hideCountryDatum(e, 'NG')}
      >
        <path d="M480 384H456V408H480V384Z" />
        <text style={getCountryStyle('NG')}>
          <tspan x="461" y="399.39">
            NG
          </tspan>
        </text>
      </g>
      <g
        id="GM"
        fill={getCountryFill('GM')}
        onMouseOver={(e) => showCountryDatum(e, 'GM')}
        onMouseOut={(e) => hideCountryDatum(e, 'GM')}
      >
        <path d="M456 312H432V336H456V312Z" />
        <text style={getCountryStyle('GM')}>
          <tspan x="436" y="327.39">
            GM
          </tspan>
        </text>
      </g>
      <g
        id="CI"
        fill={getCountryFill('CI')}
        onMouseOver={(e) => showCountryDatum(e, 'CI')}
        onMouseOut={(e) => hideCountryDatum(e, 'CI')}
      >
        <path d="M456 384H432V408H456V384Z" />
        <text style={getCountryStyle('CI')}>
          <tspan x="439" y="399.39">
            CI
          </tspan>
        </text>
      </g>
      <g
        id="CM"
        fill={getCountryFill('CM')}
        onMouseOver={(e) => showCountryDatum(e, 'CM')}
        onMouseOut={(e) => hideCountryDatum(e, 'CM')}
      >
        <path d="M504 384H480V408H504V384Z" />
        <text style={getCountryStyle('CM')}>
          <tspan x="484" y="398.622">
            CM
          </tspan>
        </text>
      </g>
      <g
        id="SS"
        fill={getCountryFill('SS')}
        onMouseOver={(e) => showCountryDatum(e, 'SS')}
        onMouseOut={(e) => hideCountryDatum(e, 'SS')}
      >
        <path d="M528 336H504V360H528V336Z" />
        <text style={getCountryStyle('SS')}>
          <tspan x="510" y="350.614">
            SS
          </tspan>
        </text>
      </g>
      <g
        id="CF"
        fill={getCountryFill('CF')}
        onMouseOver={(e) => showCountryDatum(e, 'CF')}
        onMouseOut={(e) => hideCountryDatum(e, 'CF')}
      >
        <path d="M552 360H528V384H552V360Z" />
        <text style={getCountryStyle('CF')}>
          <tspan x="534" y="375.39">
            CF
          </tspan>
        </text>
      </g>
      <g
        id="GQ"
        fill={getCountryFill('GQ')}
        onMouseOver={(e) => showCountryDatum(e, 'GQ')}
        onMouseOut={(e) => hideCountryDatum(e, 'GQ')}
      >
        <path d="M480 408H456V432H480V408Z" />
        <text style={getCountryStyle('GQ')}>
          <tspan x="460" y="423.39">
            GQ
          </tspan>
        </text>
      </g>
      <g
        id="GA"
        fill={getCountryFill('GA')}
        onMouseOver={(e) => showCountryDatum(e, 'GA')}
        onMouseOut={(e) => hideCountryDatum(e, 'GA')}
      >
        <path d="M504 432H480V456H504V432Z" />
        <text style={getCountryStyle('GA')}>
          <tspan x="485" y="447.39">
            GA
          </tspan>
        </text>
      </g>
      <g
        id="CG"
        fill={getCountryFill('CG')}
        onMouseOver={(e) => showCountryDatum(e, 'CG')}
        onMouseOut={(e) => hideCountryDatum(e, 'CG')}
      >
        <path d="M504 408H480V432H504V408Z" />
        <text style={getCountryStyle('CG')}>
          <tspan x="485" y="423.39">
            CG
          </tspan>
        </text>
      </g>
      <g
        id="UG"
        fill={getCountryFill('UG')}
        onMouseOver={(e) => showCountryDatum(e, 'UG')}
        onMouseOut={(e) => hideCountryDatum(e, 'UG')}
      >
        <path d="M552 384H528V408H552V384Z" />
        <text style={getCountryStyle('UG')}>
          <tspan x="529" y="399.085">
            UG
          </tspan>
        </text>
      </g>
      <g
        id="CD"
        fill={getCountryFill('CD')}
        onMouseOver={(e) => showCountryDatum(e, 'CD')}
        onMouseOut={(e) => hideCountryDatum(e, 'CD')}
      >
        <path d="M528 384H504V408H528V384Z" />
        <text style={getCountryStyle('CD')}>
          <tspan x="509" y="399.085">
            CD
          </tspan>
        </text>
      </g>
      <g
        id="RW"
        fill={getCountryFill('RW')}
        onMouseOver={(e) => showCountryDatum(e, 'RW')}
        onMouseOut={(e) => hideCountryDatum(e, 'RW')}
      >
        <path d="M552 408H528V432H552V408Z" />
        <text style={getCountryStyle('RW')}>
          <tspan x="532" y="423.39">
            RW
          </tspan>
        </text>
      </g>
      <g
        id="AO"
        fill={getCountryFill('AO')}
        onMouseOver={(e) => showCountryDatum(e, 'AO')}
        onMouseOut={(e) => hideCountryDatum(e, 'AO')}
      >
        <path d="M480 432H456V456H480V432Z" />
        <text style={getCountryStyle('AO')}>
          <tspan x="461" y="447.085">
            AO
          </tspan>
        </text>
      </g>
      <g
        id="MW"
        fill={getCountryFill('MW')}
        onMouseOver={(e) => showCountryDatum(e, 'MW')}
        onMouseOut={(e) => hideCountryDatum(e, 'MW')}
      >
        <path d="M528 432H504V456H528V432Z" />
        <text style={getCountryStyle('MW')}>
          <tspan x="507" y="447.085">
            MW
          </tspan>
        </text>
      </g>
      <g
        id="MZ"
        fill={getCountryFill('MZ')}
        onMouseOver={(e) => showCountryDatum(e, 'MZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'MZ')}
      >
        <path d="M552 432H528V456H552V432Z" />
        <text style={getCountryStyle('MZ')}>
          <tspan x="533" y="447.39">
            MZ
          </tspan>
        </text>
      </g>
      <g
        id="BI"
        fill={getCountryFill('BI')}
        onMouseOver={(e) => showCountryDatum(e, 'BI')}
        onMouseOut={(e) => hideCountryDatum(e, 'BI')}
      >
        <path d="M528 408H504V432H528V408Z" />
        <text style={getCountryStyle('BI')}>
          <tspan x="511" y="423.39">
            BI
          </tspan>
        </text>
      </g>
      <g
        id="BW"
        fill={getCountryFill('BW')}
        onMouseOver={(e) => showCountryDatum(e, 'BW')}
        onMouseOut={(e) => hideCountryDatum(e, 'BW')}
      >
        <path d="M528 456H504V480H528V456Z" />
        <text style={getCountryStyle('BW')}>
          <tspan x="508" y="471.085">
            BW
          </tspan>
        </text>
      </g>
      <g
        id="ZW"
        fill={getCountryFill('ZW')}
        onMouseOver={(e) => showCountryDatum(e, 'ZW')}
        onMouseOut={(e) => hideCountryDatum(e, 'ZW')}
      >
        <path d="M552 456H528V480H552V456Z" />
        <text style={getCountryStyle('ZW')}>
          <tspan x="532" y="471.085">
            ZW
          </tspan>
        </text>
      </g>
      <g
        id="KM"
        fill={getCountryFill('KM')}
        onMouseOver={(e) => showCountryDatum(e, 'KM')}
        onMouseOut={(e) => hideCountryDatum(e, 'KM')}
      >
        <path d="M605 461H581V485H605V461Z" />
        <text style={getCountryStyle('KM')}>
          <tspan x="585" y="476.085">
            KM
          </tspan>
        </text>
      </g>
      <g
        id="SC"
        fill={getCountryFill('SC')}
        onMouseOver={(e) => showCountryDatum(e, 'SC')}
        onMouseOut={(e) => hideCountryDatum(e, 'SC')}
      >
        <path d="M605 432H581V456H605V432Z" />
        <text style={getCountryStyle('SC')}>
          <tspan x="586" y="446.758">
            SC
          </tspan>
        </text>
      </g>
      <g
        id="LS"
        fill={getCountryFill('LS')}
        onMouseOver={(e) => showCountryDatum(e, 'LS')}
        onMouseOut={(e) => hideCountryDatum(e, 'LS')}
      >
        <path d="M576 480H552V504H576V480Z" />
        <text style={getCountryStyle('LS')}>
          <tspan x="558" y="495.085">
            LS
          </tspan>
        </text>
      </g>
      <g
        id="NA"
        fill={getCountryFill('NA')}
        onMouseOver={(e) => showCountryDatum(e, 'NA')}
        onMouseOut={(e) => hideCountryDatum(e, 'NA')}
      >
        <path d="M528 480H504V504H528V480Z" />
        <text style={getCountryStyle('NA')}>
          <tspan x="509" y="494.898">
            NA
          </tspan>
        </text>
      </g>
      <g
        id="ZM"
        fill={getCountryFill('ZM')}
        onMouseOver={(e) => showCountryDatum(e, 'ZM')}
        onMouseOut={(e) => hideCountryDatum(e, 'ZM')}
      >
        <path d="M504 456H480V480H504V456Z" />
        <text style={getCountryStyle('ZM')}>
          <tspan x="485" y="471.085">
            ZM
          </tspan>
        </text>
      </g>
      <g
        id="KE"
        fill={getCountryFill('KE')}
        onMouseOver={(e) => showCountryDatum(e, 'KE')}
        onMouseOut={(e) => hideCountryDatum(e, 'KE')}
      >
        <path d="M576 384H552V408H576V384Z" />
        <text style={getCountryStyle('KE')}>
          <tspan x="554" y="399.085">
            KE
          </tspan>
        </text>
      </g>
      <g
        id="ZA"
        fill={getCountryFill('ZA')}
        onMouseOver={(e) => showCountryDatum(e, 'ZA')}
        onMouseOut={(e) => hideCountryDatum(e, 'ZA')}
      >
        <path d="M552 504H528V528H552V504Z" />
        <text style={getCountryStyle('ZA')}>
          <tspan x="534" y="519.39">
            ZA
          </tspan>
        </text>
      </g>
      <g
        id="MU"
        fill={getCountryFill('MU')}
        onMouseOver={(e) => showCountryDatum(e, 'MU')}
        onMouseOut={(e) => hideCountryDatum(e, 'MU')}
      >
        <path d="M629 509H605V533H629V509Z" />
        <text style={getCountryStyle('MU')}>
          <tspan x="609" y="524.085">
            MU
          </tspan>
        </text>
      </g>
      <g
        id="MG"
        fill={getCountryFill('MG')}
        onMouseOver={(e) => showCountryDatum(e, 'MG')}
        onMouseOut={(e) => hideCountryDatum(e, 'MG')}
      >
        <path d="M629 485H605V509H629V485Z" />
        <text style={getCountryStyle('MG')}>
          <tspan x="609" y="500.39">
            MG
          </tspan>
        </text>
      </g>
      <g
        id="TZ"
        fill={getCountryFill('TZ')}
        onMouseOver={(e) => showCountryDatum(e, 'TZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'TZ')}
      >
        <path d="M576 408H552V432H576V408Z" />
        <text style={getCountryStyle('TZ')}>
          <tspan x="558" y="423.39">
            TZ
          </tspan>
        </text>
      </g>
      <g
        id="YT"
        fill={getCountryFill('YT')}
        onMouseOver={(e) => showCountryDatum(e, 'YT')}
        onMouseOut={(e) => hideCountryDatum(e, 'YT')}
      >
        <path d="M629 461H605V485H629V461Z" />
        <text style={getCountryStyle('YT')}>
          <tspan x="611" y="476.085">
            YT
          </tspan>
        </text>
      </g>
      <g
        id="SH"
        fill={getCountryFill('SH')}
        onMouseOver={(e) => showCountryDatum(e, 'SH')}
        onMouseOut={(e) => hideCountryDatum(e, 'SH')}
      >
        <path d="M456 480H432V504H456V480Z" />
        <text style={getCountryStyle('SH')}>
          <tspan x="434" y="495.085">
            SH
          </tspan>
        </text>
      </g>
      <g
        id="ST"
        fill={getCountryFill('ST')}
        onMouseOver={(e) => showCountryDatum(e, 'ST')}
        onMouseOut={(e) => hideCountryDatum(e, 'ST')}
      >
        <path d="M451 413H427V437H451V413Z" />
        <text style={getCountryStyle('ST')}>
          <tspan x="433" y="428.085">
            ST
          </tspan>
        </text>
      </g>
      <g
        id="SZ"
        fill={getCountryFill('SZ')}
        onMouseOver={(e) => showCountryDatum(e, 'SZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'SZ')}
      >
        <path d="M576 456H552V480.015H576V456Z" />
        <text style={getCountryStyle('SZ')}>
          <tspan x="558" y="471.39">
            SZ
          </tspan>
        </text>
      </g>
      <g
        id="RE"
        fill={getCountryFill('RE')}
        onMouseOver={(e) => showCountryDatum(e, 'RE')}
        onMouseOut={(e) => hideCountryDatum(e, 'RE')}
      >
        <path d="M671.985 485H648V509H671.985V485Z" />
        <text style={getCountryStyle('RE')}>
          <tspan x="653" y="500.085">
            RE
          </tspan>
        </text>
      </g>
    </g>
  )
}
