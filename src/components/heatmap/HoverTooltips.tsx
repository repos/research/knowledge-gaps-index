import { formatNumbersWithCommas } from 'lib'
import { round } from 'lodash-es'

interface HoverTooltipsProps {
  subSelection: string
  hoverValue: string[]
  minValue: number
  maxValue: number
}

export const HoverTooltips = ({
  subSelection,
  hoverValue,
  minValue,
  maxValue,
}: HoverTooltipsProps) => {
  function getRoundedValue(value: number) {
    return subSelection === 'quality_score_value' ? round(value, 2) : value
  }
  const tooltipText = { fill: 'black' }
  const tooltipBold = { fontWeight: 'bold' }
  return (
    <g id="Hover tooltips" style={tooltipText}>
      <text x="0" y={520}>
        Current country:
        <tspan style={tooltipBold}> {hoverValue[1]}</tspan>
      </text>
      <text x="0" y={540}>
        Current value:
        <tspan style={tooltipBold}> {hoverValue[0]}</tspan>
      </text>
      <text x="0" y={570}>
        Smallest value:
        <tspan style={{ fill: '#e5abff' }}> ⬤</tspan> 
        {formatNumbersWithCommas(getRoundedValue(minValue))}
      </text>
      <text x="0" y={590}>
        Highest value:
        <tspan style={{ fill: '#4c208e' }}> ⬤</tspan> 
        {formatNumbersWithCommas(getRoundedValue(maxValue))}
      </text>
    </g>
  )
}
