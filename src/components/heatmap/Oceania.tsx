import { texts, formatNumbersWithCommas } from 'lib'
import { scaleThreshold } from 'd3'

interface OceaniaProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Oceania = ({
  values,
  maxValue,
  minValue,
  setHoverValue,
}: OceaniaProps) => {
  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const delta = maxValue - minValue
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])
  return (
    <g id="Oceania">
      <g
        id="AS"
        fill={getCountryFill('AS')}
        onMouseOver={(e) => showCountryDatum(e, 'AS')}
        onMouseOut={(e) => hideCountryDatum(e, 'AS')}
      >
        <path d="M48 312H24V336H48V312Z" />
        <text style={getCountryStyle('AS')}>
          <tspan x="30" y="327.39">
            AS
          </tspan>
        </text>
      </g>
      <g
        id="CK"
        fill={getCountryFill('CK')}
        onMouseOver={(e) => showCountryDatum(e, 'CK')}
        onMouseOut={(e) => hideCountryDatum(e, 'CK')}
      >
        <path d="M96 288H72V312H96V288Z" />
        <text style={getCountryStyle('CK')}>
          <tspan x="77" y="303.39">
            CK
          </tspan>
        </text>
      </g>
      <g
        id="FJ"
        fill={getCountryFill('FJ')}
        onMouseOver={(e) => showCountryDatum(e, 'FJ')}
        onMouseOut={(e) => hideCountryDatum(e, 'FJ')}
      >
        <path d="M864 480H840V504H864V480Z" />
        <text style={getCountryStyle('FJ')}>
          <tspan x="847" y="495.085">
            FJ
          </tspan>
        </text>
      </g>
      <g
        id="PF"
        fill={getCountryFill('PF')}
        onMouseOver={(e) => showCountryDatum(e, 'PF')}
        onMouseOut={(e) => hideCountryDatum(e, 'PF')}
      >
        <path d="M120 288H96V312H120V288Z" />
        <text style={getCountryStyle('PF')}>
          <tspan x="102" y="303.39">
            PF
          </tspan>
        </text>
      </g>
      <g
        id="GU"
        fill={getCountryFill('GU')}
        onMouseOver={(e) => showCountryDatum(e, 'GU')}
        onMouseOut={(e) => hideCountryDatum(e, 'GU')}
      >
        <path d="M792 408H768V432H792V408Z" />
        <text style={getCountryStyle('GU')}>
          <tspan x="773" y="423.085">
            GU
          </tspan>
        </text>
      </g>
      <g
        id="FM"
        fill={getCountryFill('FM')}
        onMouseOver={(e) => showCountryDatum(e, 'FM')}
        onMouseOut={(e) => hideCountryDatum(e, 'FM')}
      >
        <path d="M792 384H768V408H792V384Z" />
        <text style={getCountryStyle('FM')}>
          <tspan x="773" y="399.695">
            FM
          </tspan>
        </text>
      </g>
      <g
        id="KI"
        fill={getCountryFill('KI')}
        onMouseOver={(e) => showCountryDatum(e, 'KI')}
        onMouseOut={(e) => hideCountryDatum(e, 'KI')}
      >
        <path d="M72 240H48V264H72V240Z" />
        <text style={getCountryStyle('KI')}>
          <tspan x="55" y="255.39">
            KI
          </tspan>
        </text>
      </g>
      <g
        id="MH"
        fill={getCountryFill('MH')}
        onMouseOver={(e) => showCountryDatum(e, 'MH')}
        onMouseOut={(e) => hideCountryDatum(e, 'MH')}
      >
        <path d="M840 408H816V432H840V408Z" />
        <text style={getCountryStyle('MH')}>
          <tspan x="820" y="423.39">
            MH
          </tspan>
        </text>
      </g>
      <g
        id="NR"
        fill={getCountryFill('NR')}
        onMouseOver={(e) => showCountryDatum(e, 'NR')}
        onMouseOut={(e) => hideCountryDatum(e, 'NR')}
      >
        <path d="M840 432H816V456H840V432Z" />
        <text style={getCountryStyle('NR')}>
          <tspan x="821" y="447.39">
            NR
          </tspan>
        </text>
      </g>
      <g
        id="NC"
        fill={getCountryFill('NC')}
        onMouseOver={(e) => showCountryDatum(e, 'NC')}
        onMouseOut={(e) => hideCountryDatum(e, 'NC')}
      >
        <path d="M840 504H816V528H840V504Z" />
        <text style={getCountryStyle('NC')}>
          <tspan x="821" y="519.085">
            NC
          </tspan>
        </text>
      </g>
      <g
        id="NZ"
        fill={getCountryFill('NZ')}
        onMouseOver={(e) => showCountryDatum(e, 'NZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'NZ')}
      >
        <path d="M864 552H840V576H864V552Z" />
        <text style={getCountryStyle('NZ')}>
          <tspan x="845" y="567.085">
            NZ
          </tspan>
        </text>
      </g>
      <g
        id="NU"
        fill={getCountryFill('NU')}
        onMouseOver={(e) => showCountryDatum(e, 'NU')}
        onMouseOut={(e) => hideCountryDatum(e, 'NU')}
      >
        <path d="M48 336H24V360H48V336Z" />
        <text style={getCountryStyle('NU')}>
          <tspan x="29" y="351.39">
            NU
          </tspan>
        </text>
      </g>
      <g
        id="NF"
        fill={getCountryFill('NF')}
        onMouseOver={(e) => showCountryDatum(e, 'NF')}
        onMouseOut={(e) => hideCountryDatum(e, 'NF')}
      >
        <path d="M840 528H816V552H840V528Z" />
        <text style={getCountryStyle('NF')}>
          <tspan x="822" y="543.085">
            NF
          </tspan>
        </text>
      </g>
      <g
        id="PW"
        fill={getCountryFill('PW')}
        onMouseOver={(e) => showCountryDatum(e, 'PW')}
        onMouseOut={(e) => hideCountryDatum(e, 'PW')}
      >
        <path d="M768 432H744V456H768V432Z" />
        <text style={getCountryStyle('PW')}>
          <tspan x="748" y="447.085">
            PW
          </tspan>
        </text>
      </g>
      <g
        id="PG"
        fill={getCountryFill('PG')}
        onMouseOver={(e) => showCountryDatum(e, 'PG')}
        onMouseOut={(e) => hideCountryDatum(e, 'PG')}
      >
        <path d="M792 456H768V480H792V456Z" />
        <text style={getCountryStyle('PG')}>
          <tspan x="773" y="471.085">
            PG
          </tspan>
        </text>
      </g>
      <g
        id="PN"
        fill={getCountryFill('PN')}
        onMouseOver={(e) => showCountryDatum(e, 'PN')}
        onMouseOut={(e) => hideCountryDatum(e, 'PN')}
      >
        <path d="M144 312H120V336H144V312Z" />
        <text style={getCountryStyle('PN')}>
          <tspan x="125" y="327.39">
            PN
          </tspan>
        </text>
      </g>
      <g
        id="SB"
        fill={getCountryFill('SB')}
        onMouseOver={(e) => showCountryDatum(e, 'SB')}
        onMouseOut={(e) => hideCountryDatum(e, 'SB')}
      >
        <path d="M840 456H816V480H840V456Z" />
        <text style={getCountryStyle('SB')}>
          <tspan x="821" y="471.39">
            SB
          </tspan>
        </text>
      </g>
      <g
        id="TK"
        fill={getCountryFill('TK')}
        onMouseOver={(e) => showCountryDatum(e, 'TK')}
        onMouseOut={(e) => hideCountryDatum(e, 'TK')}
      >
        <path d="M48 264H24V288H48V264Z" />
        <text style={getCountryStyle('TK')}>
          <tspan x="30" y="279.39">
            TK
          </tspan>
        </text>
      </g>
      <g
        id="TO"
        fill={getCountryFill('TO')}
        onMouseOver={(e) => showCountryDatum(e, 'TO')}
        onMouseOut={(e) => hideCountryDatum(e, 'TO')}
      >
        <path d="M24 336H0V360H24V336Z" />
        <text style={getCountryStyle('TO')}>
          <tspan x="5" y="351.39">
            TO
          </tspan>
        </text>
      </g>
      <g
        id="TV"
        fill={getCountryFill('TV')}
        onMouseOver={(e) => showCountryDatum(e, 'TV')}
        onMouseOut={(e) => hideCountryDatum(e, 'TV')}
      >
        <path d="M864 456H840V480H864V456Z" />
        <text style={getCountryStyle('TV')}>
          <tspan x="846" y="471.085">
            TV
          </tspan>
        </text>
      </g>
      <g
        id="VU"
        fill={getCountryFill('VU')}
        onMouseOver={(e) => showCountryDatum(e, 'VU')}
        onMouseOut={(e) => hideCountryDatum(e, 'VU')}
      >
        <path d="M840 480H816V504H840V480Z" />
        <text style={getCountryStyle('VU')}>
          <tspan x="821" y="495.085">
            VU
          </tspan>
        </text>
      </g>
      <g
        id="WF"
        fill={getCountryFill('WF')}
        onMouseOver={(e) => showCountryDatum(e, 'WF')}
        onMouseOut={(e) => hideCountryDatum(e, 'WF')}
      >
        <path d="M24 288H0V312H24V288Z" />
        <text style={getCountryStyle('WF')}>
          <tspan x="5" y="303.39">
            WF
          </tspan>
        </text>
      </g>
      <g
        id="WS"
        fill={getCountryFill('WS')}
        onMouseOver={(e) => showCountryDatum(e, 'WS')}
        onMouseOut={(e) => hideCountryDatum(e, 'WS')}
      >
        <path d="M48 288H24V312H48V288Z" />
        <text style={getCountryStyle('WS')}>
          <tspan x="28" y="303.39">
            WS
          </tspan>
        </text>
      </g>
      <g
        id="MP"
        fill={getCountryFill('MP')}
        onMouseOver={(e) => showCountryDatum(e, 'MP')}
        onMouseOut={(e) => hideCountryDatum(e, 'MP')}
      >
        <path d="M696 485H672V509H696V485Z" />
        <text style={getCountryStyle('MP')}>
          <tspan x="676" y="500.39">
            MP
          </tspan>
        </text>
      </g>
      <g
        id="AU"
        fill={getCountryFill('AU')}
        onMouseOver={(e) => showCountryDatum(e, 'AU')}
        onMouseOut={(e) => hideCountryDatum(e, 'AU')}
      >
        <path d="M768 495H744V519H768V495Z" />
        <text style={getCountryStyle('AU')}>
          <tspan x="749" y="510.39">
            AU
          </tspan>
        </text>
      </g>
    </g>
  )
}
