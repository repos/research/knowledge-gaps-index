import { texts, formatNumbersWithCommas } from 'lib'
import { scaleThreshold } from 'd3'

interface AmericasProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Americas = ({
  maxValue,
  minValue,
  values,
  setHoverValue,
}: AmericasProps) => {
  const delta = maxValue - minValue
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])

  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  return (
    <g id="Americas">
      <g
        id="CU"
        fill={getCountryFill('CU')}
        onMouseOver={(e) => showCountryDatum(e, 'CU')}
        onMouseOut={(e) => hideCountryDatum(e, 'CU')}
      >
        <path d="M221 235H197V259.015H221V235Z" />
        <text id="CW" style={getCountryStyle('CW')}>
          <tspan x="201" y="250.085">
            CW
          </tspan>
        </text>
      </g>
      <g
        id="AR"
        fill={getCountryFill('AR')}
        onMouseOver={(e) => showCountryDatum(e, 'AR')}
        onMouseOut={(e) => hideCountryDatum(e, 'AR')}
      >
        <path d="M216 384H192V408H216V384Z" />
        <text style={getCountryStyle('AR')}>
          <tspan x="197" y="399.39">
            AR
          </tspan>
        </text>
      </g>
      <g
        id="FK"
        fill={getCountryFill('FK')}
        onMouseOver={(e) => showCountryDatum(e, 'FK')}
        onMouseOut={(e) => hideCountryDatum(e, 'FK')}
      >
        <path d="M245 384H221V408H245V384Z" />
        <text style={getCountryStyle('FK')}>
          <tspan x="226" y="399.39">
            FK
          </tspan>
        </text>
      </g>
      <g
        id="GS"
        fill={getCountryFill('GS')}
        onMouseOver={(e) => showCountryDatum(e, 'GS')}
        onMouseOut={(e) => hideCountryDatum(e, 'GS')}
      >
        <path d="M274 384H250V408H274V384Z" />
        <text style={getCountryStyle('GS')}>
          <tspan x="255" y="399.695">
            GS
          </tspan>
        </text>
      </g>
      <g
        id="BZ"
        fill={getCountryFill('BZ')}
        onMouseOver={(e) => showCountryDatum(e, 'BZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'BZ')}
      >
        <path d="M120 120H96V144H120V120Z" />
        <text style={getCountryStyle('BZ')}>
          <tspan x="102" y="135.182">
            BZ
          </tspan>
        </text>
      </g>
      <g
        id="BO"
        fill={getCountryFill('BO')}
        onMouseOver={(e) => showCountryDatum(e, 'BO')}
        onMouseOut={(e) => hideCountryDatum(e, 'BO')}
      >
        <path d="M216 312H192V336H216V312Z" />
        <text style={getCountryStyle('BO')}>
          <tspan x="197" y="327.39">
            BO
          </tspan>
        </text>
      </g>
      <g
        id="BR"
        fill={getCountryFill('BR')}
        onMouseOver={(e) => showCountryDatum(e, 'BR')}
        onMouseOut={(e) => hideCountryDatum(e, 'BR')}
      >
        <path d="M264 312H240V336H264V312Z" />
        <text style={getCountryStyle('BR')}>
          <tspan x="245" y="327.39">
            BR
          </tspan>
        </text>
      </g>
      <g
        id="CA"
        fill={getCountryFill('CA')}
        onMouseOver={(e) => showCountryDatum(e, 'CA')}
        onMouseOut={(e) => hideCountryDatum(e, 'CA')}
      >
        <path d="M96 48H72V72H96V48Z" />
        <text style={getCountryStyle('CA')}>
          <tspan x="77" y="63.3898">
            CA
          </tspan>
        </text>
      </g>
      <g
        id="CL"
        fill={getCountryFill('CL')}
        onMouseOver={(e) => showCountryDatum(e, 'CL')}
        onMouseOut={(e) => hideCountryDatum(e, 'CL')}
      >
        <path d="M216 360H192V384H216V360Z" />
        <text style={getCountryStyle('CL')}>
          <tspan x="198" y="375.39">
            CL
          </tspan>
        </text>
      </g>
      <g
        id="CO"
        fill={getCountryFill('CO')}
        onMouseOver={(e) => showCountryDatum(e, 'CO')}
        onMouseOut={(e) => hideCountryDatum(e, 'CO')}
      >
        <path d="M192 264H168V288H192V264Z" />
        <text style={getCountryStyle('CO')}>
          <tspan x="173" y="279.39">
            CO
          </tspan>
        </text>
      </g>
      <g
        id="CR"
        fill={getCountryFill('CR')}
        onMouseOver={(e) => showCountryDatum(e, 'CR')}
        onMouseOut={(e) => hideCountryDatum(e, 'CR')}
      >
        <path d="M144 216H120V240H144V216Z" />
        <text style={getCountryStyle('CR')}>
          <tspan x="124" y="230.901">
            CR
          </tspan>
        </text>
      </g>
      <g
        id="EC"
        fill={getCountryFill('EC')}
        onMouseOver={(e) => showCountryDatum(e, 'EC')}
        onMouseOut={(e) => hideCountryDatum(e, 'EC')}
      >
        <path d="M192 288H168V312H192V288Z" />
        <text style={getCountryStyle('EC')}>
          <tspan x="173" y="303.39">
            EC
          </tspan>
        </text>
      </g>
      <g
        id="SV"
        fill={getCountryFill('SV')}
        onMouseOver={(e) => showCountryDatum(e, 'SV')}
        onMouseOut={(e) => hideCountryDatum(e, 'SV')}
      >
        <path d="M96 168H72V192H96V168Z" />
        <text style={getCountryStyle('SV')}>
          <tspan x="78" y="182.74">
            SV
          </tspan>
        </text>
      </g>
      <g
        id="GL"
        fill={getCountryFill('GL')}
        onMouseOver={(e) => showCountryDatum(e, 'GL')}
        onMouseOut={(e) => hideCountryDatum(e, 'GL')}
      >
        <path d="M240 24H216V48H240V24Z" />
        <text style={getCountryStyle('GL')}>
          <tspan x="221" y="39.3898">
            GL
          </tspan>
        </text>
      </g>
      <g
        id="GT"
        fill={getCountryFill('GT')}
        onMouseOver={(e) => showCountryDatum(e, 'GT')}
        onMouseOut={(e) => hideCountryDatum(e, 'GT')}
      >
        <path d="M96 144H72V168H96V144Z" />
        <text style={getCountryStyle('GT')}>
          <tspan x="77" y="159.294">
            GT
          </tspan>
        </text>
      </g>
      <g
        id="GY"
        fill={getCountryFill('GY')}
        onMouseOver={(e) => showCountryDatum(e, 'GY')}
        onMouseOut={(e) => hideCountryDatum(e, 'GY')}
      >
        <path d="M216 288H192V312H216V288Z" />
        <text style={getCountryStyle('GY')}>
          <tspan x="197" y="303.39">
            GY
          </tspan>
        </text>
      </g>
      <g
        id="HN"
        fill={getCountryFill('HN')}
        onMouseOver={(e) => showCountryDatum(e, 'HN')}
        onMouseOut={(e) => hideCountryDatum(e, 'HN')}
      >
        <path d="M120 168H96V192H120V168Z" />
        <text style={getCountryStyle('HN')}>
          <tspan x="101" y="182.86">
            HN
          </tspan>
        </text>
      </g>
      <g
        id="MX"
        fill={getCountryFill('MX')}
        onMouseOver={(e) => showCountryDatum(e, 'MX')}
        onMouseOut={(e) => hideCountryDatum(e, 'MX')}
      >
        <path d="M96 120H72V144H96V120Z" />
        <text style={getCountryStyle('MX')}>
          <tspan x="77" y="134.752">
            MX
          </tspan>
        </text>
      </g>
      <g
        id="NI"
        fill={getCountryFill('NI')}
        onMouseOver={(e) => showCountryDatum(e, 'NI')}
        onMouseOut={(e) => hideCountryDatum(e, 'NI')}
      >
        <path d="M120 192H96V216H120V192Z" />
        <text style={getCountryStyle('NI')}>
          <tspan x="103" y="207.39">
            NI
          </tspan>
        </text>
      </g>
      <g
        id="PA"
        fill={getCountryFill('PA')}
        onMouseOver={(e) => showCountryDatum(e, 'PA')}
        onMouseOut={(e) => hideCountryDatum(e, 'PA')}
      >
        <path d="M168 240H144V264H168V240Z" />
        <text style={getCountryStyle('PA')}>
          <tspan x="150" y="255.39">
            PA
          </tspan>
        </text>
      </g>
      <g
        id="PY"
        fill={getCountryFill('PY')}
        onMouseOver={(e) => showCountryDatum(e, 'PY')}
        onMouseOut={(e) => hideCountryDatum(e, 'PY')}
      >
        <path d="M216 336H192V360H216V336Z" />
        <text style={getCountryStyle('PY')}>
          <tspan x="198" y="351.39">
            PY
          </tspan>
        </text>
      </g>
      <g
        id="PE"
        fill={getCountryFill('PE')}
        onMouseOver={(e) => showCountryDatum(e, 'PE')}
        onMouseOut={(e) => hideCountryDatum(e, 'PE')}
      >
        <path d="M192 312H168V336H192V312Z" />
        <text style={getCountryStyle('PE')}>
          <tspan x="174" y="327.39">
            PE
          </tspan>
        </text>
      </g>
      <g
        id="PM"
        fill={getCountryFill('PM')}
        onMouseOver={(e) => showCountryDatum(e, 'PM')}
        onMouseOut={(e) => hideCountryDatum(e, 'PM')}
      >
        <path d="M125 61H101V85H125V61Z" />
        <text style={getCountryStyle('PM')}>
          <tspan x="105" y="76.3014">
            PM
          </tspan>
        </text>
      </g>
      <g
        id="SR"
        fill={getCountryFill('SR')}
        onMouseOver={(e) => showCountryDatum(e, 'SR')}
        onMouseOut={(e) => hideCountryDatum(e, 'SR')}
      >
        <path d="M240 312H216V336H240V312Z" />
        <text style={getCountryStyle('SR')}>
          <tspan x="221" y="327.334">
            SR
          </tspan>
        </text>
      </g>
      <g
        id="UY"
        fill={getCountryFill('UY')}
        onMouseOver={(e) => showCountryDatum(e, 'UY')}
        onMouseOut={(e) => hideCountryDatum(e, 'UY')}
      >
        <path d="M240 336H216V360H240V336Z" />
        <text style={getCountryStyle('UY')}>
          <tspan x="221" y="351.39">
            UY
          </tspan>
        </text>
      </g>
      <g
        id="VE"
        fill={getCountryFill('VE')}
        onMouseOver={(e) => showCountryDatum(e, 'VE')}
        onMouseOut={(e) => hideCountryDatum(e, 'VE')}
      >
        <path d="M216 264H192V288H216V264Z" />
        <text style={getCountryStyle('VE')}>
          <tspan x="198" y="279.39">
            VE
          </tspan>
        </text>
      </g>
      <g
        id="US"
        fill={getCountryFill('US')}
        onMouseOver={(e) => showCountryDatum(e, 'US')}
        onMouseOut={(e) => hideCountryDatum(e, 'US')}
      >
        <path d="M96 72H72V96H96V72Z" />
        <text style={getCountryStyle('US')}>
          <tspan x="77" y="87.3898">
            US
          </tspan>
        </text>
      </g>
      <g
        id="GF"
        fill={getCountryFill('GF')}
        onMouseOver={(e) => showCountryDatum(e, 'GF')}
        onMouseOut={(e) => hideCountryDatum(e, 'GF')}
      >
        <path d="M240 288H216V312H240V288Z" />
        <text style={getCountryStyle('GF')}>
          <tspan x="221" y="303.39">
            GF
          </tspan>
        </text>
      </g>
      <g
        id="BM"
        fill={getCountryFill('BM')}
        onMouseOver={(e) => showCountryDatum(e, 'BM')}
        onMouseOut={(e) => hideCountryDatum(e, 'BM')}
      >
        <path d="M252 72H228V96H252V72Z" />
        <text style={getCountryStyle('BM')}>
          <tspan x="232" y="87.3898">
            BM
          </tspan>
        </text>
      </g>
      <g
        id="KN"
        fill={getCountryFill('KN')}
        onMouseOver={(e) => showCountryDatum(e, 'KN')}
        onMouseOut={(e) => hideCountryDatum(e, 'KN')}
      >
        <path d="M230 187H206V211H230V187Z" />
        <text style={getCountryStyle('KN')}>
          <tspan x="211" y="202.39">
            KN
          </tspan>
        </text>
      </g>
      <g
        id="TT"
        fill={getCountryFill('TT')}
        onMouseOver={(e) => showCountryDatum(e, 'TT')}
        onMouseOut={(e) => hideCountryDatum(e, 'TT')}
      >
        <path d="M245 259H221V283H245V259Z" />
        <text style={getCountryStyle('TT')}>
          <tspan x="227" y="273.521">
            TT
          </tspan>
        </text>
      </g>
      <g
        id="JM"
        fill={getCountryFill('JM')}
        onMouseOver={(e) => showCountryDatum(e, 'JM')}
        onMouseOut={(e) => hideCountryDatum(e, 'JM')}
      >
        <path d="M182 139H158V163H182V139Z" />
        <text style={getCountryStyle('JM')}>
          <tspan x="163" y="154.39">
            JM
          </tspan>
        </text>
      </g>
      <g
        id="GD"
        fill={getCountryFill('GD')}
        onMouseOver={(e) => showCountryDatum(e, 'GD')}
        onMouseOut={(e) => hideCountryDatum(e, 'GD')}
      >
        <path d="M278 187H254V211H278V187Z" />
        <text style={getCountryStyle('GD')}>
          <tspan x="259" y="202.39">
            GD
          </tspan>
        </text>
      </g>
      <g
        id="VC"
        fill={getCountryFill('VC')}
        onMouseOver={(e) => showCountryDatum(e, 'VC')}
        onMouseOut={(e) => hideCountryDatum(e, 'VC')}
      >
        <path d="M206 187H182V211H206V187Z" />
        <text style={getCountryStyle('VC')}>
          <tspan x="187" y="202.39">
            VC
          </tspan>
        </text>
      </g>
      <g
        id="BB"
        fill={getCountryFill('BB')}
        onMouseOver={(e) => showCountryDatum(e, 'BB')}
        onMouseOut={(e) => hideCountryDatum(e, 'BB')}
      >
        <path d="M301 211H277V235H301V211Z" />
        <text style={getCountryStyle('BB')}>
          <tspan x="282" y="226.39">
            BB
          </tspan>
        </text>
      </g>
      <g
        fill={getCountryFill('CU_2')}
        onMouseOver={(e) => showCountryDatum(e, 'CU_2')}
        onMouseOut={(e) => hideCountryDatum(e, 'CU_2')}
      >
        <path d="M158 115H134V139H158V115Z" />
        <text style={getCountryStyle('CU_2')}>
          <tspan x="139" y="130.39">
            CU
          </tspan>
        </text>
      </g>
      <g
        id="AG"
        fill={getCountryFill('AG')}
        onMouseOver={(e) => showCountryDatum(e, 'AG')}
        onMouseOut={(e) => hideCountryDatum(e, 'AG')}
      >
        <path d="M253 163H229V187H253V163Z" />
        <text style={getCountryStyle('AG')}>
          <tspan x="234" y="178.412">
            AG
          </tspan>
        </text>
      </g>
      <g
        id="DM"
        fill={getCountryFill('DM')}
        onMouseOver={(e) => showCountryDatum(e, 'DM')}
        onMouseOut={(e) => hideCountryDatum(e, 'DM')}
      >
        <path d="M301 187H277V211H301V187Z" />
        <text style={getCountryStyle('DM')}>
          <tspan x="281" y="202.259">
            DM
          </tspan>
        </text>
      </g>
      <g
        id="HT"
        fill={getCountryFill('HT')}
        onMouseOver={(e) => showCountryDatum(e, 'HT')}
        onMouseOut={(e) => hideCountryDatum(e, 'HT')}
      >
        <path d="M205 139H181V163H205V139Z" />
        <text style={getCountryStyle('HT')}>
          <tspan x="187" y="154.39">
            HT
          </tspan>
        </text>
      </g>
      <g
        id="DO"
        fill={getCountryFill('DO')}
        onMouseOver={(e) => showCountryDatum(e, 'DO')}
        onMouseOut={(e) => hideCountryDatum(e, 'DO')}
      >
        <path d="M229 139H205V163H229V139Z" />
        <text style={getCountryStyle('DO')}>
          <tspan x="210" y="154.39">
            DO
          </tspan>
        </text>
      </g>
      <g
        id="KY"
        fill={getCountryFill('KY')}
        onMouseOver={(e) => showCountryDatum(e, 'KY')}
        onMouseOut={(e) => hideCountryDatum(e, 'KY')}
      >
        <path d="M158 139H134V163H158V139Z" />
        <text style={getCountryStyle('KY')}>
          <tspan x="139" y="154.39">
            KY
          </tspan>
        </text>
      </g>
      <g
        id="GP"
        fill={getCountryFill('GP')}
        onMouseOver={(e) => showCountryDatum(e, 'GP')}
        onMouseOut={(e) => hideCountryDatum(e, 'GP')}
      >
        <path d="M254 187H230V211H254V187Z" />
        <text style={getCountryStyle('GP')}>
          <tspan x="235" y="202.39">
            GP
          </tspan>
        </text>
      </g>
      <g
        id="LC"
        fill={getCountryFill('LC')}
        onMouseOver={(e) => showCountryDatum(e, 'LC')}
        onMouseOut={(e) => hideCountryDatum(e, 'LC')}
      >
        <path d="M277 211H253V235H277V211Z" />
        <text style={getCountryStyle('LC')}>
          <tspan x="259" y="226.39">
            LC
          </tspan>
        </text>
      </g>
      <g
        id="MQ"
        fill={getCountryFill('MQ')}
        onMouseOver={(e) => showCountryDatum(e, 'MQ')}
        onMouseOut={(e) => hideCountryDatum(e, 'MQ')}
      >
        <path d="M277 163H253V187H277V163Z" />
        <text style={getCountryStyle('MQ')}>
          <tspan x="257" y="178.39">
            MQ
          </tspan>
        </text>
      </g>
      <g
        id="MS"
        fill={getCountryFill('MS')}
        onMouseOver={(e) => showCountryDatum(e, 'MS')}
        onMouseOut={(e) => hideCountryDatum(e, 'MS')}
      >
        <path d="M229 163H205V187H229V163Z" />
        <text style={getCountryStyle('MS')}>
          <tspan x="209" y="178.39">
            MS
          </tspan>
        </text>
      </g>
      <g
        id="PR"
        fill={getCountryFill('PR')}
        onMouseOver={(e) => showCountryDatum(e, 'PR')}
        onMouseOut={(e) => hideCountryDatum(e, 'PR')}
      >
        <path d="M253 139H229V163H253V139Z" />
        <text style={getCountryStyle('PR')}>
          <tspan x="234" y="154.39">
            PR
          </tspan>
        </text>
      </g>
      <g
        id="AW"
        fill={getCountryFill('AW')}
        onMouseOver={(e) => showCountryDatum(e, 'AW')}
        onMouseOut={(e) => hideCountryDatum(e, 'AW')}
      >
        <path d="M197 235H173V259H197V235Z" />
        <text style={getCountryStyle('AW')}>
          <tspan x="177" y="250.39">
            AW
          </tspan>
        </text>
      </g>
      <g
        id="BS"
        fill={getCountryFill('BS')}
        onMouseOver={(e) => showCountryDatum(e, 'BS')}
        onMouseOut={(e) => hideCountryDatum(e, 'BS')}
      >
        <path d="M158 91H134V115H158V91Z" />
        <text style={getCountryStyle('BS')}>
          <tspan x="139" y="106.39">
            BS
          </tspan>
        </text>
      </g>
      <g
        id="TC"
        fill={getCountryFill('TC')}
        onMouseOver={(e) => showCountryDatum(e, 'TC')}
        onMouseOut={(e) => hideCountryDatum(e, 'TC')}
      >
        <path d="M182 115H158V139H182V115Z" />
        <text style={getCountryStyle('TC')}>
          <tspan x="164" y="130.39">
            TC
          </tspan>
        </text>
      </g>
      <g
        id="VG"
        fill={getCountryFill('VG')}
        onMouseOver={(e) => showCountryDatum(e, 'VG')}
        onMouseOut={(e) => hideCountryDatum(e, 'VG')}
      >
        <path d="M229 115H205V139H229V115Z" />
        <text style={getCountryStyle('VG')}>
          <tspan x="210" y="130.39">
            VG
          </tspan>
        </text>
      </g>
      <g
        id="VI"
        fill={getCountryFill('VI')}
        onMouseOver={(e) => showCountryDatum(e, 'VI')}
        onMouseOut={(e) => hideCountryDatum(e, 'VI')}
      >
        <path d="M206 163H182V187H206V163Z" />
        <text style={getCountryStyle('VI')}>
          <tspan x="190" y="178.695">
            VI
          </tspan>
        </text>
      </g>
      <g
        id="AI"
        fill={getCountryFill('AI')}
        onMouseOver={(e) => showCountryDatum(e, 'AI')}
        onMouseOut={(e) => hideCountryDatum(e, 'AI')}
      >
        <path d="M253 115H229V139H253V115Z" />
        <text style={getCountryStyle('AI')}>
          <tspan x="235" y="130.39">
            AI
          </tspan>
        </text>
      </g>
      <g
        id="AI_3"
        fill={getCountryFill('AI_3')}
        onMouseOver={(e) => showCountryDatum(e, 'AI_3')}
        onMouseOut={(e) => hideCountryDatum(e, 'AI_3')}
      >
        <path d="M277 139H253V163H277V139Z" />
        <text id="MF" style={getCountryStyle('AI_3')}>
          <tspan x="258" y="154.695">
            MF
          </tspan>
        </text>
      </g>
      <g
        id="SX"
        fill={getCountryFill('SX')}
        onMouseOver={(e) => showCountryDatum(e, 'SX')}
        onMouseOut={(e) => hideCountryDatum(e, 'SX')}
      >
        <path d="M277 115H253V139H277V115Z" />
        <text style={getCountryStyle('SX')}>
          <tspan x="258" y="130.695">
            SX
          </tspan>
        </text>
      </g>
      <g
        id="AI_4"
        fill={getCountryFill('AI_4')}
        onMouseOver={(e) => showCountryDatum(e, 'AI_4')}
        onMouseOut={(e) => hideCountryDatum(e, 'AI_4')}
      >
        <path d="M301 139H277V163H301V139Z" />
        <text id="BL" style={getCountryStyle('AI_4')}>
          <tspan x="283" y="154.39">
            BL
          </tspan>
        </text>
      </g>
    </g>
  )
}
