export const ContinentNames = () => {
  const continentText = { fill: 'grey' }
  return (
    <g id="Continent Names" style={continentText}>
      <text x="72" y="39.806">
        Americas
      </text>
      <text x="706" y="423.806">
        Oceania
      </text>
      <text x="0" y="231.806">
        Oceania
      </text>
      <text x="330" y="233.5">
        Europe
      </text>
      <text x="362" y="301.806">
        Africa
      </text>
      <text x="507" y="570.806">
        Antarctica
      </text>
      <text x="648" y="306.806">
        Asia
      </text>
    </g>
  )
}
