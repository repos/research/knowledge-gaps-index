import { scaleThreshold } from 'd3'
import { texts, formatNumbersWithCommas } from 'lib'

interface EuropeProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Europe = ({
  values,
  maxValue,
  minValue,
  setHoverValue,
}: EuropeProps) => {
  const delta = maxValue - minValue
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])

  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  return (
    <g id="Europe">
      <g
        id="AL"
        fill={getCountryFill('AL')}
        onMouseOver={(e) => showCountryDatum(e, 'AL')}
        onMouseOut={(e) => hideCountryDatum(e, 'AL')}
      >
        <path d="M528 240H504V264H528V240Z" />
        <text style={getCountryStyle('AL')}>
          <tspan x="510" y="255.39">
            AL
          </tspan>
        </text>
      </g>
      <g
        id="AD"
        fill={getCountryFill('AD')}
        onMouseOver={(e) => showCountryDatum(e, 'AD')}
        onMouseOut={(e) => hideCountryDatum(e, 'AD')}
      >
        <path d="M408 168H384V192H408V168Z" />
        <text style={getCountryStyle('AD')}>
          <tspan x="389" y="182.842">
            AD
          </tspan>
        </text>
      </g>
      <g
        id="AT"
        fill={getCountryFill('AT')}
        onMouseOver={(e) => showCountryDatum(e, 'AT')}
        onMouseOut={(e) => hideCountryDatum(e, 'AT')}
      >
        <path d="M456 120H432V144H456V120Z" />
        <text style={getCountryStyle('AT')}>
          <tspan x="438" y="134.615">
            AT
          </tspan>
        </text>
      </g>
      <g
        id="BY"
        fill={getCountryFill('BY')}
        onMouseOver={(e) => showCountryDatum(e, 'BY')}
        onMouseOut={(e) => hideCountryDatum(e, 'BY')}
      >
        <path d="M528 96H504V120H528V96Z" />
        <text style={getCountryStyle('BY')}>
          <tspan x="509" y="110.99">
            BY
          </tspan>
        </text>
      </g>
      <g
        id="BE"
        fill={getCountryFill('BE')}
        onMouseOver={(e) => showCountryDatum(e, 'BE')}
        onMouseOut={(e) => hideCountryDatum(e, 'BE')}
      >
        <path d="M432.311 144H408.311V168H432.311V144Z" />
        <text style={getCountryStyle('BE')}>
          <tspan x="414" y="159.39">
            BE
          </tspan>
        </text>
      </g>
      <g
        id="BA"
        fill={getCountryFill('BA')}
        onMouseOver={(e) => showCountryDatum(e, 'BA')}
        onMouseOut={(e) => hideCountryDatum(e, 'BA')}
      >
        <path d="M528 192H504V216H528V192Z" />
        <text style={getCountryStyle('BA')}>
          <tspan x="509" y="207.085">
            BA
          </tspan>
        </text>
      </g>
      <g
        id="BG"
        fill={getCountryFill('BG')}
        onMouseOver={(e) => showCountryDatum(e, 'BG')}
        onMouseOut={(e) => hideCountryDatum(e, 'BG')}
      >
        <path d="M552 192H528V216H552V192Z" />
        <text style={getCountryStyle('BG')}>
          <tspan x="533" y="207.085">
            BG
          </tspan>
        </text>
      </g>
      <g
        id="HR"
        fill={getCountryFill('HR')}
        onMouseOver={(e) => showCountryDatum(e, 'HR')}
        onMouseOut={(e) => hideCountryDatum(e, 'HR')}
      >
        <path d="M504 168H480V192H504V168Z" />
        <text style={getCountryStyle('HR')}>
          <tspan x="485" y="183.085">
            HR
          </tspan>
        </text>
      </g>
      <g
        id="CZ"
        fill={getCountryFill('CZ')}
        onMouseOver={(e) => showCountryDatum(e, 'CZ')}
        onMouseOut={(e) => hideCountryDatum(e, 'CZ')}
      >
        <path d="M480 120H456V144H480V120Z" />
        <text style={getCountryStyle('CZ')}>
          <tspan x="461" y="135.085">
            CZ
          </tspan>
        </text>
      </g>
      <g
        id="DK"
        fill={getCountryFill('DK')}
        onMouseOver={(e) => showCountryDatum(e, 'DK')}
        onMouseOut={(e) => hideCountryDatum(e, 'DK')}
      >
        <path d="M456 72H432V96H456V72Z" />
        <text style={getCountryStyle('DK')}>
          <tspan x="437" y="87.2433">
            DK
          </tspan>
        </text>
      </g>
      <g
        id="EE"
        fill={getCountryFill('EE')}
        onMouseOver={(e) => showCountryDatum(e, 'EE')}
        onMouseOut={(e) => hideCountryDatum(e, 'EE')}
      >
        <path d="M504 48H480V72H504V48Z" />
        <text style={getCountryStyle('EE')}>
          <tspan x="486" y="62.8708">
            EE
          </tspan>
        </text>
      </g>
      <g
        id="FO"
        fill={getCountryFill('FO')}
        onMouseOver={(e) => showCountryDatum(e, 'FO')}
        onMouseOut={(e) => hideCountryDatum(e, 'FO')}
      >
        <path d="M355 43H331V67H355V43Z" />
        <text style={getCountryStyle('FO')}>
          <tspan x="336" y="58.0846">
            FO
          </tspan>
        </text>
      </g>
      <g
        id="FI"
        fill={getCountryFill('FI')}
        onMouseOver={(e) => showCountryDatum(e, 'FI')}
        onMouseOut={(e) => hideCountryDatum(e, 'FI')}
      >
        <path d="M504 24H480V48H504V24Z" />
        <text style={getCountryStyle('FI')}>
          <tspan x="488" y="38.5597">
            FI
          </tspan>
        </text>
      </g>
      <g
        id="FR"
        fill={getCountryFill('FR')}
        onMouseOver={(e) => showCountryDatum(e, 'FR')}
        onMouseOut={(e) => hideCountryDatum(e, 'FR')}
      >
        <path d="M408 144H384V168H408V144Z" />
        <text style={getCountryStyle('FR')}>
          <tspan x="390" y="158.707">
            FR
          </tspan>
        </text>
      </g>
      <g
        id="DE"
        fill={getCountryFill('DE')}
        onMouseOver={(e) => showCountryDatum(e, 'DE')}
        onMouseOut={(e) => hideCountryDatum(e, 'DE')}
      >
        <path d="M456 96H432V120H456V96Z" />
        <text style={getCountryStyle('DE')}>
          <tspan x="437" y="110.794">
            DE
          </tspan>
        </text>
      </g>
      <g
        id="GI"
        fill={getCountryFill('GI')}
        onMouseOver={(e) => showCountryDatum(e, 'GI')}
        onMouseOut={(e) => hideCountryDatum(e, 'GI')}
      >
        <path d="M408 216H384V240H408V216Z" />
        <text style={getCountryStyle('GI')}>
          <tspan x="391" y="231.085">
            GI
          </tspan>
        </text>
      </g>
      <g
        id="GR"
        fill={getCountryFill('GR')}
        onMouseOver={(e) => showCountryDatum(e, 'GR')}
        onMouseOut={(e) => hideCountryDatum(e, 'GR')}
      >
        <path d="M552 240H528V264H552V240Z" />
        <text style={getCountryStyle('GR')}>
          <tspan x="533" y="255.085">
            GR
          </tspan>
        </text>
      </g>
      <g
        id="GG"
        fill={getCountryFill('GG')}
        onMouseOver={(e) => showCountryDatum(e, 'GG')}
        onMouseOut={(e) => hideCountryDatum(e, 'GG')}
      >
        <path d="M379 149H355V173H379V149Z" />
        <text style={getCountryStyle('GG')}>
          <tspan x="359" y="164.085">
            GG
          </tspan>
        </text>
      </g>
      <g
        id="HU"
        fill={getCountryFill('HU')}
        onMouseOver={(e) => showCountryDatum(e, 'HU')}
        onMouseOut={(e) => hideCountryDatum(e, 'HU')}
      >
        <path d="M504 144H480V168H504V144Z" />
        <text style={getCountryStyle('HU')}>
          <tspan x="485" y="159.085">
            HU
          </tspan>
        </text>
      </g>
      <g
        id="IS"
        fill={getCountryFill('IS')}
        onMouseOver={(e) => showCountryDatum(e, 'IS')}
        onMouseOut={(e) => hideCountryDatum(e, 'IS')}
      >
        <path d="M331 19H307V43H331V19Z" />
        <text style={getCountryStyle('IS')}>
          <tspan x="314" y="34.695">
            IS
          </tspan>
        </text>
      </g>
      <g
        id="IE"
        fill={getCountryFill('IE')}
        onMouseOver={(e) => showCountryDatum(e, 'IE')}
        onMouseOut={(e) => hideCountryDatum(e, 'IE')}
      >
        <path d="M336 96H312V120H336V96Z" />
        <text style={getCountryStyle('IE')}>
          <tspan x="320" y="111.085">
            IE
          </tspan>
        </text>
      </g>
      <g
        id="IT"
        fill={getCountryFill('IT')}
        onMouseOver={(e) => showCountryDatum(e, 'IT')}
        onMouseOut={(e) => hideCountryDatum(e, 'IT')}
      >
        <path d="M456 192H432V216H456V192Z" />
        <text style={getCountryStyle('IT')}>
          <tspan x="440" y="206.49">
            IT
          </tspan>
        </text>
      </g>
      <g
        id="JE"
        fill={getCountryFill('JE')}
        onMouseOver={(e) => showCountryDatum(e, 'JE')}
        onMouseOut={(e) => hideCountryDatum(e, 'JE')}
      >
        <path d="M355 149H331V173H355V149Z" />
        <text style={getCountryStyle('JE')}>
          <tspan x="337" y="164.13">
            JE
          </tspan>
        </text>
      </g>
      <g
        id="LV"
        fill={getCountryFill('LV')}
        onMouseOver={(e) => showCountryDatum(e, 'LV')}
        onMouseOut={(e) => hideCountryDatum(e, 'LV')}
      >
        <path d="M504 72H480V96H504V72Z" />
        <text style={getCountryStyle('LV')}>
          <tspan x="487" y="87.1789">
            LV
          </tspan>
        </text>
      </g>
      <g
        id="LI"
        fill={getCountryFill('LI')}
        onMouseOver={(e) => showCountryDatum(e, 'LI')}
        onMouseOut={(e) => hideCountryDatum(e, 'LI')}
      >
        <path d="M456 144H432V168H456V144Z" />
        <text style={getCountryStyle('LI')}>
          <tspan x="440" y="159.085">
            LI
          </tspan>
        </text>
      </g>
      <g
        id="LT"
        fill={getCountryFill('LT')}
        onMouseOver={(e) => showCountryDatum(e, 'LT')}
        onMouseOut={(e) => hideCountryDatum(e, 'LT')}
      >
        <path d="M504 96H480V120H504V96Z" />
        <text style={getCountryStyle('LT')}>
          <tspan x="487" y="111.19">
            LT
          </tspan>
        </text>
      </g>
      <g
        id="LU"
        fill={getCountryFill('LU')}
        onMouseOver={(e) => showCountryDatum(e, 'LU')}
        onMouseOut={(e) => hideCountryDatum(e, 'LU')}
      >
        <path d="M432 120H408V144H432V120Z" />
        <text style={getCountryStyle('LU')}>
          <tspan x="414" y="134.481">
            LU
          </tspan>
        </text>
      </g>
      <g
        id="MT"
        fill={getCountryFill('MT')}
        onMouseOver={(e) => showCountryDatum(e, 'MT')}
        onMouseOut={(e) => hideCountryDatum(e, 'MT')}
      >
        <path d="M480 245H456V269H480V245Z" />
        <text style={getCountryStyle('MT')}>
          <tspan x="461" y="260.357">
            MT
          </tspan>
        </text>
      </g>
      <g
        id="MD"
        fill={getCountryFill('MD')}
        onMouseOver={(e) => showCountryDatum(e, 'MD')}
        onMouseOut={(e) => hideCountryDatum(e, 'MD')}
      >
        <path d="M552 120H528V144H552V120Z" />
        <text style={getCountryStyle('MD')}>
          <tspan x="532" y="135.39">
            MD
          </tspan>
        </text>
      </g>
      <g
        id="MC"
        fill={getCountryFill('MC')}
        onMouseOver={(e) => showCountryDatum(e, 'MC')}
        onMouseOut={(e) => hideCountryDatum(e, 'MC')}
      >
        <path d="M432.311 192H408.311V216H432.311V192Z" />
        <text style={getCountryStyle('MC')}>
          <tspan x="412" y="207.085">
            MC
          </tspan>
        </text>
      </g>
      <g
        id="ME"
        fill={getCountryFill('ME')}
        onMouseOver={(e) => showCountryDatum(e, 'ME')}
        onMouseOut={(e) => hideCountryDatum(e, 'ME')}
      >
        <path d="M528 216H504V240H528V216Z" />
        <text style={getCountryStyle('ME')}>
          <tspan x="509" y="231.254">
            ME
          </tspan>
        </text>
      </g>
      <g
        id="NL"
        fill={getCountryFill('NL')}
        onMouseOver={(e) => showCountryDatum(e, 'NL')}
        onMouseOut={(e) => hideCountryDatum(e, 'NL')}
      >
        <path d="M432 96H408V120H432V96Z" />
        <text style={getCountryStyle('NL')}>
          <tspan x="414" y="110.689">
            NL
          </tspan>
        </text>
      </g>
      <g
        id="NO"
        fill={getCountryFill('NO')}
        onMouseOver={(e) => showCountryDatum(e, 'NO')}
        onMouseOut={(e) => hideCountryDatum(e, 'NO')}
      >
        <path d="M456 24H432V48H456V24Z" />
        <text style={getCountryStyle('NO')}>
          <tspan x="437" y="38.7121">
            NO
          </tspan>
        </text>
      </g>
      <g
        id="PL"
        fill={getCountryFill('PL')}
        onMouseOver={(e) => showCountryDatum(e, 'PL')}
        onMouseOut={(e) => hideCountryDatum(e, 'PL')}
      >
        <path d="M480 96H456V120H480V96Z" />
        <text style={getCountryStyle('PL')}>
          <tspan x="462" y="110.39">
            PL
          </tspan>
        </text>
      </g>
      <g
        id="PT"
        fill={getCountryFill('PT')}
        onMouseOver={(e) => showCountryDatum(e, 'PT')}
        onMouseOut={(e) => hideCountryDatum(e, 'PT')}
      >
        <path d="M384 192H360V216H384V192Z" />
        <text style={getCountryStyle('PT')}>
          <tspan x="366" y="207.085">
            PT
          </tspan>
        </text>
      </g>
      <g
        id="RO"
        fill={getCountryFill('RO')}
        onMouseOver={(e) => showCountryDatum(e, 'RO')}
        onMouseOut={(e) => hideCountryDatum(e, 'RO')}
      >
        <path d="M528 144H504V168H528V144Z" />
        <text style={getCountryStyle('RO')}>
          <tspan x="509" y="159.085">
            RO
          </tspan>
        </text>
      </g>
      <g
        id="SM"
        fill={getCountryFill('SM')}
        onMouseOver={(e) => showCountryDatum(e, 'SM')}
        onMouseOut={(e) => hideCountryDatum(e, 'SM')}
      >
        <path d="M456 216H432V240H456V216Z" />
        <text style={getCountryStyle('SM')}>
          <tspan x="436" y="231.377">
            SM
          </tspan>
        </text>
      </g>
      <g
        id="SK"
        fill={getCountryFill('SK')}
        onMouseOver={(e) => showCountryDatum(e, 'SK')}
        onMouseOut={(e) => hideCountryDatum(e, 'SK')}
      >
        <path d="M504 120H480V144H504V120Z" />
        <text style={getCountryStyle('SK')}>
          <tspan x="485" y="134.427">
            SK
          </tspan>
        </text>
      </g>
      <g
        id="SI"
        fill={getCountryFill('SI')}
        onMouseOver={(e) => showCountryDatum(e, 'SI')}
        onMouseOut={(e) => hideCountryDatum(e, 'SI')}
      >
        <path d="M480 144H456V168H480V144Z" />
        <text style={getCountryStyle('SI')}>
          <tspan x="463" y="159.085">
            SI
          </tspan>
        </text>
      </g>
      <g
        id="ES"
        fill={getCountryFill('ES')}
        onMouseOver={(e) => showCountryDatum(e, 'ES')}
        onMouseOut={(e) => hideCountryDatum(e, 'ES')}
      >
        <path d="M408 192H384V216H408V192Z" />
        <text style={getCountryStyle('ES')}>
          <tspan x="390" y="207.085">
            ES
          </tspan>
        </text>
      </g>
      <g
        id="SE"
        fill={getCountryFill('SE')}
        onMouseOver={(e) => showCountryDatum(e, 'SE')}
        onMouseOut={(e) => hideCountryDatum(e, 'SE')}
      >
        <path d="M480 24H456V48H480V24Z" />
        <text style={getCountryStyle('SE')}>
          <tspan x="462" y="39.4338">
            SE
          </tspan>
        </text>
      </g>
      <g
        id="CH"
        fill={getCountryFill('CH')}
        onMouseOver={(e) => showCountryDatum(e, 'CH')}
        onMouseOut={(e) => hideCountryDatum(e, 'CH')}
      >
        <path d="M432 168H408V192H432V168Z" />
        <text style={getCountryStyle('CH')}>
          <tspan x="413" y="183.39">
            CH
          </tspan>
        </text>
      </g>
      <g
        id="UA"
        fill={getCountryFill('UA')}
        onMouseOver={(e) => showCountryDatum(e, 'UA')}
        onMouseOut={(e) => hideCountryDatum(e, 'UA')}
      >
        <path d="M528 120H504V144H528V120Z" />
        <text style={getCountryStyle('UA')}>
          <tspan x="509" y="134.79">
            UA
          </tspan>
        </text>
      </g>
      <g
        id="GB"
        fill={getCountryFill('GB')}
        onMouseOver={(e) => showCountryDatum(e, 'GB')}
        onMouseOut={(e) => hideCountryDatum(e, 'GB')}
      >
        <path d="M384 96H360V120H384V96Z" />
        <text style={getCountryStyle('GB')}>
          <tspan x="365" y="111.39">
            GB
          </tspan>
        </text>
      </g>
      <g
        id="AX"
        fill={getCountryFill('AX')}
        onMouseOver={(e) => showCountryDatum(e, 'AX')}
        onMouseOut={(e) => hideCountryDatum(e, 'AX')}
      >
        <path d="M480 48H456V72H480V48Z" />
        <text style={getCountryStyle('AX')}>
          <tspan x="463" y="63.0846">
            AX
          </tspan>
        </text>
      </g>
      <g
        id="IM"
        fill={getCountryFill('IM')}
        onMouseOver={(e) => showCountryDatum(e, 'IM')}
        onMouseOut={(e) => hideCountryDatum(e, 'IM')}
      >
        <path d="M384 72H360V96H384V72Z" />
        <text style={getCountryStyle('IM')}>
          <tspan x="366" y="87.0846">
            IM
          </tspan>
        </text>
      </g>
      <g
        id="MK"
        fill={getCountryFill('MK')}
        onMouseOver={(e) => showCountryDatum(e, 'MK')}
        onMouseOut={(e) => hideCountryDatum(e, 'MK')}
      >
        <path d="M552 216H528V240H552V216Z" />
        <text style={getCountryStyle('MK')}>
          <tspan x="532" y="231.085">
            MK
          </tspan>
        </text>
      </g>
      <g
        id="RS"
        fill={getCountryFill('RS')}
        onMouseOver={(e) => showCountryDatum(e, 'RS')}
        onMouseOut={(e) => hideCountryDatum(e, 'RS')}
      >
        <path d="M528 168H504V192H528V168Z" />
        <text style={getCountryStyle('RS')}>
          <tspan x="509" y="183.39">
            RS
          </tspan>
        </text>
      </g>
      <g
        id="VA"
        fill={getCountryFill('VA')}
        onMouseOver={(e) => showCountryDatum(e, 'VA')}
        onMouseOut={(e) => hideCountryDatum(e, 'VA')}
      >
        <path d="M480 216H456V240H480V216Z" />
        <text style={getCountryStyle('VA')}>
          <tspan x="462" y="230.986">
            VA
          </tspan>
        </text>
      </g>
    </g>
  )
}
