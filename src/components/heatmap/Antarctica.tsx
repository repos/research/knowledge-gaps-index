import { scaleThreshold } from 'd3'
import { texts, formatNumbersWithCommas } from 'lib'

interface AntarcticaProps {
  values: any
  maxValue: number
  minValue: number
  setHoverValue: any
}

export const Antarctica = ({
  maxValue,
  minValue,
  values,
  setHoverValue,
}: AntarcticaProps) => {
  const delta = maxValue - minValue
  function getCountryStyle(countryInitials: string) {
    return {
      fontSize: 10,
      fill: ['#7D4CBF', '#4c208e', '#0F005B'].includes(
        getCountryFill(countryInitials)
      )
        ? '#FFFFFF'
        : '#000000',
    }
  }
  const colorThresholdScale = scaleThreshold<number, string>()
    .domain([
      delta * 0.2 + minValue,
      delta * 0.4 + minValue,
      delta * 0.6 + minValue,
      delta * 0.8 + minValue,
    ])
    .range(['#e5abff', '#9b69d8', '#7D4CBF', '#4c208e', '#0F005B'])

  function getCountryFill(countryInitials: string) {
    const countryDatum = values[countryInitials]
    if (!countryDatum) return 'lightgrey'
    return colorThresholdScale(countryDatum)
  }
  function showCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'pointer'
    e.currentTarget.setAttribute('stroke', 'white')
    e.currentTarget.setAttribute('strokeWidth', '1px')
    setHoverValue([
      formatNumbersWithCommas(values[id]),
      texts[id.toLowerCase()],
    ])
  }
  function hideCountryDatum(e: any, id: string) {
    document.body.style.cursor = 'default'
    e.currentTarget.setAttribute('stroke', 'none')
    e.currentTarget.setAttribute('strokeWidth', '0')
    setHoverValue(['none', 'not hovering'])
  }
  return (
    <g id="Antarctica">
      <g
        id="TF"
        fill={getCountryFill('TF')}
        onMouseOver={(e) => showCountryDatum(e, 'TF')}
        onMouseOut={(e) => hideCountryDatum(e, 'TF')}
      >
        <path d="M671.5 552.5H648.5V575.515H671.5V552.5Z" />
        <text style={getCountryStyle('TF')}>
          <tspan x="654" y="567.39">
            TF
          </tspan>
        </text>
      </g>
      <g
        id="AQ"
        fill={getCountryFill('AQ')}
        onMouseOver={(e) => showCountryDatum(e, 'AQ')}
        onMouseOut={(e) => hideCountryDatum(e, 'AQ')}
      >
        <path d="M432 552H408V576H432V552Z" />
        <text style={getCountryStyle('AQ')}>
          <tspan x="413" y="567.112">
            AQ
          </tspan>
        </text>
      </g>
    </g>
  )
}
