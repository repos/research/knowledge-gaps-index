import { FormControl, Select, MenuItem, SelectChangeEvent } from '@mui/material'
import { AGGREGATION_TYPE_MONTHLY, AGGREGATION_TYPE_YEARLY } from 'models'

interface AggregationSelectProps {
  aggregation: string
  handleAggregation: (event: SelectChangeEvent) => void
}

export const AggregationSelect = ({
  aggregation,
  handleAggregation,
}: AggregationSelectProps) => {
  return (
    <FormControl style={{ width: '50%', marginRight: '-10px' }}>
      <Select
        labelId="aggregation"
        defaultValue={AGGREGATION_TYPE_MONTHLY}
        placeholder={AGGREGATION_TYPE_MONTHLY}
        id="aggregation"
        value={aggregation}
        onChange={handleAggregation}
        sx={{
          height: '35px',
          '&. MuiOutlinedInput-notchedOutline': {
            border: 'none',
          },
        }}
        style={{fontWeight: 'bold'}}
      >
        <MenuItem value={AGGREGATION_TYPE_MONTHLY}>
          {AGGREGATION_TYPE_MONTHLY}
        </MenuItem>
        <MenuItem value={AGGREGATION_TYPE_YEARLY}>
          {AGGREGATION_TYPE_YEARLY}
        </MenuItem>
      </Select>
    </FormControl>
  )
}
