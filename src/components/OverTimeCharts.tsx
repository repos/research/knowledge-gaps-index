import { Box, Grid } from '@mui/material'
import ParentSize from '@visx/responsive/lib/components/ParentSize'
import {
  VisxLineChart,
  VisxStackedBarchart,
  DetailBarchart,
  HighlightBarchart,
} from 'components'
import { ScaleOrdinal } from 'd3'
import {
  Datum,
  Group,
  IAggregationType,
  IDatasetType,
  IDataType,
  IVisualizationType,
  VISUALIZATION_TYPE_BARCHART,
  VISUALIZATION_TYPE_LINECHART,
} from 'models'

interface OverTimeChartsProps {
  visualization: IVisualizationType
  filteredDataset: Datum[]
  aggregation: IAggregationType
  datasetType: IDatasetType
  normalization: boolean
  subSelection: IDataType
  colorScale: ScaleOrdinal<string, string>
  fullDataset: Datum[]
  groups: Group[]
  filters: Record<string, boolean>
  alphabeticOrder: string
}

export const OverTimeCharts = ({
  visualization,
  filteredDataset,
  aggregation,
  datasetType,
  normalization,
  subSelection,
  colorScale,
  fullDataset,
  groups,
  filters,
  alphabeticOrder,
}: OverTimeChartsProps) => {
  if (!filteredDataset || filteredDataset.length < 0) {
    return null
  }

  if (filteredDataset.length === 0) {
    return (
      <Grid item xs={6} style={{ display: 'flex' }}>
        <h3 style={{ margin: 'auto', color: 'grey' }}>
          Currently there is no visible data. Select a category to discover
          more.
        </h3>
      </Grid>
    )
  }

  if (visualization === VISUALIZATION_TYPE_LINECHART) {
    return (
      <Grid item xs={6}>
        <Box sx={{ width: '100%', height: 400 }}>
          <ParentSize>
            {({ width, height }) => {
              return (
                <>
                  {/* TODO: */}
                  {/* <HighlightLinechart
                    width={width}
                    height={height}
                    fullDataset={fullDataset}
                    filteredDataset={filteredDataset}
                    aggregation={aggregation}
                    datasetType={datasetType}
                    subSelection={subSelection}
                  /> */}
                  <VisxLineChart
                    height={height}
                    width={width}
                    filteredDataset={filteredDataset}
                    aggregation={aggregation}
                    subSelection={subSelection}
                    colorScale={colorScale}
                  />
                </>
              )
            }}
          </ParentSize>
        </Box>
      </Grid>
    )
  }

  if (visualization === VISUALIZATION_TYPE_BARCHART) {
    return (
      <>
        <Grid item xs={5}>
          <Box sx={{ width: '100%', height: 400 }}>
            <ParentSize>
              {({ width, height }) => {
                return (
                  <>
                    <HighlightBarchart
                      width={width}
                      height={height}
                      filteredDataset={filteredDataset}
                      fullDataset={fullDataset}
                      aggregation={aggregation}
                      subSelection={subSelection}
                      colorScale={colorScale}
                      groups={groups}
                      filters={filters}
                    />
                    <VisxStackedBarchart
                      width={width}
                      height={height}
                      filteredDataset={filteredDataset}
                      normalization={normalization}
                      aggregation={aggregation}
                      datasetType={datasetType}
                      subSelection={subSelection}
                      colorScale={colorScale}
                    />
                  </>
                )
              }}
            </ParentSize>
          </Box>
        </Grid>
        <Grid item xs={2} sx={{ paddingLeft: '20px' }}>
          <Box
            sx={{
              width: '100%',
              height: '600px',
            }}
          >
            <ParentSize>
              {({ width, height }) => {
                return (
                  <DetailBarchart
                    width={width}
                    visibleHeight={height}
                    dataset={filteredDataset}
                    subSelection={subSelection}
                    colorScale={colorScale}
                    alphabeticOrder={alphabeticOrder}
                  />
                )
              }}
            </ParentSize>
          </Box>
        </Grid>
      </>
    )
  }

  return null
}
