import { Box, Grid, IconButton, Link, Tooltip, Typography } from '@mui/material'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import { FilterToggleSVG } from './svg/FilterToggleSVG'
import { IMainFilters } from 'models'

interface BreadcrumbProps {
  mainFilters: IMainFilters
  languages: string[]
  onIconClick: () => void
}

export const Breadcrumb = ({
  mainFilters,
  languages,
  onIconClick,
}: BreadcrumbProps) => {
  const moreLanguagesCount = Math.max(0, languages.length - 2)
  const languagesLabel = `${languages.slice(0, 2).join(', ')}${
    moreLanguagesCount > 0 ? ` & ${moreLanguagesCount} more` : ''
  }`

  return (
    <Grid container height={'7vh'} alignContent={'center'} mt={2} style={{}}>
      <Grid item xs={12} display={'flex'} alignContent={'center'} ml={-0.7}>
        <Tooltip title="Click to open data configuration" arrow>
          <IconButton onClick={onIconClick} style={{ height: 48 }}>
            <FilterToggleSVG />
          </IconButton>
        </Tooltip>
        <Box ml={1.5} style={{}}>
          <Tooltip title="Click to open data configuration" arrow>
            <Link onClick={onIconClick} style={{ textDecoration: 'none' }}>
              <Typography variant="h2" mt={0.5}>
                {mainFilters.datasetType}
                <Chevron />
                Wikipedia
                <Chevron />
                Contents
                <Chevron />
                {languagesLabel}
              </Typography>
            </Link>
          </Tooltip>
        </Box>
      </Grid>
    </Grid>
  )
}

const Chevron = () => {
  return (
    <ChevronRightIcon
      sx={{ position: 'relative', top: '5px', color: '#54595D' }}
    />
  )
}
