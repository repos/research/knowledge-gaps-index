import { useState } from 'react'
import { ScaleOrdinal } from 'd3'
import { ReactComponent as AddToGroupIcon } from '../assets/add_to_group.svg'
import { ReactComponent as MoveOutFromGroupIcon } from '../assets/move_out_from_group.svg'
import { Group as GroupType } from 'models/group'
import { IDatasetType, DATASET_TYPE_GEOGRAPHY } from 'models'
import {
  deleteGroup,
  getParentGroup,
  isGroup,
  isInsideAGroup,
  moveOut,
  moveToGroup,
  moveToNewGroup,
} from 'lib/groups'
import { CategoryRow } from './CategoryRow'
import { IVisualizationType, VISUALIZATION_TYPE_HEATMAP } from 'models'
import { VisibilityIcon } from './VisibilityIcon'
import { IconButton, Tooltip } from '@mui/material'
import { getCountry } from 'lib'

interface CategoryRowProps {
  element: GroupType
  filters: FilterValue
  visualization: IVisualizationType
  toggleVisibility: (key: string) => void
  groups: GroupType[]
  setGroups: any
  colorScale: ScaleOrdinal<string, string>
  datasetType: IDatasetType
}

type FilterValue = {
  [key: string]: boolean
}

export const GroupRow = ({
  element,
  filters,
  toggleVisibility,
  visualization,
  groups,
  setGroups,
  colorScale,
  datasetType,
}: CategoryRowProps) => {
  const [showGroupMenu, setShowGroupMenu] = useState<boolean>(false)

  const isAFolder = isGroup(groups, element.label)
  const parentGroup = getParentGroup(groups, element.label)
  const isInGroup = isInsideAGroup(groups, element.label)
  const text = element.label
  const hideGroupsLogic = visualization === VISUALIZATION_TYPE_HEATMAP

  const getLabel = (text: string) => {
    if (datasetType === DATASET_TYPE_GEOGRAPHY) {
      return `${getCountry(text)} (${text})`
    } else {
      return text
    }
  }

  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingTop: 2,

          userSelect: 'none',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          {visualization !== VISUALIZATION_TYPE_HEATMAP && (
            <div
              style={{
                width: 12,
                height: 12,
                backgroundColor: colorScale(element.label),
                borderRadius: '50%',
                marginRight: '8px',
              }}
            />
          )}
          <div
            style={{
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              width: 130,
              fontWeight: isAFolder ? 'bold' : 'normal',
            }}
          >
            {isAFolder ? `▾ ${text}` : getLabel(text)}
          </div>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div
            style={{ cursor: 'pointer', paddingLeft: 3, paddingRight: 3 }}
            onClick={() => toggleVisibility(element.label)}
          >
            <Tooltip title="Hide category" arrow>
              <IconButton style={{ height: 32 }}>
                <VisibilityIcon
                  size={18}
                  isVisible={!!filters[element.label]}
                />
              </IconButton>
            </Tooltip>
          </div>

          <div style={{ cursor: 'pointer', position: 'relative' }}>
            {isAFolder ? (
              <div
                onClick={() =>
                  deleteGroup(
                    groups,
                    element.label,
                    setGroups,
                    setShowGroupMenu
                  )
                }
              >
                <Tooltip title="Break Group" arrow>
                  <IconButton style={{ height: 32, marginLeft: 2 }}>
                    <MoveOutFromGroupIcon />
                  </IconButton>
                </Tooltip>
              </div>
            ) : !hideGroupsLogic ? (
              <div onClick={() => setShowGroupMenu(!showGroupMenu)}>
                <Tooltip title="Add to Group" arrow>
                  <IconButton style={{ height: 32 }}>
                    <AddToGroupIcon />
                  </IconButton>
                </Tooltip>
              </div>
            ) : null}
            {showGroupMenu && (
              <div
                style={{
                  position: 'absolute',
                  right: 0,
                  top: 25,
                  border: '1px solid black',
                  backgroundColor: 'white',
                  padding: 3,
                  zIndex: 2,
                  overflow: 'visible',
                  width: 150,
                  fontSize: 14,
                }}
              >
                {groups.map((group) => {
                  return (
                    <div
                      key={group.label}
                      style={{ cursor: 'pointer' }}
                      onClick={() =>
                        moveToGroup(
                          groups,
                          group,
                          parentGroup,
                          element,
                          setGroups,
                          setShowGroupMenu
                        )
                      }
                    >
                      {`Move to ${group.label}`}
                    </div>
                  )
                })}
                <div
                  style={{ cursor: 'pointer' }}
                  onClick={() =>
                    moveToNewGroup(groups, element, setGroups, setShowGroupMenu)
                  }
                >
                  Move to new group
                </div>
                {isInGroup && (
                  <div
                    style={{ cursor: 'pointer' }}
                    onClick={() =>
                      moveOut(
                        groups,
                        element,
                        parentGroup,
                        setGroups,
                        setShowGroupMenu
                      )
                    }
                  >
                    Move out
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>

      {element.categories.map((category) => {
        return (
          <CategoryRow
            key={category}
            element={{ label: category, categories: [] }}
            filters={filters}
            toggleVisibility={toggleVisibility}
            visualization={visualization}
            groups={groups}
            setGroups={setGroups}
            colorScale={colorScale}
          />
        )
      })}
    </>
  )
}
