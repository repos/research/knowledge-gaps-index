import { ReactNode } from 'react'
import { Grid } from '@mui/material'

interface LayoutProps {
  children: ReactNode
}

export const Layout = ({ children }: LayoutProps) => {
  return (
    <>
      <Grid
        container
        columns={10}
        py={2}
        minHeight={'100vh'}
        width={'98%'}
        margin={'0 auto'}
        alignContent={'start'}
      >
        {children}
      </Grid>
    </>
  )
}
