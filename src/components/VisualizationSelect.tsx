import {
  FormControl,
  Select,
  MenuItem,
  Box,
  Typography,
  SelectChangeEvent,
} from '@mui/material'
import {
  DATASET_TYPE_GEOGRAPHY,
  IDatasetType,
  ITimeframe,
  IVisualizationType,
  TIMEFRAME_CURRENT,
  TIMEFRAME_OVER_TIME,
  VISUALIZATION_TYPE_BARCHART,
  VISUALIZATION_TYPE_HEATMAP,
  VISUALIZATION_TYPE_LINECHART,
  VISUALIZATION_TYPE_RICH_TABLE,
} from 'models'

interface VisualizationSelectProps {
  visualization: IVisualizationType
  handleVisualization: (event: SelectChangeEvent) => void
  timeframe: ITimeframe
  datasetType: IDatasetType
}

export const VisualizationSelect = ({
  visualization,
  handleVisualization,
  timeframe,
  datasetType,
}: VisualizationSelectProps) => {
  function calcVizTypes(
    timeframe: ITimeframe,
    datasetType: IDatasetType
  ): string[] {
    if (timeframe === TIMEFRAME_CURRENT) {
      if (datasetType === DATASET_TYPE_GEOGRAPHY) {
        return [
          VISUALIZATION_TYPE_BARCHART,
          VISUALIZATION_TYPE_RICH_TABLE,
          VISUALIZATION_TYPE_HEATMAP,
        ]
      } else {
        return [VISUALIZATION_TYPE_BARCHART, VISUALIZATION_TYPE_RICH_TABLE]
      }
    }
    if (timeframe === TIMEFRAME_OVER_TIME) {
      return [VISUALIZATION_TYPE_BARCHART, VISUALIZATION_TYPE_LINECHART]
    }
    return []
  }

  const vizTypes = calcVizTypes(timeframe, datasetType)

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <Typography variant="body1" m={0}>
        Visualization
      </Typography>
      <FormControl sx={{ width: '50%', marginRight: '-10px' }}>
        <Select
          labelId="visualization"
          defaultValue={VISUALIZATION_TYPE_BARCHART}
          placeholder={VISUALIZATION_TYPE_BARCHART}
          id="aggregation"
          value={visualization}
          onChange={handleVisualization}
          sx={{
            height: '35px',
            '&. MuiOutlinedInput-notchedOutline': {
              border: 'none',
            },
          }}
          style={{fontWeight: 'bold'}}
        >
          {vizTypes.map((visualization) => {
            return (
              <MenuItem key={visualization} value={visualization}>
                {visualization}
              </MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </Box>
  )
}
