import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Visualization } from 'routes'

export const IS_DEVELOPMENT =
  !process.env.NODE_ENV || process.env.NODE_ENV === 'development'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Visualization />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
