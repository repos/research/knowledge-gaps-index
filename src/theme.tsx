import { createTheme } from '@mui/material/styles'
import '@fontsource/roboto'
import '@fontsource/dm-sans'
import '@fontsource/inter'
import { THEME_COLORS } from './constants'

const { lightGrey, white, black, neutralGrey } = THEME_COLORS

const palette = {
  primary: { main: black, light: neutralGrey },
  common: { white: white },
  grey: { 100: lightGrey },
}

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
  palette,
  typography: {
    fontFamily: ['Inter', 'sans-serif'].join(','),
    h2: {
      fontFamily: 'DM Sans',
      color: '#3A25FF',
      fontSize: '20px',
    },
    body1: {
      fontFamily: 'Inter',
      fontSize: '14px',
    },
    body2: {
      fontFamily: 'Inter',
      fontSize: '14px',
      fontWeight: 'bold',
    },
  },
  components: {
    MuiCheckbox: {
      styleOverrides: {
        colorPrimary: {
          // color: neutralGrey,
          '&$checked': {
            fill: neutralGrey,
          },
        },
      },
    },
    MuiTabs: {},
    MuiButton: {
      styleOverrides: {
        root: {
          padding: '4px 12px 5px',
          borderRadius: '10px',
          cursor: 'pointer',
        },
      },
    },
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          '&.MuiInputLabel-root': {
            display: 'none',
          },
          '&.MuiFormLabel-root-MuiInputLabel-root.MuiFocused': {
            display: 'none !important',
          },
          ':hover .MuiAutocomplete-clearIndicator': {
            visibility: 'hidden',
          },
        },
        tag: {
          border: '1px solid #3a25ff',
          color: '#3a25ff',
          fontSize: '14px',
          padding: '5px',
          cursor: 'pointer',
          backgroundColor: '#eaecf0',
        },
        inputRoot: {
          '&:before': {
            borderBottom: 'none !important',
          },
          '&:after': {
            borderBottom: 'none !important',
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          border: 'none',
        },
      },
    },
    MuiSelect: {
      styleOverrides: {},
    },
  },
})

export default theme
