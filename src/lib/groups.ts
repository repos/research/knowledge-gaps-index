/* eslint-disable @typescript-eslint/ban-ts-comment */
import { find, groupBy, sumBy, uniqBy } from 'lodash-es'
import {
  DATA_COLUMN_ARTICLE_CREATED_VALUE,
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  DATA_COLUMN_LANGUAGE,
  DATA_COLUMN_PAGEVIEWS_SUM_VALUE,
  DATA_COLUMN_QUALITY_SCORE_VALUE,
  DATA_COLUMN_REVISION_COUNT_VALUE,
  Datum,
} from 'models'
import { Group } from 'models/group'
import { groupByMulti } from './array'
import { filterDatasetByDates, filterItems } from './filters'

export function updateDatasetCategoriesWithGroups(
  dataset: any,
  groups: Group[]
) {
  const groupsCategories = groups.map((g) => ({
    label: g.label,
    value: g.label,
  }))
  const datasetWithGroupsCategories = {
    ...dataset,
    categories: uniqBy(
      [
        ...dataset.categories.filter(
          (c: any) => !isGroupCheckBasedOnName(c.label)
        ),
        ...groupsCategories,
      ],
      'label'
    ),
  }
  return datasetWithGroupsCategories
}

export function getDatasetWithGroups(
  fullDataset: Datum[],
  filteredDataset: Datum[],
  filters: Record<string, boolean>,
  groups: Group[],
  dates: [string, string]
) {
  const filteredByDate = filterDatasetByDates(fullDataset, filters, ...dates)
  const groupsDataset = groups.flatMap((group) => {
    const { label, categories } = group
    const datasetsForCategories = filteredByDate.filter((d) =>
      categories.includes(d[DATA_COLUMN_CATEGORY])
    )
    const datasetsForCategoriesGroupByDateAndLang = groupByMulti(
      datasetsForCategories,
      [DATA_COLUMN_DATE, DATA_COLUMN_LANGUAGE]
    )

    const groupSum = Object.entries(
      datasetsForCategoriesGroupByDateAndLang
    ).flatMap(
      ([date, datasetByGroupAndDate]: [
        date: string,
        datasetByGroupAndDate: Record<string, Datum[]>
      ]) => {
        return Object.entries(datasetByGroupAndDate).map(
          ([language, dataset]) => {
            return {
              ...dataset[0],
              [DATA_COLUMN_CATEGORY]: label,
              [DATA_COLUMN_ARTICLE_CREATED_VALUE]: sumBy(
                dataset,
                DATA_COLUMN_ARTICLE_CREATED_VALUE
              ),
              [DATA_COLUMN_PAGEVIEWS_SUM_VALUE]: sumBy(
                dataset,
                DATA_COLUMN_PAGEVIEWS_SUM_VALUE
              ),
              [DATA_COLUMN_QUALITY_SCORE_VALUE]: sumBy(
                dataset,
                DATA_COLUMN_QUALITY_SCORE_VALUE
              ),
              [DATA_COLUMN_REVISION_COUNT_VALUE]: sumBy(
                dataset,
                DATA_COLUMN_REVISION_COUNT_VALUE
              ),
            }
          }
        )
      }
    )

    return groupSum
  })

  const groupsDatasetFiltered = groupsDataset.filter((d) =>
    filterItems(filters).includes(d[DATA_COLUMN_CATEGORY])
  )
  const categoriesInGroups = groups.flatMap((g) => g.categories)
  const filteredDatasetWithoutDataInGroups = filteredDataset.filter(
    (d) =>
      !categoriesInGroups.includes(d[DATA_COLUMN_CATEGORY]) &&
      !isGroup(groups, d[DATA_COLUMN_CATEGORY])
  )
  const filteredDatasetWithGroupsData = [
    ...filteredDatasetWithoutDataInGroups,
    ...groupsDatasetFiltered,
  ]
  return filteredDatasetWithGroupsData
}

export function isGroup(groups: Group[], name: string) {
  return groups.map((g) => g.label).includes(name)
}
export function isGroupCheckBasedOnName(name: string) {
  return name.startsWith('Group ')
}

export function getParentGroup(groups: Group[], name: string) {
  return find(groups, (g) => g.categories.includes(name))
}

export function isInsideAGroup(groups: Group[], name: string) {
  return !getParentGroup(groups, name)
}

export function getCategoriesHierarchy(
  groups: Group[],
  filtersKeys: string[]
): Group[] {
  const categoriesInGroups = groups.flatMap((g) => g.categories)
  const groupsLabels = groups.map((g) => g.label)
  const filtersKeysWithoutGroups = filtersKeys.filter(
    (key) => !isGroupCheckBasedOnName(key)
  )
  const filtersKeyHierarchy = filtersKeysWithoutGroups.map((k) => ({
    label: k,
    categories: [],
  }))
  const categoriesHierarchy = [
    ...groups,
    ...filtersKeyHierarchy.filter(
      (k) => ![...groupsLabels, ...categoriesInGroups].includes(k.label)
    ),
  ]
  return categoriesHierarchy
}

export function moveToNewGroup(
  groups: Group[],
  element: Group,
  setGroups: (g: Group[]) => void,
  setShowGroupMenu: (v: boolean) => void
) {
  const groupNumber = groups.length + 1
  const updatedGroups = [
    ...groups,
    {
      label: `Group ${groupNumber}`,
      categories: [element.label],
    },
  ]
  setGroups(updatedGroups)
  setShowGroupMenu(false)
}

export function moveToGroup(
  groups: Group[],
  group: Group,
  parentGroup: Group | undefined,
  element: Group,
  setGroups: (g: Group[]) => void,
  setShowGroupMenu: (v: boolean) => void
) {
  if (!parentGroup) {
    const parentGroupUpdated = {
      ...group,
      categories: [...group.categories, element.label],
    }
    const groupsWithoutOldParentGroup = groups.filter(
      (g) => g.label !== group.label
    )
    const updatedGroups = [...groupsWithoutOldParentGroup, parentGroupUpdated]
    setGroups(updatedGroups)
    setShowGroupMenu(false)
  } else {
    const oldParentGroupUpdated = {
      ...parentGroup,
      categories: parentGroup.categories.filter((c) => c !== element.label),
    }
    const parentGroupNew = {
      ...group,
      categories: [...group.categories, element.label],
    }
    const groupsWithoutOldParentGroups = groups.filter(
      (g) => g.label !== parentGroup.label && g.label !== group.label
    )
    const updatedGroups = [
      ...groupsWithoutOldParentGroups,
      oldParentGroupUpdated,
      parentGroupNew,
    ]
    setGroups(updatedGroups.filter((g) => g.categories.length > 0))
    setShowGroupMenu(false)
  }
}

export function moveOut(
  groups: Group[],
  element: Group,
  parentGroup: Group | undefined,
  setGroups: (g: Group[]) => void,
  setShowGroupMenu: (v: boolean) => void
) {
  if (!parentGroup) {
    return
  }
  const parentGroupUpdated = {
    ...parentGroup,
    categories: parentGroup.categories.filter((c) => c !== element.label),
  }
  const groupsWithoutOldParentGroup = groups.filter(
    (g) => g.label !== parentGroup.label
  )
  const updatedGroups = [...groupsWithoutOldParentGroup, parentGroupUpdated]
  setGroups(updatedGroups.filter((g) => g.categories.length > 0))
  setShowGroupMenu(false)
}

export function deleteGroup(
  groups: Group[],
  groupLabel: string,
  setGroups: (g: Group[]) => void,
  setShowGroupMenu: (v: boolean) => void
) {
  const groupsWithoutTheGroup = groups.filter((g) => g.label !== groupLabel)

  setGroups(groupsWithoutTheGroup)
  setShowGroupMenu(false)
}
