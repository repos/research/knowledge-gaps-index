import { IS_DEVELOPMENT } from 'App'
import {
  DATASET_TYPE_GENDER,
  DATASET_TYPE_GEOGRAPHY,
  DATASET_TYPE_SEXUAL_ORIENTATION,
  DATASET_TYPE_TEST,
  DATASET_TYPE_TIMESPAN,
} from 'models'
import { getParsedDatasetFromPath } from './format'

export async function getDatasets() {
  const dataGenderParsed = await getParsedDatasetFromPath(
    './datasets/gender.csv'
  )
  const dataSexualOrientationParsed = await getParsedDatasetFromPath(
    './datasets/sexual.csv'
  )
  const dataGeographicParsed = await getParsedDatasetFromPath(
    './datasets/geo.csv'
  )
  const dataTimespanParsed = await getParsedDatasetFromPath(
    './datasets/centuries.csv'
  )
  const dataTestParsed = await getParsedDatasetFromPath('./datasets/fruit.csv')
  const fullDatasets = {
    [DATASET_TYPE_GENDER]: dataGenderParsed,
    [DATASET_TYPE_SEXUAL_ORIENTATION]: dataSexualOrientationParsed,
    [DATASET_TYPE_GEOGRAPHY]: dataGeographicParsed,
    [DATASET_TYPE_TIMESPAN]: dataTimespanParsed,
    [DATASET_TYPE_TEST]: dataTestParsed,
  }
  return fullDatasets
}
