import { extent } from 'd3-array'

const isUndefinedTuple = (x: [any, any]): x is [undefined, undefined] => {
  return x.length === 2 && x[0] === undefined && x[1] === undefined
}

export const extentBy = <T>(
  array: T[],
  by: (value: T) => number | number[],
  fallback: [number, number]
): [number, number] => {
  const result = extent(array.map(by).flat())
  return isUndefinedTuple(result) ? fallback : result
}
