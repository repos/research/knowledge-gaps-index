import { format, add } from 'date-fns'
import { DATA_COLUMN_DATE, Datum } from 'models'

export function getMinMaxDates(dataset: Datum[]): [string, string] {
  const dates = dataset.map((e) => {
    const date = new Date(`${e[DATA_COLUMN_DATE]}-01`)
    return date
  })

  const min = dates.reduce((a: Date, b: Date) => (a < b ? a : b))
  const max = dates.reduce((a: Date, b: Date) => (a > b ? a : b))
  const oneMonthAfterMax = add(max, { months: 1 })

  return [format(min, 'yyyy-MM'), format(oneMonthAfterMax, 'yyyy-MM')]
}
