/* eslint-disable @typescript-eslint/ban-ts-comment */
import { groupBy, uniqBy } from 'lodash-es'

export function getValuesUniqBy(dataset: any[], column: string) {
  return uniqBy(dataset, column).map((d) => d[column])
}

export function naturalSort(array: Array<string | number>) {
  return array.sort((a, b) =>
    a.toString().localeCompare(b.toString(), undefined, { numeric: true })
  )
}

export function groupByMulti(dataset: any[], keys: string[]) {
  if (!keys.length) return dataset
  const byFirst = groupBy(dataset, keys[0])
  const rest = keys.slice(1)
  for (const prop in byFirst) {
    // @ts-ignore
    byFirst[prop] = groupByMulti(byFirst[prop], rest)
  }
  return byFirst
}
