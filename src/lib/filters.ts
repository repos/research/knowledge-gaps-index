import {
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  DATA_COLUMN_LANGUAGE,
  Datum,
} from 'models'

const getNumericDate = (dateString: string) => {
  if (dateString === '') {
    return ''
  }
  const year = parseInt(dateString.split('-')[0])
  const month = parseInt(dateString.split('-')[1])
  const numericValue = month + year * 12
  return numericValue
}

export const filterByDate = (
  dataset: Datum[],
  startDate?: string,
  endDate?: string
): Datum[] => {
  if (dataset && startDate === '' && endDate === '') {
    return dataset
  }
  if (dataset && startDate && startDate === '' && endDate && endDate !== '') {
    return dataset.filter(
      (d: any) => getNumericDate(d[DATA_COLUMN_DATE]) <= getNumericDate(endDate)
    )
  }
  if (dataset && startDate && startDate !== '' && endDate === '') {
    return dataset.filter(
      (d: any) =>
        getNumericDate(d[DATA_COLUMN_DATE]) >= getNumericDate(startDate)
    )
  }
  if (dataset && startDate && startDate !== '' && endDate && endDate !== '') {
    return dataset.filter(
      (d: any) =>
        getNumericDate(d[DATA_COLUMN_DATE]) >= getNumericDate(startDate) &&
        getNumericDate(d[DATA_COLUMN_DATE]) <= getNumericDate(endDate)
    )
  }
  return []
}

export const getCategoryValues = (
  categories: string[],
  oldFilters: Record<string, boolean>,
  resetFilters = false
) => {
  const filtersTuple: Array<[string, boolean]> = categories.map((category) => {
    const existsBefore = resetFilters
      ? false
      : oldFilters[category] !== undefined
    return [category, existsBefore ? oldFilters[category] : true]
  })

  const result = filtersTuple.reduce((acc, value) => {
    const [category, isVisible] = value
    acc[category] = isVisible
    return acc
  }, {} as Record<string, boolean>)
  return result
}

export const filterByCategory = (
  dataset: Datum[],
  visibleCategories: string[]
): Datum[] => {
  return dataset.filter((d) =>
    visibleCategories.includes(d[DATA_COLUMN_CATEGORY])
  )
}

export const filterItems = (arr: Record<string, boolean>) => {
  const keys = Object.keys(arr).filter((e) => {
    return arr[e] === true
  })
  return keys
}

export const filterDatasetByDates = (
  fullDataset: Datum[],
  filters: Record<string, boolean>,
  startDate: string,
  endDate: string
) => {
  const visibleCategories = filterItems(filters) // list of categories with value true in filters

  // filter by category
  const filteredByCategory = filterByCategory(fullDataset, visibleCategories)

  // filter by date
  const filteredByDate = filterByDate(filteredByCategory, startDate, endDate)

  return filteredByDate
}

export const filterDatasetByDatesCategoriesAndLanguage = (
  dataset: Datum[],
  dates: [string, string],
  categories: Record<string, boolean>,
  languages: string[]
) => {
  const [startDate, endDate] = dates

  const visibleCategories = filterItems(categories) // array of categories with value true in filters

  const filteredByCategoriesAndLanguage = dataset.filter((d) => {
    return (
      visibleCategories.includes(d[DATA_COLUMN_CATEGORY]) &&
      languages.includes(d[DATA_COLUMN_LANGUAGE])
    )
  })

  const filteredByDate = filterByDate(
    filteredByCategoriesAndLanguage,
    startDate,
    endDate
  )

  return filteredByDate
}

export function filterByLanguages(dataset: Datum[], languages: string[]) {
  return dataset.filter((d) => {
    return languages.includes(d[DATA_COLUMN_LANGUAGE])
  })
}
