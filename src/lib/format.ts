/* eslint-disable @typescript-eslint/ban-ts-comment */
import { csv } from 'd3'
import { groupBy, omit, sortBy, uniqBy } from 'lodash-es'
import {
  Datum,
  AGGREGATION_TYPE_MONTHLY,
  AGGREGATION_TYPE_YEARLY,
  DATA_COLUMN_CATEGORY,
  DATA_COLUMN_DATE,
  IAggregationType,
  RawDatum,
  Group,
} from 'models'

export function formatNumbersWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export function formatNumberCompact(x: number) {
  return x.toLocaleString('en-US', {
    notation: 'compact',
    compactDisplay: 'short',
  })
}

export function prepareDatasetForStackedBarchart(
  dataset: Datum[],
  columnName: string
) {
  const categories = uniqBy(dataset, DATA_COLUMN_CATEGORY).map(
    (d) => d[DATA_COLUMN_CATEGORY]
  )
  const groupedByDate = groupBy(dataset, DATA_COLUMN_DATE)
  const initializer = Object.fromEntries(categories.map((c) => [c, 0]))
  const dates = Object.keys(groupedByDate)
  const flatizedDataset = dates.map((date) => {
    return groupedByDate[date].reduce((acc, datum) => {
      acc = {
        ...initializer,
        ...acc,
        [datum[DATA_COLUMN_CATEGORY]]:
          // @ts-ignore
          (acc[datum[DATA_COLUMN_CATEGORY]] || 0) + (datum[columnName] || 0),
        [DATA_COLUMN_DATE]: date,
      }
      return acc
    }, {})
  })
  return flatizedDataset
}

export const aggregateAndFlatDataset = (
  unstackedDataset: Datum[],
  aggregation: IAggregationType,
  columnName: string
) => {
  const flatDataset = prepareDatasetForStackedBarchart(
    unstackedDataset,
    columnName
  )
  let summedByCategoryUnsorted
  if (aggregation === AGGREGATION_TYPE_MONTHLY) {
    summedByCategoryUnsorted = flatDataset
  }
  if (aggregation === AGGREGATION_TYPE_YEARLY) {
    const yearlyFlatDataset = flatDataset.map((d) => {
      return {
        ...d,
        // @ts-ignore
        [DATA_COLUMN_DATE]: d[DATA_COLUMN_DATE].substring(0, 4),
      }
    })
    const groupedByDate = groupBy(yearlyFlatDataset, DATA_COLUMN_DATE)
    const keysToSum = Object.keys(omit(yearlyFlatDataset[0], DATA_COLUMN_DATE))
    const summedByCategory = Object.entries(groupedByDate).reduce(
      (acc, item) => {
        const [date, datasets] = item
        const datumCategoriesSum = sumValuesWithSameKeys(datasets, keysToSum)
        acc[date] = { ...datumCategoriesSum, [DATA_COLUMN_DATE]: date }
        return acc
      },
      {} as Record<string, any>
    )
    summedByCategoryUnsorted = Object.values(summedByCategory)
  }
  const sortedByDate = sortBy(summedByCategoryUnsorted, DATA_COLUMN_DATE)
  return sortedByDate
}

export const sumValuesWithSameKeys = (dataset: any[], keys: string[]) => {
  const result = dataset.reduce((acc, e, i, a) => {
    keys.forEach((k) => {
      acc[k] = (acc[k] || 0) + parseFloat(e[k] || 0)
    })
    if (!a[i + 1])
      Object.keys(e)
        .filter((k) => typeof e[k] == 'string')
        .forEach((k) => (acc[k] /= dataset.length))
    return acc
  }, {})

  return result
}

export function setAllCategoriesVisibility(
  obj: Record<string, boolean>,
  value: boolean
) {
  return Object.keys(obj).reduce((accumulator, key) => {
    return { ...accumulator, [key]: value }
  }, {})
}

export function textEllipsis(text: string, barWidth: number) {
  const characterWidth = 8 //Single Character Width In PX
  const barCharactersWidth = barWidth / 10

  if (text.length * characterWidth > barWidth) {
    const slicedText = text.slice(0, barCharactersWidth) + '...'
    return slicedText
  }
  return text
}

function parseDataset(dataset: RawDatum[]): Datum[] {
  return dataset.map((d) => {
    return {
      ...d,
      article_created_value: Number(d['article_created_value']),
      pageviews_sum_value: Number(d['pageviews_sum_value']),
      quality_score_value: Number(d['quality_score_value']),
      revision_count_value: Number(d['revision_count_value']),
    }
  })
}

async function fetchCsvFromPath(filepath: string): Promise<RawDatum[]> {
  const dataCsv = (await csv(filepath)) as RawDatum[]
  return dataCsv
}

export async function getParsedDatasetFromPath(filepath: string) {
  const dataCsv = await fetchCsvFromPath(filepath)
  const dataParsed = parseDataset(dataCsv)
  const sortedByCategory = dataParsed.sort((a, b) =>
    a[DATA_COLUMN_CATEGORY].toString().localeCompare(
      b[DATA_COLUMN_CATEGORY].toString(),
      undefined,
      {
        numeric: true,
      }
    )
  )
  return sortedByCategory
}

export function sortArrayGroupsAlphabetically(
  alphabeticOrder: string,
  categoriesHierarchy: any,
  keyType: string
) {
  const groupsArray: any = []

  const allCategoriesSorted = categoriesHierarchy.sort((a: any, b: any) => {
    const aValue = a[keyType]
    const bValue = b[keyType]

    if (alphabeticOrder === 'Alphabetical') {
      return aValue.localeCompare(bValue)
    } else {
      return bValue.localeCompare(aValue)
    }
  })

  const categoriesWithoutGroups = allCategoriesSorted.filter(
    (category: any) => {
      if (category[keyType].slice(0, 5) !== 'Group') {
        return category
      } else {
        groupsArray.push(category)
        return false
      }
    }
  )
  return [...groupsArray, ...categoriesWithoutGroups]
}
