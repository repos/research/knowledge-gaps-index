import { VIZ_COLORS } from '../constants'
import {
  interpolateBrBG,
  scaleSequential,
  scaleOrdinal,
  ScaleOrdinal,
} from 'd3'
import { countries } from 'lib'
import * as _ from 'lodash-es'

export const countriesColors = () => {
  const keys = Object.keys(countries)

  const numberArray = keys.map((_, i) => {
    return i
  })

  const min = Math.min(...numberArray)
  const max = Math.max(...numberArray)

  const color = scaleSequential()
    .domain([min, max])
    .interpolator(interpolateBrBG)

  const colorArray = numberArray.map((e, i) => {
    return { key: keys[i], value: color(e) }
  })

  const colors = colorArray.reduce(
    (obj, item) => Object.assign(obj, { [item.key]: item.value }),
    {}
  )

  return colors
}

export function getColorScale(
  domain: string[],
  categoriesColors: string[],
  groupsColors: string[] = VIZ_COLORS.group
): ScaleOrdinal<string, string> {
  // data categories
  const repeateCategoriesRange = domain.map(
    (_, i) => categoriesColors[i % categoriesColors.length]
  )

  // groups
  const groupsStartingFromZero = _.range(1000).map((item, i) => `Group ${i}`)
  const groups = groupsStartingFromZero.slice(1)
  const repeateGroupsRange = groups.map(
    (_, i) => groupsColors[i % groupsColors.length]
  )

  const mixedScale = scaleOrdinal<string, string>(
    [...domain, ...groups],
    [...repeateCategoriesRange, ...repeateGroupsRange]
  )
  return mixedScale
}
