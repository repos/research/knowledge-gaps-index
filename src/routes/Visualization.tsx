import { useCallback, useEffect, useState } from 'react'
import { Grid, SelectChangeEvent } from '@mui/material'
import { ScaleOrdinal } from 'd3'
import {
  Filters,
  Layout,
  MainSelect,
  VisualizationSelect,
  Charts,
} from 'components'
import { LoadingPage } from 'components/LoadingPage'
import {
  AGGREGATION_TYPE_YEARLY,
  DATASET_TYPE_GENDER,
  DATASET_TYPE_GEOGRAPHY,
  DATASET_TYPE_SEXUAL_ORIENTATION,
  DATASET_TYPE_TIMESPAN,
  DATA_COLUMN_CATEGORY,
  DATA_TYPE_ARTICLE_COUNTS,
  Datum,
  IAggregationType,
  IDatasetType,
  IDataType,
  IVisualizationType,
  TIMEFRAME_CURRENT,
  VISUALIZATION_TYPE_BARCHART,
  VISUALIZATION_TYPE_HEATMAP,
  Group as GroupType,
  DATA_COLUMN_LANGUAGE,
  IMainFilters,
  DATASET_TYPE_TEST,
} from 'models'
import { VIZ_COLORS } from '../constants'
import {
  getDatasetWithGroups,
  getCategoryValues,
  getColorScale,
  isGroupCheckBasedOnName,
  filterDatasetByDatesCategoriesAndLanguage,
  getValuesUniqBy,
  filterByLanguages,
} from 'lib'
import { getDatasets } from 'lib/dataset'
import { IS_DEVELOPMENT } from 'App'

export const Visualization = () => {
  ///// -----------------------------------------------------------------------------
  ///// STATES
  ///// -----------------------------------------------------------------------------

  const [isLoading, setIsLoading] = useState(true)
  // datasets original, no filters applied on them
  const [fullDatasets, setFullDatasets] = useState<
    Record<IDatasetType, Datum[]>
  >({
    [DATASET_TYPE_GENDER]: [],
    [DATASET_TYPE_SEXUAL_ORIENTATION]: [],
    [DATASET_TYPE_GEOGRAPHY]: [],
    [DATASET_TYPE_TIMESPAN]: [],
    [DATASET_TYPE_TEST]: [],
  })
  const [mainFilters, setMainFilters] = useState<IMainFilters>({
    datasetType: IS_DEVELOPMENT ? DATASET_TYPE_TEST : DATASET_TYPE_GENDER,
    timeframe: TIMEFRAME_CURRENT,
  })

  const [fullDataset, setFullDataset] = useState<Datum[]>([]) // dataset filtered only by selected languages
  const [filteredDataset, setFilteredDataset] = useState<Datum[]>([])
  const [visualization, setVisualization] = useState<IVisualizationType>(
    VISUALIZATION_TYPE_BARCHART
  )
  const [subSelection, setSubSelection] = useState<IDataType>(
    DATA_TYPE_ARTICLE_COUNTS
  )
  const [groups, setGroups] = useState<GroupType[]>([])
  const [startDate, setStartDate] = useState<string>('')
  const [endDate, setEndDate] = useState<string>('')
  const [aggregation, setAggregation] = useState<IAggregationType>(
    AGGREGATION_TYPE_YEARLY
  )
  const [normalization, setNormalization] = useState<boolean>(false)
  const [colorScale, setColorScale] = useState<ScaleOrdinal<
    string,
    string
  > | null>(null)
  const [filters, setFilters] = useState<Record<string, boolean>>({})
  const [alphabeticOrder, setAlphabeticOrder] = useState<string>('Alphabetical')
  const [languages, setLanguages] = useState<string[]>([])
  const [selectedLanguages, setSelectedLanguages] = useState<string[]>([
    'arwiki',
  ])

  const [mainSelectOpen, setMainSelectOpen] = useState(false)

  ///// -----------------------------------------------------------------------------
  ///// FUNCTIONS
  ///// -----------------------------------------------------------------------------

  ///// -----------------------------------------------------------------------------
  ///// DATA FETCHING
  ///// -----------------------------------------------------------------------------

  const fetchFullRawDatasets = useCallback(async () => {
    const fullDatasets = await getDatasets()
    setFullDatasets(fullDatasets)

    const datasetType = mainFilters.datasetType
    const dataset = fullDatasets[datasetType]
    setFullDataset(dataset)
    setFilteredDataset(dataset)

    const categoriesNames = getValuesUniqBy(dataset, DATA_COLUMN_CATEGORY)
    const colorScale = getColorScale(
      categoriesNames.filter((c) => !isGroupCheckBasedOnName(c)),
      VIZ_COLORS.main
    )
    setColorScale(() => colorScale)

    const initFilters = getCategoryValues(categoriesNames, {})
    setFilters(initFilters)

    const languages = getValuesUniqBy(dataset, DATA_COLUMN_LANGUAGE).sort()
    setLanguages(languages)
    setSelectedLanguages([languages[0]])

    setIsLoading(false)
  }, [])
  useEffect(() => {
    fetchFullRawDatasets()
  }, [])

  ///// -----------------------------------------------------------------------------
  ///// EFFECTS
  ///// -----------------------------------------------------------------------------

  useEffect(() => {
    if (isLoading) return
    const datasetType = mainFilters.datasetType
    const dataset = fullDatasets[datasetType]

    setFullDataset(dataset)
    const categoriesNames = getValuesUniqBy(dataset, DATA_COLUMN_CATEGORY)
    const colorScale = getColorScale(
      categoriesNames.filter((c) => !isGroupCheckBasedOnName(c)),
      VIZ_COLORS.main
    )
    setColorScale(() => colorScale)
    if (datasetType === DATASET_TYPE_GEOGRAPHY) {
      if (mainFilters.timeframe === TIMEFRAME_CURRENT) {
        setVisualization(VISUALIZATION_TYPE_HEATMAP)
      } else {
        setVisualization(VISUALIZATION_TYPE_BARCHART)
      }
    } else {
      setVisualization(VISUALIZATION_TYPE_BARCHART)
    }
    const initFilters = getCategoryValues(categoriesNames, filters)
    setFilters(initFilters)
    const languages = getValuesUniqBy(dataset, DATA_COLUMN_LANGUAGE).sort()
    setLanguages(languages)
    const selectedLanguages = [languages[0]]
    setSelectedLanguages(selectedLanguages)
    const filteredByLanguage = filterByLanguages(dataset, selectedLanguages)
    setFilteredDataset(filteredByLanguage)
  }, [mainFilters])

  useEffect(() => {
    // update datasets
    const fullDatasetWithAllLanguages = fullDatasets[mainFilters.datasetType]
    const fullDataset = filterByLanguages(
      fullDatasetWithAllLanguages,
      selectedLanguages
    )
    setFullDataset(fullDataset)
    setFilteredDataset(fullDataset)
    const categoriesNames = getValuesUniqBy(fullDataset, DATA_COLUMN_CATEGORY)
    // reset filters
    const initFilters = getCategoryValues(categoriesNames, filters, true)
    setFilters(initFilters)
    // reset dates
    setStartDate('')
    setEndDate('')
    // reset groups
    setGroups([])
  }, [selectedLanguages])

  useEffect(() => {
    // update datasets
    const fullDatasetWithAllLanguages = fullDatasets[mainFilters.datasetType]
    const fullDataset = filterByLanguages(
      fullDatasetWithAllLanguages,
      selectedLanguages
    )
    setFullDataset(fullDataset)
    setFilteredDataset(fullDataset)
    const categoriesNames = getValuesUniqBy(fullDataset, DATA_COLUMN_CATEGORY)
    const categoriesNamesWithGroups = [
      ...categoriesNames,
      ...groups.map((g) => g.label),
    ]
    // reset filters
    const initFilters = getCategoryValues(categoriesNamesWithGroups, filters)
    setFilters(initFilters)
    // reset dates
    setStartDate('')
    setEndDate('')
  }, [aggregation])

  useEffect(() => {
    if (isLoading) return
    const groupsFilters = groups.reduce((acc, group) => {
      const { label } = group
      acc[label] = filters[label] !== undefined ? filters[label] : true
      return acc
    }, {} as Record<string, boolean>)
    const oldFiltersNotGroups = Object.entries(filters).reduce(
      (acc, filter) => {
        if (!isGroupCheckBasedOnName(filter[0])) {
          acc[filter[0]] = filter[1]
        }
        return acc
      },
      {} as Record<string, boolean>
    )
    const updatedFilters = { ...oldFiltersNotGroups, ...groupsFilters }
    setFilters(updatedFilters)
  }, [groups])

  useEffect(() => {
    if (isLoading) return
    const filteredByDatesCategoriesAndDates =
      filterDatasetByDatesCategoriesAndLanguage(
        fullDataset,
        [startDate, endDate],
        filters,
        selectedLanguages
      )
    const filteredDatasetWithGroupsData = getDatasetWithGroups(
      fullDataset,
      filteredByDatesCategoriesAndDates,
      filters,
      groups,
      [startDate, endDate]
    )
    setFilteredDataset(filteredDatasetWithGroupsData)
  }, [filters, startDate, endDate])

  ///// -----------------------------------------------------------------------------
  ///// RENDER
  ///// -----------------------------------------------------------------------------

  if (isLoading) {
    return <LoadingPage />
  }

  return (
    <Layout>
      <Grid container columns={10} mb="2vh">
        <Grid item xs={1} />
        <Grid item xs={8}>
          <MainSelect
            mainFilters={mainFilters}
            setMainFilters={setMainFilters}
            languages={languages}
            selectedLanguages={selectedLanguages}
            setSelectedLanguages={setSelectedLanguages}
            setMainSelectOpen={setMainSelectOpen}
            mainSelectOpen={mainSelectOpen}
          />
        </Grid>
      </Grid>
      <Grid container columns={10} style={{ position: 'relative' }}>
        <Grid item xs={1} />
        <Grid item xs={2}>
          <VisualizationSelect
            visualization={visualization}
            handleVisualization={(event: SelectChangeEvent) =>
              setVisualization(event.target.value as IVisualizationType)
            }
            timeframe={mainFilters.timeframe}
            datasetType={mainFilters.datasetType}
          />
          <Filters
            setNormalization={setNormalization}
            aggregation={aggregation}
            setAggregation={setAggregation}
            fullDataset={fullDataset}
            setStartDate={setStartDate}
            startDate={startDate}
            setEndDate={setEndDate}
            endDate={endDate}
            subSelection={subSelection}
            setSubSelection={setSubSelection}
            timeframe={mainFilters.timeframe}
            visualization={visualization}
            normalization={normalization}
            groups={groups}
            setGroups={setGroups}
            colorScale={colorScale!}
            filters={filters}
            setFilters={setFilters}
            datasetType={mainFilters.datasetType}
            alphabeticOrder={alphabeticOrder}
            setAlphabeticOrder={setAlphabeticOrder}
          />
        </Grid>

        <Charts
          visualization={visualization}
          filteredDataset={filteredDataset}
          fullDataset={fullDataset}
          subSelection={subSelection}
          aggregation={aggregation}
          datasetType={mainFilters.datasetType}
          timeframe={mainFilters.timeframe}
          normalization={normalization}
          colorScale={colorScale!}
          groups={groups}
          filters={filters}
          alphabeticOrder={alphabeticOrder}
        />
        {mainSelectOpen && (
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
              backgroundColor: '#ffffffd4',
            }}
          ></div>
        )}
      </Grid>
    </Layout>
  )
}
