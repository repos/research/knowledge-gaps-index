import { Layout } from 'components'

export const Homepage = () => {
  return (
    <Layout>
      <h1 style={{ margin: 'auto' }}>Homepage</h1>
    </Layout>
  )
}
