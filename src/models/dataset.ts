export interface RawDatum {
  wiki_db: string
  category: string
  time_bucket: string
  article_created_value: string
  pageviews_sum_value: string
  quality_score_value: string
  revision_count_value: string
}
export interface Datum {
  wiki_db: string
  category: string
  time_bucket: string
  article_created_value: number
  pageviews_sum_value: number
  quality_score_value: number
  revision_count_value: number
}
