export interface IMainSelect {
  gap: string
  project: string
  dimension: string
  timeframe: string
}

export const TIMEFRAME_CURRENT = 'Current'
export const TIMEFRAME_OVER_TIME = 'Over Time'
export type ITimeframe = typeof TIMEFRAME_CURRENT | typeof TIMEFRAME_OVER_TIME

export const DATASET_TYPE_GENDER = 'Gender'
export const DATASET_TYPE_SEXUAL_ORIENTATION = 'Sexual Orientation'
export const DATASET_TYPE_GEOGRAPHY = 'Geography'
export const DATASET_TYPE_TIMESPAN = 'Timespan'
export const DATASET_TYPE_TEST = 'Test'
export type IDatasetType =
  | typeof DATASET_TYPE_GENDER
  | typeof DATASET_TYPE_SEXUAL_ORIENTATION
  | typeof DATASET_TYPE_GEOGRAPHY
  | typeof DATASET_TYPE_TIMESPAN
  | typeof DATASET_TYPE_TEST

// dataset columns names
export const DATA_COLUMN_LANGUAGE = 'wiki_db'
export const DATA_COLUMN_CATEGORY = 'category'
export const DATA_COLUMN_DATE = 'time_bucket'
export const DATA_COLUMN_ARTICLE_CREATED_VALUE = 'article_created_value'
export const DATA_COLUMN_PAGEVIEWS_SUM_VALUE = 'pageviews_sum_value'
export const DATA_COLUMN_QUALITY_SCORE_VALUE = 'quality_score_value'
export const DATA_COLUMN_REVISION_COUNT_VALUE = 'revision_count_value'

// dataset columns to visualize
export const DATA_TYPE_ARTICLE_COUNTS = DATA_COLUMN_ARTICLE_CREATED_VALUE
export const DATA_TYPE_PAGE_VIEWS = DATA_COLUMN_PAGEVIEWS_SUM_VALUE
export const DATA_TYPE_QUALITY_SCORE = DATA_COLUMN_QUALITY_SCORE_VALUE
export const DATA_TYPE_REVISION_COUNT = DATA_COLUMN_REVISION_COUNT_VALUE
export type IDataType =
  | typeof DATA_TYPE_ARTICLE_COUNTS
  | typeof DATA_TYPE_PAGE_VIEWS
  | typeof DATA_TYPE_QUALITY_SCORE
  | typeof DATA_TYPE_REVISION_COUNT

export const VISUALIZATION_TYPE_BARCHART = 'Barchart'
export const VISUALIZATION_TYPE_RICH_TABLE = 'Rich Table'
export const VISUALIZATION_TYPE_HEATMAP = 'Heatmap'
export const VISUALIZATION_TYPE_LINECHART = 'Linechart'
export type IVisualizationType =
  | typeof VISUALIZATION_TYPE_BARCHART
  | typeof VISUALIZATION_TYPE_RICH_TABLE
  | typeof VISUALIZATION_TYPE_HEATMAP
  | typeof VISUALIZATION_TYPE_LINECHART

export const AGGREGATION_TYPE_YEARLY = 'Yearly'
export const AGGREGATION_TYPE_MONTHLY = 'Monthly'
export type IAggregationType =
  | typeof AGGREGATION_TYPE_YEARLY
  | typeof AGGREGATION_TYPE_MONTHLY

export interface IMainFilters {
  datasetType: IDatasetType
  timeframe: ITimeframe
}
