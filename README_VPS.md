# Setting up this repository on Cloud VPS

- UI: https://knowledge-gap-index-tool.wmcloud.org/
- Cloud VPS project name: `wmf-research-tools`
- Cloud VPS instance name: `knowledge-gap-index-tool`
- Location of files: `/var/www/html/knowledge-gaps-index/`

## Web server
Apache is running on the VPS instance. Check out the config files
`000-default.conf` and `default-ssl.conf` in
`/etc/apache2/sites-enabled/`.

## One time setup
Do the following steps on your laptop (the Cloud VPS instance is
underpowered and it fails to build the resources).

1. Install `node` and `npm`, e.g. by following [these
   instructions](https://github.com/nodesource/distributions/blob/master/README.md#debinstall).
2. Also install `yarn`: `sudo npm install -g yarn`.
3. `cd knowledge-gaps-index && yarn install && npm run build`
4. `tar czvf build.tar.gz build`

## Updating the server
1. `scp build.tar.gz knowledge-gap-index-tool.wmf-research-tools.eqiad1.wikimedia.cloud:`
2. `ssh knowledge-gap-index-tool.wmf-research-tools.eqiad1.wikimedia.cloud`
3. `tar xzvf build.tar.gz`
4. `cp -r build/* /var/www/html/knowledge-gaps-index/`
5. `sudo systemctl reload apache2`
6. Visit https://knowledge-gap-index-tool.wmcloud.org/ to see your
   changes.

## Data files
The `datasets` directory contains smaller data files. A larger version
of these files are at
https://analytics.wikimedia.org/published/datasets/knowledge_gaps/content_gaps/.
You can use larger files, however, the browser takes a long time to load
them. This script is used to generated the data files.
https://gitlab.wikimedia.org/repos/research/knowledge-gaps/-/blob/main/knowledge_gaps/output_datasets.py#L112
